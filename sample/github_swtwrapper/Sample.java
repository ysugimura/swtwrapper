package github_swtwrapper;

import org.eclipse.swt.*;
import org.eclipse.swt.widgets.*;


import com.cm55.swt.*;
import com.cm55.swt.label.*;
import com.cm55.swt.layout.*;
import com.cm55.swt.misc.*;
import com.cm55.swt.other.*;
import com.cm55.swt.window.*;

public class Sample {

  static class Foo {
    public final String text;
    public Foo(String text) {
      this.text = text;
    }
    @Override
    public String toString() {
      return text;
    }
  }
  
  public static void main(String[]args) {
    Display display = new Display ();
    Shell shell = ShellManager.newShell(display, 
        SWT.TITLE|SWT.CLOSE|SWT.MIN);
    
    SwObjectCombo<Foo> combo = new SwObjectCombo<>();
    SwVerticalLayout layout = new SwVerticalLayout(new SwItem[] {
        combo,
      new SwLabel("sample")
    });
    SwLayouter.layout(shell,  layout);
    combo.setObjects(new Foo("A"), new Foo("B"));
    
    shell.pack();
    SwUtil.centering(shell);
    
    shell.open();
   
    /*
    display.timerExec(1000, oneSecondRun);
    display.addFilter(SWT.MouseMove, inputListener);
    display.addFilter(SWT.MouseDown, inputListener);
    display.addFilter(SWT.KeyDown, inputListener);
    */
    
    while (!shell.isDisposed ()) {
      try {
        if (!display.readAndDispatch ()) {
          display.sleep ();
        }
      } catch (Error er) {
//        processUncatched(shell, er);
        throw er;
      } catch (RuntimeException ex) {
//        processUncatched(shell, ex);
        throw ex;
      }
    }
    display.dispose ();
    System.out.println("exited");
//    MainLoop.setInstance(new MyMainLoop()).dispatchLoop(shell);
  }
}
