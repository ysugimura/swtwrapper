package com.cm55.swt;

import java.util.function.*;

import org.eclipse.swt.*;
import org.eclipse.swt.events.*;
import org.eclipse.swt.graphics.*;
import org.eclipse.swt.widgets.*;

import com.cm55.eventBus.*;
import com.cm55.swt.misc.*;

public abstract class SwControl<T extends Control> extends SwItem {

  protected T control;
  protected Control focusDelegate;
  protected float fontScale = 1.0F;
  protected EventBus bus = new SwEventBus();
  
  public T getControl() {
    return control;
  }


  public T create(Composite parent) {
    control = doCreate(parent);
    
    if (fontScale != 1.0F) {
      Font font = control.getFont();
      FontData fontData = font.getFontData()[0];
      font  = new Font(control.getDisplay(),"", 
          Math.round(fontData.getHeight() * fontScale), SWT.NORMAL);
      control.setFont(font);
    }
    
    control.addFocusListener(new FocusAdapter() {
      public void focusLost(FocusEvent e) {
        SwControl.this.focusLost(e);
      }
      public void focusGained(FocusEvent e) {
        SwControl.this.focusGained(e);
      }
    });
    return control;
  }
  
  protected void focusLost(FocusEvent e) {
    
  }
  
  protected void focusGained(FocusEvent e) {
    if (focusDelegate != null) focusDelegate.setFocus();
  }
  
  protected abstract T doCreate(Composite parent);

  public final <E> void listen(Class<E> eventType, Consumer<E> consumer) {
    if (consumer == null) return;
    bus.listen(eventType, consumer);
  }

  public final <E> void listenWeak(Class<E> eventType, Object object, Consumer<E> consumer) {
    if (consumer == null) return;
    bus.listenWeak(eventType, object, consumer);
  }
  
  public void dispatchEvent(final Object event) {
    bus.dispatchEvent(event);
  }


  
  public final void setVisible(boolean value) {
    getControl().setVisible(value);
  }
  
  public final boolean isEnabled() {
    return getEnabled();
  }
  public final boolean getEnabled() {
    return getControl().getEnabled();
  }
  
  public final boolean isDisposed() {
    return getControl().isDisposed();
  }

  public final void setFontScale(float value) {
    fontScale = value;
  }
  
  /** シェルを取得 */
  public final Shell getShell() {
    return control.getShell();
  }

  public final void setFocusDelegate(Control c) {
    focusDelegate = c;
  }

  /** フォーカス移動 */
  public final boolean setFocus() {
    if (focusDelegate != null) return false;
    return control.setFocus();
  }

  /** 有効状態の設定 */
  public final void setEnabled(boolean value) {
    control.setEnabled(value);
  }

  /** 可視状態の取得 */
  public final boolean isVisible() {
    return control.isVisible();
  }

  private WheelTargetControl wheelTargetControl = null;
  
  public void setWheelTarget(WheelTarget c) {
    if (wheelTargetControl == null) wheelTargetControl = new WheelTargetControl();
    wheelTargetControl.setWheelTarget(c);
  }
  

  class WheelTargetControl {
    protected WheelTarget wheelTargetControl;

    WheelTargetControl() {
      control.addListener(SWT.MouseWheel, new Listener() {
        public void handleEvent(Event e) {
          wheelTargetControl.scroll(-e.count);
        }
      });      
    }
    
    public void setWheelTarget(WheelTarget c) {
      assert(wheelTargetControl == null);
      wheelTargetControl = c;

    }    
  }
  
  protected void hook(Runnable runnable) {
    try {
      runnable.run();
    } catch (SWTException ex) {
      Shell shell = control.getShell();
      Object data = shell.getData();
      if (data != null) {
        System.err.println("SWTException in shell:" + data.getClass());
      }
    }
  }
}
