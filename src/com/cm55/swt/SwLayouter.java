package com.cm55.swt;

import org.eclipse.swt.*;
import org.eclipse.swt.widgets.*;

import com.cm55.swt.layout.*;
import com.cm55.swt.misc.*;

/**
 * uidefパッケージのコンポーネント定義をSWTに変換する。
 * 変換途中で作成されたコントロールは指定されたcontroller中の変数に格納される。
 * また、コントロールのSelectionListenerがcontrollerのメソッドを呼び出すように
 * 設定される。
 */
public class SwLayouter {


  
  /** 指定されたcompositeにレイアウトを行う */
  public static void layout(Composite composite, SwLayout udLayout) {
    layout(composite, udLayout, null);
  }
  
  public static void layout(Composite composite, SwLayout udLayout, LMargin margin) {
    udLayout.layout(composite, margin);
  }
  
  /** 指定されたparent上に、udItemを元にしてSWTコントロールを作成し、
   *  それを返す。 */
  public static Control create(Composite parent, SwItem udItem) {
    Control result = internalCreate(parent, udItem);
    if (result == null) {
      System.err.println("" + udItem);
      Thread.dumpStack(); 
    }
    return result;
  }

  /**　指定されたparent上にudItemを元にしてSWTコントロールを作成して返す
   * @param parent
   * @param udItem
   * @param margin
   * @return
   */
  private static Control internalCreate(Composite parent, SwItem udItem) {
    
    if (udItem == null) return null;

    if (udItem instanceof SwControl) {
      Control result = ((SwControl<?>)udItem).create(parent);
      if (result != null) return result;
    }
    
    // レイアウトを行う
    if (udItem instanceof SwLayout) {
      Composite composite = new Composite(parent, SWT.NULL);
      layout(composite, (SwLayout)udItem, null);
      return composite;
    }
    
    // ユーザ側コードによりコントロール群を作成する。
    if (udItem instanceof SwControlClosure) {
      return ((SwControlClosure<?>)udItem).create(parent);
    } 
    


    throw new RuntimeException();    
    
  }

  /** UDMarginおよびUDGroupでのデフォルトのマージン */
  public static LMargin getDefaultMargin() {
    return LMargin.MARGIN_5;
  }

}
