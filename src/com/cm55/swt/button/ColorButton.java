// Created by Cryptomedia Co., Ltd. 2007/03/06
package com.cm55.swt.button;

import org.eclipse.swt.*;
import org.eclipse.swt.events.*;
import org.eclipse.swt.widgets.*;

import com.cm55.swt.label.*;


public class ColorButton extends Label2 {

  //private static final Log log = LogFactory.getLog(ColorButton.class);

  protected SelectionListener listener;

  
  public ColorButton(Composite parent) { 
    super(parent, SWT.BORDER|SWT.NO_BACKGROUND);
    
    addMouseListener(new MouseAdapter() {
      public void mouseUp(MouseEvent e) {
        ColorButton.this.mouseUp(e);
      }
    });
    
    // ペイントイベント受信
    addPaintListener(new PaintListener() {
      public void paintControl(PaintEvent e) {
        ColorButton.this.paint(e);
      }
    });
  }
  
  public void addSelectionListener(SelectionListener l) {
    assert(listener == null);
    listener = l;
  }

  
  protected void mouseUp(MouseEvent e) {
    if (listener != null) listener.widgetSelected(null);
  }
  
}
