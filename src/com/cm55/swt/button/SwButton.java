package com.cm55.swt.button;

import java.util.function.*;

import com.cm55.swt.event.*;

/**
 * ボタン
 */
public class SwButton extends SwButtonAbstract {

  /** ハンドラを指定して作成 */
  public SwButton(String text, Consumer<SwSelectionEvent>handler) {    
    super(0, text, handler);
  }
}
