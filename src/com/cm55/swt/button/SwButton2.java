package com.cm55.swt.button;

import org.eclipse.swt.*;
import org.eclipse.swt.events.*;
import org.eclipse.swt.widgets.*;

import com.cm55.swt.*;
import com.cm55.swt.event.*;

/**
 * カスタムボタン
 */
public class SwButton2 extends SwControl<Button> {

  protected int style = SWT.NULL;

  public Button doCreate(Composite parent) {
    Button button = new Button(parent, style);
    button.addSelectionListener(new SelectionAdapter() {
      @Override
      public void widgetSelected(SelectionEvent e) {
        selected(new SwSelectionEvent(SwButton2.this, e));
      }
    });
    button.addMouseListener(new MouseAdapter() {
      public void mouseUp(MouseEvent e) {
        int stateMask = e.stateMask & ~(1 << (e.button + 18));
        stateMask &= SWT.BUTTON_MASK;
        if (stateMask == 0 && focusDelegate != null) {
          if (focusDelegate != null) focusDelegate.setFocus();
        }
      }
    });
    return button;
  }
  
  /** 上位で行っているfocusDelegateへの移動をやめる */
  @Override
  protected final void focusGained(FocusEvent e) {
  }
  
  /** フォーカス喪失時 */
  @Override
  protected final void focusLost(FocusEvent e) {
  }
  
  /** ボタンクリック時 */
  protected void selected(SwSelectionEvent e) {    
  }
}
