package com.cm55.swt.button;

import java.util.function.*;

import org.eclipse.swt.events.*;
import org.eclipse.swt.widgets.*;

import com.cm55.swt.*;
import com.cm55.swt.event.*;

public class SwButtonAbstract extends SwControl<Button> {

  private int style;
  
  /** 表示テキスト */
  public String text;
  
  /** ハンドラを指定して作成 */
  protected SwButtonAbstract(int style, String text, Consumer<SwSelectionEvent>handler) {    
    this.style = style;
    this.text = text;
    listen(SwSelectionEvent.class, handler);
  }
  
  public void setText(String text) {
    getControl().setText(text);
  }

  public void setAlignment(int value) {
    control.setAlignment(value);
  }
  
  @Override
  protected Button doCreate(Composite parent) {
    // SWT-Button作成
    /*
    int style = SWT.NULL;
    if (this instanceof SwCheckBox) style = SWT.CHECK;
    else if (this instanceof UDToggleButton) style = SWT.TOGGLE;
    */
    
    Button button = new Button(parent, style);
    if (this.text != null) button.setText(this.text);
    button.addSelectionListener(new SelectionAdapter() {
      @Override
      public void widgetSelected(SelectionEvent e) {
        dispatchEvent(new SwSelectionEvent(SwButtonAbstract.this, e));
      }
    });    
    return button;
  };
  
}
