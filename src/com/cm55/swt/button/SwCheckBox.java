package com.cm55.swt.button;

import java.util.function.*;

import org.eclipse.swt.*;

import com.cm55.swt.event.*;

/**
 * チェックボックス
 */
public class SwCheckBox extends SwButtonAbstract {

  public SwCheckBox(String text, Consumer<SwSelectionEvent>handler) {
    super(SWT.CHECK, text, handler);
  }
  
  public void setSelection(boolean value) {
    control.setSelection(value);
  }
  
  public boolean getSelection() {
    return control.getSelection();
  }
  

}
