// Created by Cryptomedia Co., Ltd. 2007/03/06
package com.cm55.swt.button;

import org.eclipse.swt.widgets.*;

import com.cm55.swt.*;

public class SwColorButton extends SwControl<ColorButton> {
 
  public ColorButton doCreate(Composite parent) {
    ColorButton button = new ColorButton(parent);
    return button;
  }
}
