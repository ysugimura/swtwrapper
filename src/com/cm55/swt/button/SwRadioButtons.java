package com.cm55.swt.button;

import java.util.function.*;

import org.eclipse.swt.*;
import org.eclipse.swt.events.*;
import org.eclipse.swt.widgets.*;

import com.cm55.swt.*;
import com.cm55.swt.event.*;

public abstract class SwRadioButtons extends SwControl<RadioButtons> {

  public static class H extends SwRadioButtons {
    public H(String[]texts, Consumer<SwSelectionEvent>handler) {
      super(false, texts, handler);
    }
  }
  
  public static class V extends SwRadioButtons {
    public V(String[]texts, Consumer<SwSelectionEvent>handler) {
      super(true, texts, handler);
    }
  }
  
  
  private final String[]texts;

  private final boolean vertical;
  
  /** ハンドラを指定して作成。横向き */
  private SwRadioButtons(boolean vertical, String[]texts, Consumer<SwSelectionEvent>handler) {    
    this.vertical = vertical;
    this.texts = texts;
    listen(SwSelectionEvent.class, handler);
  }

  public RadioButtons getControl() {
    return (RadioButtons)super.getControl();
  }
  
  public void select(int index) {
    getControl().select(index);
  }
  
  public int getSelectionIndex() {
    return getControl().getSelectionIndex();
  }
  
  @Override
  protected RadioButtons doCreate(Composite parent) {
    RadioButtons radios;
    if (vertical) radios = new RadioButtons(parent, SWT.VERTICAL);
    else radios = new RadioButtons(parent);
    radios.setItems(texts);


    radios.addSelectionListener(new SelectionAdapter() {
      @Override
      public void widgetSelected(SelectionEvent e) { bus.dispatchEvent(new SwSelectionEvent(SwRadioButtons.this, e)); }
    });
    
    return radios;
  };
}
