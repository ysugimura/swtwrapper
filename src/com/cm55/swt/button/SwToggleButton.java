// Created by Cryptomedia Co., Ltd. 2006/09/06
package com.cm55.swt.button;

import java.util.function.*;

import org.eclipse.swt.*;

import com.cm55.swt.event.*;

public class SwToggleButton extends SwButtonAbstract {

  public SwToggleButton(String text, Consumer<SwSelectionEvent>handler) {
    super(SWT.TOGGLE, text, handler);
  }
  
  public boolean getSelection() {
    return control.getSelection();
  }
  
  public void setSelection(boolean value) {
    control.setSelection(value);
  }
}
