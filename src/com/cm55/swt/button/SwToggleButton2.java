// Created by Cryptomedia Co., Ltd. 2005/12/30
package com.cm55.swt.button;

import org.eclipse.swt.*;
import org.eclipse.swt.events.*;
import org.eclipse.swt.widgets.*;

import com.cm55.swt.*;

public class SwToggleButton2 extends SwControl<Button> {
  
  protected int style = SWT.TOGGLE;
  protected Control focusDelegatel;
  
  public Button doCreate(Composite parent) {
    Button button = new Button(parent, style);
    button.addFocusListener(new FocusListener() {
      public void focusGained(FocusEvent e) {
        SwToggleButton2.this.focusGained(e);
      }
      public void focusLost(FocusEvent e) {
        SwToggleButton2.this.focusLost(e);
      }
    });
    button.addSelectionListener(new SelectionAdapter() {
      @Override
      public void widgetSelected(SelectionEvent e) {
//        setModified();
        selected(button.getSelection());
      }
    });
    button.addMouseListener(new MouseAdapter() {
      public void mouseUp(MouseEvent e) {
        int stateMask = e.stateMask & ~(1 << (e.button + 18));
        stateMask &= SWT.BUTTON_MASK;
        if (stateMask == 0 && focusDelegate != null) {
          focusDelegate.setFocus();
        }
      }
    });
    return button;
  }

  
  protected void focusGained(FocusEvent e) {
    
  }
  
  protected void focusLost(FocusEvent e) {
  }
  
  protected void selected(boolean selection) {
    
  }
  
  public void setSelection(boolean value) { control.setSelection(value); }
  
  public void setText(String s) { control.setText(s); }
}
