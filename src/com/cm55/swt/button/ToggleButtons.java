// Created by Cryptomedia Co., Ltd. 2006/01/31
package com.cm55.swt.button;

import java.util.*;

import org.eclipse.swt.*;
import org.eclipse.swt.events.*;
import org.eclipse.swt.graphics.*;
import org.eclipse.swt.layout.*;
import org.eclipse.swt.widgets.*;



/**
 * トグルボタン
 */
public class ToggleButtons extends Composite {

  //private static final Log log = LogFactory.getLog(ToggleButtons.class);
  
  protected int selectionIndex = -1;

  public ToggleButtons(Composite parent) {
    this(parent, SWT.HORIZONTAL);
  }
  
  /** 作成 */
  public ToggleButtons(Composite parent, int type) {
    super(parent, SWT.NULL);
    RowLayout layout = new RowLayout();
    layout.type = type;
    layout.wrap = true;
    layout.pack = false;
    
    super.setLayout(layout);
  }
  
  /** アイテムを設定 */
  public void setItems(String[]items) {
    setItems(null, items);
  }

  /** アイテムを設定 */
  public void setItems(Image[]images, String[]items) {
    //if (log.isTraceEnabled()) log.trace("setItems");

    if (images != null && images.length != items.length)
      throw new InternalError();
    
    // 現在の子をすべて削除
    Control[]children = getChildren();
    for (int i = 0; i < children.length; i++) children[i].dispose();

    //if (log.isTraceEnabled()) log.trace("" + getChildren().length);

    // 新たな子を挿入
    for (int i = 0; i < items.length; i++) {
      Button button = new Button(this, SWT.TOGGLE);
      button.setText(items[i]);
      if (images != null) button.setImage(images[i]);
      button.addSelectionListener(buttonListener);
    }
    selectionIndex = -1;
    layout();
  }


  /** 選択 */
  public void select(int index) {
    if (selectionIndex == index) return;
    Control[]children = getChildren();
    selectionIndex = index;
    if (selectionIndex < 0) {
      for (int i = 0; i < children.length; i++)
        ((Button)children[i]).setSelection(false);
    } else {
      ((Button)children[index]).setSelection(true);
    }
  }

  /** 選択インデックスを取得 */  
  public int getSelectionIndex() {
    return selectionIndex;
  }
  
  private SelectionListener buttonListener = new SelectionAdapter() {
    @Override
    public void widgetSelected(SelectionEvent e) {
      Control[]children = ToggleButtons.this.getChildren();
      
      int newIndex = -1;
      for (int i = 0; i < children.length; i++) {
        if (e.widget == children[i]) {
          newIndex = i;
          //break;
        } else
          ((Button)children[i]).setSelection(false);
      }
      if (newIndex == selectionIndex) return;
      selectionIndex = newIndex;
            
      fireSelectionChanged();
    }
  };
  
  protected ArrayList<SelectionListener> listeners = new ArrayList<SelectionListener>();
    
  public void addSelectionListener(SelectionListener listener) {
    listeners.add(listener);
  }

  protected void fireSelectionChanged() {
    for (int i = 0; i < listeners.size(); i++)
      ((SelectionListener)listeners.get(i)).widgetSelected(null);
  }
  
  /** レイアウトは設定不可 */
  public void setLayout(Layout l) {
    throw new InternalError();
  }
  
  public void setEnabled(boolean value) {
    super.setEnabled(value);
    Control[]children = getChildren();
    for (int i = 0; i < children.length; i++)
      children[i].setEnabled(value);
  }
}
