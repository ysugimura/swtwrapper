package com.cm55.swt.color;

import java.util.*;

import org.eclipse.swt.graphics.*;
import org.eclipse.swt.widgets.*;

/**
 * <p>タイトル: </p>
 * <p>説明: </p>
 * <p>著作権: Copyright (c) 2003</p>
 * <p>会社名: cryptomedia</p>
 * @author yuichi sugimura
 * @version 1.0
 */
public class ColorStock {

  static final Map<ColorData,Color> colorMap = new HashMap<ColorData,Color>();
  public final static Color yellow;
  public final static Color white_yellow;
  public final static Color white;
  public final static Color black;
  public final static Color red;
  public final static Color white_blue;
  public final static Color pink;
  public final static Color light_green;
  public final static Color green;
  public final static Color gray;

  static {
    white_yellow = getColor(0xff, 0xff, 0xcc);
    yellow = getColor(0xff, 0xff, 0);
    white = getColor(0xff, 0xff, 0xff);
    black = getColor(0, 0, 0);
    red = getColor(0xff, 0, 0);
    white_blue = getColor(0xdd, 0xff, 0xff);
    pink = getColor(0xff, 0xcc, 0xff);
    green = getColor(0, 0x80, 0x00);
    gray = getColor(128, 128, 128);
    light_green = getColor(0x90, 0xee, 0x90); //#90ee90
  }

  public static Color getColor(int r, int g, int b) {
    ColorData data = new ColorData(r, g, b);
    Color color = (Color)colorMap.get(data);
    if (color != null) return color;
    color = new Color(null, r, g, b);
    colorMap.put(data, color);
    return color;
  }

  private static class ColorData {
    int r, g, b;
    private ColorData(int r, int g, int b) {
      this.r = r;
      this.g = g;
      this.b = b;
    }
    public int hashCode() {
      return r + g + b;
    }
    public boolean equals(Object o) {
      if (!(o instanceof ColorData)) return false;
      ColorData that = (ColorData)o;
      return r ==  that.r && g == that.g && b == that.b;
    }
  }

  private static Display display;

  public static void setDisplay(Display d) {
    display = d;
  }

  public static Color getSystemColor(int no) {
    return display.getSystemColor(no);
  }
}
