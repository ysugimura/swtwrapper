// Created by Cryptomedia Co., Ltd. 2006/11/05
package com.cm55.swt.color;

import org.eclipse.swt.graphics.*;

public interface RowColoring {
  public Color get(int row);
}
