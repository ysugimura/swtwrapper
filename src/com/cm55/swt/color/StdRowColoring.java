// Created by Cryptomedia Co., Ltd. 2006/10/20
package com.cm55.swt.color;

import org.eclipse.swt.graphics.*;

public class StdRowColoring implements RowColoring {

  public static final Color[]ROW_COLORS = new Color[] {
    ColorStock.white,
    ColorStock.white_yellow,
    ColorStock.white_blue,
  };

  public Color get(int row) {
    return ROW_COLORS[row % ROW_COLORS.length];
  }
}
