// Created by Cryptomedia Co., Ltd. 2006/04/26
package com.cm55.swt.container;

import org.eclipse.swt.*;
import org.eclipse.swt.widgets.*;

import com.cm55.swt.*;

public class SwEmbeddedComposite extends SwControl<Composite> {

  
  public SwEmbeddedComposite() {
  }
  
  public Composite getComposite() {
    return (Composite)getControl();
  }
  
  @Override
  protected Composite doCreate(Composite parent) {

    Composite composite = new Composite(parent, SWT.EMBEDDED);
    this.control = composite;

    return composite;
  };
}
