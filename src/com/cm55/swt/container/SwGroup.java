package com.cm55.swt.container;

import org.eclipse.swt.*;
import org.eclipse.swt.layout.*;
import org.eclipse.swt.widgets.*;

import com.cm55.swt.*;
import com.cm55.swt.layout.*;

/**
 * グループ
 */

public class SwGroup extends SwControl<Group> {
  
  public String text;
  public SwItem item;
  public LMargin margin;
  
  public SwGroup(String text, SwItem item) {
    
    this.text = text;
    this.item = item;
  }
  
  public void setText(String text) {
    getControl().setText(text);
  }
  
  @Override
  protected Group doCreate(Composite parent) {
    Group group = new Group(parent, SWT.NONE);
    this.control = group;
    group.setText(this.text);
    group.setLayout(new FillLayout());


    LMargin margin = this.margin;
    if (margin == null) {
        margin = SwLayouter.getDefaultMargin();
    }
    if (!(this.item instanceof SwLayout))
      SwLayouter.layout(group, new SwMarginLayout(this.item), margin);
    else
      SwLayouter.layout(group, (SwLayout)this.item, margin);

    return group;
  };
}
