package com.cm55.swt.container;

import org.eclipse.swt.*;
import org.eclipse.swt.graphics.*;
import org.eclipse.swt.widgets.*;

import com.cm55.swt.*;

/**
 * Notebook
 */
public class SwNotebook extends SwControl<SwNotebook.Notebook> {

  public SwItem[]items;
 
  public SwNotebook(SwItem...items) {   
    this.items = items;
  }
  
  public void select(int index) { 
    control.select(index);
  }
  
  public int getSelectionIndex() {
    return control.getSelectionIndex();
  }
  

  @Override
  protected Notebook doCreate(Composite parent) {
    Notebook notebook = new Notebook(parent, SWT.NONE);
    for (int i = 0; i < items.length; i++)
      SwLayouter.create(notebook, items[i]);
    return notebook;
  };
  
  /**
   * Notebook
   */
  public static class Notebook extends Composite {

    protected int selectionIndex = -1;
    
    public Notebook(Composite parent) {
      this(parent, SWT.NULL);
    }
    
    public Notebook(Composite parent, int style) {
      super(parent, style);
      super.setLayout(new NBLayout());
    }

    public void setLayout (Layout layout) {
      throw new InternalError();
    }

    public void select(int index) {
      if (selectionIndex == index) return;

      Control[]children = getChildren();
      
      if (index >= children.length) throw new IllegalArgumentException();

      if (selectionIndex >= 0) {
        children[selectionIndex].setBounds(new Rectangle(-10, -10, 10, 10));
      }
      selectionIndex = index;
      if (selectionIndex >= 0) {
        children[selectionIndex].setBounds(getClientArea());
      }
    }
    
    public int getSelectionIndex() {
      return selectionIndex;
    }
    
    private final class NBLayout extends Layout {

      protected Point computeSize (Composite composite, int wHint, int hHint, boolean flushCache) {
        Control [] children = composite.getChildren ();
        int count = children.length;
        int maxWidth = 0, maxHeight = 0;
        for (int i = 0; i < count; i++) {
          Control child = children [i];
          Point pt = child.computeSize (SWT.DEFAULT, SWT.DEFAULT, flushCache);
          maxWidth = Math.max (maxWidth, pt.x);
          maxHeight = Math.max (maxHeight, pt.y);
        }
        return new Point(maxWidth, maxHeight);
      }

      protected void layout (Composite composite, boolean flushCache) {
        Rectangle rect = composite.getClientArea ();
        Control[]children = composite.getChildren ();
        for (int i = 0; i < children.length; i++) {
          if (i == selectionIndex)
            children[i].setBounds(rect);
          else
            children[i].setBounds(new Rectangle(-10, -10, 10, 10));
        }
      }
    }
  }


}
