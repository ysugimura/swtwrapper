package com.cm55.swt.container;

import org.eclipse.swt.*;
import org.eclipse.swt.custom.*;
import org.eclipse.swt.widgets.*;

import com.cm55.swt.*;

/**
 * <p>タイトル: </p>
 * <p>説明: </p>
 * <p>著作権: Copyright (c) 2003</p>
 * <p>会社名: cryptomedia</p>
 * @author yuichi sugimura
 * @version 1.0
 */

public abstract class SwSash extends SwControl<SashForm> {
  
  public static class H extends SwSash {
    public H( SwItem[]items) {
      super(false, items);
    }
    public H(SwItem[]items, int[]weights) {
      super(false, items, weights);
    }
  }

  public static class V extends SwSash  {
    public V(SwItem[]items) {
      super(true, items);
    }    
    public V(SwItem[]items, int[]weights) {
      super(true, items, weights);
    }
  }

  
  public SwItem[]items;
  public int[]weights;
  private boolean vertical;
  
  /** 最大表示中のアイテム */
  private SwControl<?> maximizedControl;
    
  protected SwSash(boolean vertical, SwItem[]items, int[]weights) {
    this.vertical = vertical;
    this.items = items;
    this.weights = weights;
  }

  protected SwSash(boolean vertical, SwItem[]items) {
    this(vertical, items, null);
  }

  /**
   * サッシ領域の全域を占めるコントロールを指定する。あるいはnullで指定解除する。
   * <p>
   * サッシは通常、指定されたアイテムすべてを表示し、それらの大きさを変更するわけだが、ときには、その中の一つだけを最大で表示させたい場合がある。
   * このような場合に、そのコントロールを指定する。解除にはnullを指定する。
   * </p>
   * @param c
   */
  public void setMaximizedControl(SwControl<?> c) {
    if (c == null) control.setMaximizedControl(null);
    else control.setMaximizedControl(c.getControl());
    maximizedControl = c;
  }

  /**
   * 現在最大化されているコントロールを取得する。
   * @return
   */
  public SwControl<?> getMaximizedControl() {  
    return maximizedControl;
  }
  
  /** サッシのセパレータ幅を指定する */
  public void setSeparatorWidth(int value) {
    getControl().SASH_WIDTH = value;
  }

  @Override
  protected SashForm doCreate(Composite parent) {

    int style = vertical? SWT.VERTICAL:SWT.HORIZONTAL;
    SwItem[]items = this.items;
    SashForm sash = new SashForm(parent, style);
   
    for (int i = 0; i < items.length; i++)
      SwLayouter.create(sash, items[i]);

    if (this.weights != null) sash.setWeights(this.weights);
    return sash;
  };
}
