package com.cm55.swt.container;

import java.util.function.*;

import org.eclipse.swt.*;
import org.eclipse.swt.events.*;
import org.eclipse.swt.widgets.*;

import com.cm55.swt.*;
import com.cm55.swt.event.*;
import com.cm55.swt.misc.*;

/**
 * タブ
 */
public class SwTabs  extends SwControl<TabFolder> {
  
  public final String[]names;
  public final SwItem[]items;

  
  public SwTabs(String[]names, Consumer<SwSelectionEvent>consumer, SwItem[]items) {
    
    this.names = names;
    listen(SwSelectionEvent.class, consumer);
    this.items = items;
    assert (names.length == items.length);
  }
  
  public SwTabs(Consumer<SwSelectionEvent>consumer, Page...defs) {
    
    names = new String[defs.length];
    items = new SwItem[defs.length];
    for (int i = 0; i < defs.length; i++) {
      if (defs[i] == null) continue;
      names[i] = defs[i].name;
      items[i] = defs[i].item;
    }
    listen(SwSelectionEvent.class, consumer);
  }
  
  public SwTabs(String[]names, Consumer<SwSelectionEvent>consumer, SwItem item) {
    
    this.names = names;
    this.items = new SwItem[] { item };
    listen(SwSelectionEvent.class, consumer);
  }
  
  public void setSelection(int index) {
    getControl().setSelection(index);
  }
  public int getSelectionIndex() {
    return getControl().getSelectionIndex();
  }
  
  public int getItemCount() {
    return getControl().getItemCount();
  }
  public static class Page {    
    private final String name;
    private final SwItem item;
    public Page(String name, SwItem item) {
      this.name = name;
      this.item = item;
    }
  }
  
  @Override
  protected TabFolder doCreate(Composite parent) {
    SwTabs udTab = this;
    
    TabFolder tabFolder = new TabFolder(parent, SWT.NULL);
    udTab.control = tabFolder;

    String[]inputNames = ArraysEx.dropNull(udTab.names);
    SwItem[]inputItems = ArraysEx.dropNull(udTab.items);
    
    if (inputItems.length == 1) {
     // if (log.isTraceEnabled()) log.trace("single item");
      Control child = SwLayouter.create(tabFolder, inputItems[0]);
      TabItem[]tabItems = new TabItem[inputNames.length];
      for (int i = 0; i < inputNames.length; i++) {
        TabItem tabItem = new TabItem(tabFolder, SWT.NULL);
        tabItem.setText(inputNames[i]);
        tabItems[i] = tabItem;
      }
      // なぜかこうしないとうまくいかない。
      for (int i = inputNames.length - 1; i >= 0; i--) {
        tabItems[i].setControl(child);
      }

    } else {
     // if (log.isTraceEnabled()) log.trace("multiple items " + inputItems.length);
      if (inputNames == null) inputNames = new String[0];
      for (int i = 0; i < inputItems.length; i++) {
        TabItem tabItem = new TabItem(tabFolder, SWT.NULL);
        if (i < inputNames.length) tabItem.setText(inputNames[i]);
        Control child = SwLayouter.create(tabFolder, inputItems[i]);
        tabItem.setControl(child);
       // if (log.isTraceEnabled()) log.trace(i + " completed");
      }
    }


    tabFolder.addSelectionListener(new SelectionAdapter() {
      @Override
      public void widgetSelected(SelectionEvent e) { bus.dispatchEvent(new SwSelectionEvent(SwTabs.this, e)); }
    });
    return tabFolder;
  };
}
