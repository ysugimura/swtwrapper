package com.cm55.swt.dnd;

public abstract class SwDragSource {

  /** ドラッグ開始が可能であるかを返す */
  public abstract boolean start();

  /*
  CtItem[]selectingItems = getSelectingItems();

  // 内部ドラッグスタート
  InternalDragDrop.start(
      ClassItemsPanel.this,
      selectingItems
  );
  */
  
  /** ドラッグが終了した */
  public abstract void finished();
  
  
  /*
      // 内部ドラッグ終了
    InternalDragDrop.clear();
    
    // これをやらないと、重複分類で重複したときにアイコンが赤表示にならない。
    customTable.redraw();
   */
}
