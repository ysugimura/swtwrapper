package com.cm55.swt.dnd;

import org.eclipse.swt.dnd.*;

/** 
 * ドラッグソースリスナ
 * 通常はSwコンポーネント内部で使用する。
 * @author ysugimura
 *
 */
public class SwDragSourceListener implements DragSourceListener {

  /** ユーザが指定したドラッグソース */
  protected final SwDragSource dragSource;

  /**
   * ユーザが指定したドラッグソースを指定する。
   * @param dragSource ドラッグソース
   */
  public SwDragSourceListener(SwDragSource dragSource) {
    this.dragSource = dragSource;
  }
  
  /** ドラッグスタートの通知 */
  @Override
  public void dragStart(DragSourceEvent e) {
    e.doit = dragSource.start();
  };

  /** ドラッグするデータの設定（ダミー） */
  @Override
  public void dragSetData (DragSourceEvent event) {
    event.data = SwDndConsts.DRAG_DATA; // ここに適当な文字列を設定しないとエラーになる。
  }

  /** ドラッグ終了 */
  @Override
  public void dragFinished(DragSourceEvent event) {
    dragSource.finished();
  }
}
