package com.cm55.swt.dnd;

/**
 * ユーザが実装するドロップターゲット
 * @author ysugimura
 */
public abstract class SwDropTarget {

  /** 
   * 主にはdragEnter時に呼び出される。マウス座標などが与えられるがあまり意味が無いので、
   * 引数とはしない。
   */
  public abstract SwDropType getDropType();
  
  /**
   * ドロップが可能かを検証する。
   * @param info ここに格納されるものは、{@link Viewer}によって異なる模様。{@link TableViewer}の場合は行インデックス整数
   * @param type {@link #getDropType()}で指定したタイプがそのまま伝達される模様。
   * @return true:ドロップを受け入れる。false:受け入れない。
   */
  public abstract boolean validate(Object info, SwDropType type);

  /**
   * ドロップを実行する。
   * @return true:実行した。false：実行しなかった。
   */
  public abstract boolean performDrop();
  
}
