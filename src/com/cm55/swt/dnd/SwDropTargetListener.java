package com.cm55.swt.dnd;

import org.eclipse.jface.viewers.*;
import org.eclipse.swt.dnd.*;

/** 
 * ドロップターゲットリスナ
 * <p>
 * 通常はSwコンポーネント内部で使用される。
 * </p>
 */
public class SwDropTargetListener extends ViewerDropAdapter {
  
  /** ユーザ側で用意したドロップターゲット */
  protected final SwDropTarget dropTarget;

  /** 
   * 作成する
   * 
   * @param viewer jfaceのコンポーネントのみがDNDをサポートしている。
   * @param dropTarget ユーザ側で用意したドロップターゲットオブジェクト
   */
  public SwDropTargetListener(Viewer viewer, SwDropTarget dropTarget) {
    super(viewer);
    this.dropTarget = dropTarget;
  }

  @Override
  public void dragEnter(DropTargetEvent e) {
    e.detail = dropTarget.getDropType().value;
  }

  @Override
  public void dragOperationChanged(DropTargetEvent e) {    
    e.detail = dropTarget.getDropType().value;
  }

  /** 
   * ドロップ可能かチェック 
   * 
   * @param target テーブルの場合には、行インデックスが格納されている。ただしnullの場合もある。
   * @param operation MOVE, LINKなど
   * @param transferData どのようなデータか不明
   */
  @Override
  public boolean validateDrop(Object target, int operation, TransferData transferData) {   
    return SwDropType.fromValue(operation)
        .map(dt->dropTarget.validate(target, dt))
        .orElseGet(()->false);
  }

  /** 
   * @param data ソース側で設定したオブジェクト。{@link SwDndConsts.DRAG_DATA}
   */
  @Override
  public boolean performDrop(Object data) {
    return dropTarget.performDrop();
  }
}
