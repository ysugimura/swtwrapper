package com.cm55.swt.dnd;

import java.util.*;

import org.eclipse.swt.dnd.*;

/**
 * ドロップタイプ。
 * これは{@link SwDropTarget}側から返され、ドラッグ中のカーソル形状に影響を与え、
 * ドロップ可能性調査の際に使用される。
 * @author ysugimura
 */
public enum SwDropType {
  /** ドロップ不可 */
  NONE(DND.DROP_NONE),
  
  /** コピー */
  COPY(DND.DROP_COPY),
  
  /** 移動 */
  MOVE(DND.DROP_MOVE),
  
  /** リンク */
  LINK(DND.DROP_LINK);
  
  /** SWT上での値 */
  public final int value;
  
  private SwDropType(int value) {
    this.value = value;
  }

  static Map<Integer, SwDropType>valueMap = new HashMap<>();
  static {
    Arrays.stream(SwDropType.values()).forEach(dt->valueMap.put(dt.value, dt));
  }
  
  public static Optional<SwDropType>fromValue(int value) {
    return Optional.ofNullable(valueMap.get(value));
  }
}
