package com.cm55.swt.dnd;

/**
 * システム内部のドラッグドロップに関するデータ
 * <p>
 * SWTのドラッグドロップは当然ながら、OSのドラッグドロップ機能に対応しているが、しかし、アプリ内部でのドラッグドロップの方がより重要であり、
 * その場合にはOSのサポートするドラッグドロップデータよりも複雑なデータ、つまりJavaオブジェクトを扱う必要がある。
 * これらのJavaオブジェクトデータがドラッグ先で受け入れ可能であるかを判断するため、ドラッグ開始時にデータを設定する。
 * </p>
 */
public class SwInternalDragDrop {

  /** ドラッグされたソース。 ドロップターゲットによっては、特定のパネルからしか受け付けないかもしれない */
  private static Object dragSource;
  
  /** ドラッグされたデータ。上記のソースからドラッグされたもの  */
  private static Object dragData;

  /** ソースを取得する */
  @SuppressWarnings("unchecked")
  public static <T>T source() {
    return (T)dragSource;
  }

  /** データを取得する */
  @SuppressWarnings("unchecked")
  public static <T> T data() {
    return (T)dragData;
  }
  
  /** ドラッグドロップ開始 */
  public static void start(Object source, Object data) {
    dragSource = source;
    dragData = data;
  }
    
  /** クリア */
  public static void clear() {
    dragSource = null;
    dragData = null;
  }
}
