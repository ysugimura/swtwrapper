// Created by Cryptomedia Co., Ltd. 2006/05/31
package com.cm55.swt.draw;

import org.eclipse.swt.*;
import org.eclipse.swt.events.*;
import org.eclipse.swt.graphics.*;
import org.eclipse.swt.widgets.*;



public class ScrolledCanvas extends Composite {

  //private static final Log log = LogFactory.getLog(ScrolledCanvas.class);

  /** キャンバス */
  protected Canvas canvas;

  /** 水平スクロールバー */
  protected Slider hScrollbar;
  protected int hsHeight;

  /** 垂直スクロールバー */
  protected Slider vScrollbar;
  protected int vsWidth;


  private static final int SCROLLBAR_INC = 100;

  /** 作成 */
  public ScrolledCanvas(Composite parent, int style) {

    super(parent, style);

    //if (log.isTraceEnabled())
      //log.trace("ScrolledCanvas");

    // 水平スクロールバー作成
    hScrollbar = new Slider(this, SWT.HORIZONTAL|SWT.NO_FOCUS);
    hScrollbar.setValues (
      0, //int selection,
      0, // int minimum,
      100, //int maximum,
      10, //int thumb,
      1, //int increment,
      10 //int pageIncrement)
    );
    hsHeight = hScrollbar.computeSize(SWT.DEFAULT, SWT.DEFAULT, false).y;
    //if (log.isTraceEnabled())
      //log.trace("hsHeight " + hsHeight);
    hScrollbar.addSelectionListener(new SelectionAdapter(){
      @Override
      public void widgetSelected(SelectionEvent e) {
        hScrollTo(hScrollbar.getSelection());
      }
    });

    // 垂直スクロールバー作成
    vScrollbar = new Slider(this, SWT.VERTICAL|SWT.NO_FOCUS);
    vScrollbar.setValues (
      0, //int selection,
      0, // int minimum,
      100, //int maximum,
      10, //int thumb,
      1, //int increment,
      10 //int pageIncrement)
    );
    vsWidth  = vScrollbar.computeSize(SWT.DEFAULT, SWT.DEFAULT, false).x;
    //if (log.isTraceEnabled())
      //log.trace("vsWidth " + vsWidth);
    vScrollbar.addSelectionListener(new SelectionAdapter(){
      @Override
      public void widgetSelected(SelectionEvent e) {
        vScrollTo(vScrollbar.getSelection());
      }
    });


    // キャンバス作成
    canvas = new Canvas(this, 0);
//    canvas.addPaintListener(new PaintListener() {
//      public void paintControl(PaintEvent e) {
//        paintCanvas(e);
//      }
//    });
    canvas.setBounds(0, 0, 500, 500);

    // リサイズ時の動作
    addControlListener(new ControlAdapter() {
      public void controlResized(ControlEvent e) {
        ScrolledCanvas.this.resize(e);
      }
    });

    setFont(getFont());
  }

  /** キャンバスを取得する */
  public Canvas getCanvas() {
    return canvas;
  }

  /** リサイズ時のレイアウト */
  protected void resize(ControlEvent e) {

    // クライアント領域取得
    Rectangle avail = getClientArea();

    //if (log.isTraceEnabled())
      //log.trace("resize " + avail);

    // キャンバスの大きさを取得
    Rectangle cb = canvas.getBounds();
    //if (log.isTraceEnabled())
      //log.trace("cb " + cb);

    // キャンバスが入りきる場合 -------------------------------------------------
    if (cb.width <= avail.width && cb.height <= avail.height) {
      //if (log.isTraceEnabled()) log.trace("no scrollbar");

      if (cb.x != 0 || cb.y != 0) {
        canvas.setBounds(0, 0, cb.width, cb.height);
      }
      hScrollbar.setVisible(false);
      vScrollbar.setVisible(false);
      return;
    }

    // スクロールバーが必要であるか ---------------------------------------------
    boolean hOverflow = false;
    boolean vOverflow = false;

    if (avail.width < cb.width) {

      // 水平方向が入りきらない場合
      hOverflow = true;

      // 垂直方向は入りきるか
      if (avail.height < cb.height + hsHeight) {
        vOverflow = true;
      }

    } else if (avail.height < cb.height) {
      // 垂直方向が入りきらない場合
      vOverflow = true;

      // 水平方向は入りきるか
      if (avail.width < cb.width + vsWidth) {
        hOverflow = true;
      }
    }

    //if (log.isTraceEnabled())
      //log.trace("hOverflow " + hOverflow + ", vOverflow " + vOverflow);


    //-------------------------------------------------------------------------

    // キャンバスの使用可能な領域の計算
    Rectangle cArea = new Rectangle(avail.x, avail.y, avail.width, avail.height);
    if (hOverflow) cArea.height -= hsHeight;
    if (vOverflow) cArea.width -= vsWidth;

    //if (log.isTraceEnabled())
      //log.trace("cArea " + cArea + ", hOverflow:" + hOverflow + ", vOverflow:" + vOverflow);

    // キャンバス位置の補正
    boolean adjusted = false;
    if (cb.x < 0) {
      if (cb.x + cb.width < cArea.x + cArea.width) {
        cb.x = cArea.x + cArea.width - cb.width;
        adjusted = true;
      }
    }
    if (cb.y < 0) {
      if (cb.y + cb.height < cArea.y + cArea.height) {
        cb.y = cArea.y + cArea.height - cb.height;
        adjusted = true;
      }
    }

    if (adjusted) {
      canvas.setBounds(cb.x, cb.y, cb.width, cb.height);
    }

    // ------------------------------------------------------------------------

    // 水平スクロールバーのレイアウト
    if (hOverflow) {
      Rectangle bounds = new Rectangle(
          avail.x,
          avail.y + avail.height - hsHeight,
          vOverflow? avail.width - vsWidth:avail.width,
          hsHeight
      );
      hScrollbar.setBounds(bounds);
      hScrollbar.setValues (
          -cb.x,      // selection,
          0,           // minimum,
          cb.width,    // maximum,
          cArea.width, // thumb,
          SCROLLBAR_INC,           // increment,
          cArea.width  // pageIncrement)
      );
      //if (log.isTraceEnabled()) log.trace("hScrollbar bounds " + bounds);
    }
    hScrollbar.setVisible(hOverflow);

    // 垂直スクロールバーのレイアウト
    if (vOverflow) {
      Rectangle bounds = new Rectangle(
          avail.x + avail.width - vsWidth,
          avail.y,
          vsWidth,
          hOverflow? avail.height - hsHeight:avail.height
      );
      vScrollbar.setBounds(bounds);
      vScrollbar.setValues (
          -cb.y,      // selection,
          0,           // minimum,
          cb.height,    // maximum,
          cArea.height, // thumb,
         SCROLLBAR_INC ,           // increment,
          cArea.height  // pageIncrement)
      );

      //if (log.isTraceEnabled()) log.trace("vScrollbar bounds " + bounds);
    }
    vScrollbar.setVisible(vOverflow);
  }

//  /** キャンバスの描画 */
//  protected void paintCanvas(PaintEvent e) {
//
//    Rectangle avail = canvas.getClientArea();
//
//    if (log.isTraceEnabled()) log.trace("paintCanvas " + avail);
//
//    GC gc = e.gc;
//    gc.drawLine(avail.x, avail.y, avail.x + avail.width, avail.y + avail.height);
//
//
//    Font font = gc.getFont();
//    FontData fontData = font.getFontData()[0];
//
//
//    fontData.height = 100;
////    fontData.data.lfHeight = 100;
//    fontData.data.lfWidth = 20;
//
//    font = new Font(canvas.getDisplay(), fontData);
//
//
//
//
//    gc.setFont(font);
//    System.out.println("" + gc.textExtent("テストテキスト"));
//
//    gc.drawText("テストテキスト", 0, 0);
//
//    gc.drawLine(avail.x, avail.y + 50, avail.x + 1000, avail.y + 50);
//  }

  /** キャンバスのサイズを設定 */
  public void setCanvasSize(Point size) {
    assert(size.x >= 0 && size.y >= 0);
    canvas.setSize(size);
    resize(null);
  }

  public Point getCanvasSize() {
    return canvas.getSize();
  }

  public void showPoint(Point p) {
    setCanvasPosition(new Point(-p.x, -p.y));
  }

  /** キャンバスのトップ位置を設定 */
  public void setCanvasPosition(Point position) {
    assert(position.x <= 0 && position.y <= 0);
    Point size = canvas.getSize();
    canvas.setBounds(position.x, position.y, size.x, size.y);
    resize(null);
  }

  /** 水平スクロール */
  protected void hScrollTo(int selection) {
    //if (log.isTraceEnabled()) log.trace("hScrollTo " + selection);

    Rectangle cb = canvas.getBounds();
    cb.x = -selection;
    canvas.setBounds(cb);
  }

  /** 垂直スクロール */
  protected void vScrollTo(int selection) {
    //if (log.isTraceEnabled()) log.trace("vScrollTo " + selection);

    Rectangle cb = canvas.getBounds();
    cb.y = - selection;
    canvas.setBounds(cb);
  }
}
