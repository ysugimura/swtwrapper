package com.cm55.swt.draw;

import org.apache.commons.logging.*;
import org.eclipse.swt.*;
import org.eclipse.swt.events.*;
import org.eclipse.swt.graphics.*;
import org.eclipse.swt.widgets.*;

import com.cm55.swt.color.*;

/**
 * スクロールバー付描画エリア
 */
public class ScrolledDrawingArea extends Composite {
  private static final Log log = LogFactory.getLog(ScrolledDrawingArea.class);
  
  /** 水平スクロールバー */
  private Slider horScrollBar;

  /** 水平スクロールバーの占有高さ。非表示時は０ */
  private int horBarHeight;
  
  /** 垂直スクロールバー */
  private Slider verScrollBar;
  
  /** 垂直スクロールバーの占有幅 */
  private int verBarWidth;
  
  /** Drawing */
  private Drawing drawing;
  
  private Point dpi;
  
  private double scale;
  
  /** 描画のピクセル幅 */
  private Integer drawingPixelWidth;
  
  /** 描画のピクセル高さ */
  private Integer drawingPixelHeight;
  
  private int drawingPixelX;
  
  private int drawingPixelY;
  
  /** スケールの幅 */
  private int SCALE_WIDTH = 20;
  
  ScrolledDrawingArea(Composite parent) {
    this(parent, SWT.BORDER|SWT.NO_BACKGROUND);
    dpi = parent.getDisplay().getDPI();
  }
  
  /** グリッド作成 */
  ScrolledDrawingArea(Composite parent, int style) {
    super(parent, style);

    // 水平スクロールバー作成
    horScrollBar = new Slider(this, SWT.HORIZONTAL|SWT.NO_FOCUS);
    horScrollBar.setValues (
        0, //int selection,
        0, // int minimum,
        100, //int maximum,
        10, //int thumb,
        1, //int increment,
        10 //int pageIncrement)
    );
    Point horExtent = horScrollBar.computeSize(SWT.DEFAULT, SWT.DEFAULT, false);
    horBarHeight = horExtent.y;
    
    // 垂直スクロールバー作成
    verScrollBar = new Slider(this, SWT.VERTICAL|SWT.NO_FOCUS);
    verScrollBar.setValues (
      0, //int selection,
      0, // int minimum,
      100, //int maximum,
      10, //int thumb,
      1, //int increment,
      10 //int pageIncrement)
    );
    Point verExtent = verScrollBar.computeSize(SWT.DEFAULT, SWT.DEFAULT, false);
    verBarWidth = verExtent.x;
    
    if (log.isTraceEnabled()) log.trace("height/width" + horBarHeight + ", " + verBarWidth);
    
    // 
    addPaintListener(new PaintListener() {
      public void paintControl(PaintEvent e) {
        ScrolledDrawingArea.this.paint(e);
      }
    });
    
    // リサイズ時の動作指定
    addControlListener(new ControlAdapter() {
      public void controlResized(ControlEvent e) {
        ScrolledDrawingArea.this.resize(e);
      }
    });
    
    // スクロールバーが動作した時にスクロールする。
    horScrollBar.addSelectionListener(new SelectionAdapter(){
      @Override
      public void widgetSelected(SelectionEvent e) {
        horScrollTo(horScrollBar.getSelection());
      }
    });
    verScrollBar.addSelectionListener(new SelectionAdapter(){
      @Override
      public void widgetSelected(SelectionEvent e) {
        verScrollTo(verScrollBar.getSelection());
      }
    });
    
    // マウスによる手動スクロール
    final MouseMover mover = new MouseMover();
    addMouseListener(new MouseAdapter() {
      @Override public void mouseDown(MouseEvent e) { 
        if (e.button == 1) mover.start(e);
      }
      @Override public void mouseUp(MouseEvent e) {        
        if (e.button == 1) mover.stop(e);
      }
    });
    addMouseMoveListener(new MouseMoveListener() {
      public void mouseMove(MouseEvent e) {        
        mover.drag(e);
      }
    });    
    
    Cursor cursor = new Cursor(null, SWT.CURSOR_HAND);
    this.setCursor(cursor);
    
    // これが必要
    this.addDisposeListener(e->cursor.dispose());
  }
  
  private class MouseMover {
    boolean started = false;
    int prevx;
    int prevy;
    public void start(MouseEvent e) {
      started = true;
      if (log.isTraceEnabled()) log.trace("MouseMover.started");
      prevx = e.x;
      prevy = e.y;
    }
    public void drag(MouseEvent e) {
      if (!started) return;
      if (drawing == null) return;
      if (log.isTraceEnabled()) log.trace("MouseMover.moved");
      
      int movex = -(e.x - prevx);
      int movey = -(e.y - prevy);
      
      drawingPixelX = Math.max(0, drawingPixelX + movex);
      drawingPixelY = Math.max(0, drawingPixelY + movey);
      
      
      prevx = e.x;
      prevy = e.y;
      
      redraw();
      
    }
    public void stop(MouseEvent e) {
      if (!started) return;
      if (log.isTraceEnabled()) log.trace("MouseMover.stopped");
      started = false;
    }
  }
  
  public static final float MM_PER_INCH = 25.399F;
  
  public void setDrawing(Drawing drawing) {
    
    this.drawing = drawing;
    
    drawingPixelX = 0;
    drawingPixelY = 0;
    drawingPixelWidth = null;
    drawingPixelHeight = null;
    
    if (drawing != null) {
      scale = drawing.getScale();
      
      if (drawing.getWidth() != null) {
        // 幅指定あり、ピクセル数を取得
        drawingPixelWidth = (int)Math.round(dpi.x * scale * drawing.getWidth() / MM_PER_INCH);            
      }
      if (drawing.getHeight() != null) {
        // 高さ指定あり、ピクセル数を取得
        drawingPixelHeight = (int)Math.round(dpi.y * scale * drawing.getHeight() / MM_PER_INCH);
      }
    } else {
      scale = 1;
    }
    
    resize(null);
  }
  
  /** レイアウト */
  public void resize(ControlEvent e) {

    if (log.isTraceEnabled()) log.trace("resize");

    // 描画対象の無いとき
    if (drawing == null) {
      horScrollBar.setVisible(false);
      verScrollBar.setVisible(false);
      return;
    }

    // クライアント領域取得
    Rectangle clientArea = getClientArea();
    int areaLeft   = clientArea.x + SCALE_WIDTH;
    int areaTop    = clientArea.y + SCALE_WIDTH;    
    int areaRight  = clientArea.x + clientArea.width;
    int areaBottom = clientArea.y + clientArea.height;
    int areaWidth = areaRight - areaLeft;
    int areaHeight = areaBottom - areaTop;

    boolean needsHorBar, needsVerBar;

    // 幅指定なし、あるいは幅がクライアントエリアより大きい場合、水平スクロールバー    
    {      
      needsHorBar = drawingPixelWidth == null || drawingPixelWidth > areaWidth;
      if (needsHorBar) {
        areaHeight -= horBarHeight;
      }

      // 高さ指定なし、あるいは高さがクライアントエリアより大きい場合、垂直スクロールバー    
      needsVerBar = drawingPixelHeight == null || drawingPixelHeight > areaHeight;
      if (needsVerBar) {
        areaWidth -= verBarWidth;
      }

      if (!needsHorBar) {
        needsHorBar = drawingPixelWidth == null || drawingPixelWidth > areaWidth;
      }
    }
    if (log.isTraceEnabled()) log.trace("needs " + needsHorBar + ", " + needsVerBar);

    // 水平スクロールバーの設定
    if (needsHorBar) {
      horScrollBar.setBounds(
          areaLeft, 
          areaBottom - horBarHeight, 
          areaRight - areaLeft - (needsVerBar? verBarWidth:0), 
          horBarHeight
      );
      horScrollBar.setVisible(true);
      horScrollBar.setEnabled(drawingPixelWidth != null);
      if (drawingPixelWidth != null) {

        // ピクセル描画位置の補正
        if (0 < drawingPixelX && drawingPixelWidth - drawingPixelX < areaWidth) {
          drawingPixelX = areaWidth - drawingPixelWidth;
        }   

        // スクロールバーの設定
        horScrollBar.setValues (
            drawingPixelX, //int selection,
            0, // int minimum,
            drawingPixelWidth, //int maximum,
            areaWidth, //int thumb,
            areaWidth / 5, //int increment,
            areaWidth //int pageIncrement)
        );
      }

    } else {
      horScrollBar.setVisible(false);
    }

    // 垂直スクロールバーの設定
    if (needsVerBar) {
      verScrollBar.setBounds(
          areaRight - verBarWidth, 
          areaTop, 
          verBarWidth, 
          areaBottom - areaTop - (needsHorBar? horBarHeight:0)
      );      
      verScrollBar.setVisible(true);
      verScrollBar.setEnabled(drawingPixelHeight != null);
      if (drawingPixelHeight != null) {

        // ピクセル描画位置の補正
        if (0 < drawingPixelY && drawingPixelHeight - drawingPixelY < areaHeight) {
          drawingPixelY = areaHeight - drawingPixelHeight;
        }

        // スクロールバーの設定
        verScrollBar.setValues (
            drawingPixelY, //int selection,
            0, // int minimum,
            drawingPixelHeight, //int maximum,
            areaHeight, //int thumb,
            areaHeight / 5, //int increment,
            areaHeight //int pageIncrement)
        );
      }
    } else {
      verScrollBar.setVisible(false);
    }   
    
    redraw();
  }


  private void horScrollTo(int position) {
    if (log.isTraceEnabled()) log.trace("horScrollTo " + position);
    drawingPixelX = position;
    this.redraw();
  }
  
  private void verScrollTo(int position) {
    if (log.isTraceEnabled()) log.trace("verScrollTo " + position);
    drawingPixelY = position;
    this.redraw();
  }
  
  /** 描画イベント */
  private void paint(PaintEvent e) {
    
    Rectangle clientArea = getClientArea();
        
    if (drawing == null) {      
      e.gc.fillRectangle(clientArea);
      return;
    }
    
    // 横スケールの描画
    e.gc.fillRectangle(new Rectangle(clientArea.x, clientArea.y, clientArea.width, SCALE_WIDTH));
    new ScalePainter() {
      protected void paintPosition(GC gc, int pixel, int ticklen) {
        gc.drawLine(pixel, SCALE_WIDTH - 1 - ticklen, pixel, SCALE_WIDTH - 1);
      }
    }.paint(e.gc, drawingPixelX, clientArea.x, clientArea.width, dpi.x);
    
    // 縦スケールの描画
    e.gc.fillRectangle(new Rectangle(clientArea.x, clientArea.y, SCALE_WIDTH, clientArea.height));
    new ScalePainter() {
      protected void paintPosition(GC gc, int pixel, int ticklen) {
        gc.drawLine(SCALE_WIDTH - 1 - ticklen, pixel, SCALE_WIDTH - 1, pixel);
      }
    }.paint(e.gc, drawingPixelY, clientArea.y, clientArea.height, dpi.y);
    
  
    int x = clientArea.x + SCALE_WIDTH;
    int y = clientArea.y + SCALE_WIDTH;
    e.gc.setClipping(new Rectangle(
        x, y, clientArea.width, clientArea.height
    ));  
    
    // 背景の描画
    {
      int width = clientArea.width;
      int height = clientArea.height;
      e.gc.setBackground(ColorStock.white);
      if (drawingPixelWidth != null) {
        width= drawingPixelWidth - drawingPixelX;
      }
      if (drawingPixelHeight != null) {
        height = drawingPixelHeight - drawingPixelY;
      }      
      e.gc.fillRectangle(x, y, width, height);      
    }
        
    // 対象の描画
    drawing.draw(e.gc, new Point(SCALE_WIDTH - drawingPixelX, SCALE_WIDTH - drawingPixelY));
  }
  
  private abstract class ScalePainter {
    public void paint(GC gc, int contentOffset, int clientOffset, int clientSize, int dpi) {
      
      // mmの目盛り描画
      double pixelsPerMM = dpi / MM_PER_INCH;
      if (log.isTraceEnabled()) log.trace("" + pixelsPerMM);
      int startMM = (int)Math.ceil(MM_PER_INCH * contentOffset / dpi / scale);  
      
      if (pixelsPerMM >= 3) {              
        for (int mm = startMM; ; mm++) {
          int pixel = clientOffset + (int)Math.round(mm * dpi * scale / MM_PER_INCH) - contentOffset + SCALE_WIDTH;
          
          if (log.isTraceEnabled()) log.trace("mm:" + mm + ", pixel:" + pixel);
          
          if (clientOffset + clientSize < pixel) break;
          paintPosition(gc, pixel, 2);
        }
      }
      
      
      // 10mmの目盛り描画
      if (pixelsPerMM * 10 > 3) {
        int mm = startMM + 9;
        mm = mm - (mm % 10);    
        for (; ; mm += 10) {
          int pixel = clientOffset + (int)Math.round(mm * dpi * scale / MM_PER_INCH) - contentOffset + SCALE_WIDTH;
          
          if (log.isTraceEnabled()) log.trace("mm:" + mm + ", pixel:" + pixel);
          
          if (clientOffset + clientSize < pixel) break;
          paintPosition(gc, pixel, 7);
        }
      }
      
      // 100mmの目盛り描画 
      if (pixelsPerMM * 100 > 3) {
        int mm = startMM + 99;
        mm = mm - (mm % 100);    
        for (; ; mm += 100) {
          int pixel = clientOffset + (int)Math.round(mm * dpi * scale / MM_PER_INCH) - contentOffset + SCALE_WIDTH;
          
          if (log.isTraceEnabled()) log.trace("mm:" + mm + ", pixel:" + pixel);
          
          if (clientOffset + clientSize < pixel) break;
          paintPosition(gc, pixel, 10);
        }
      }      
    }
    protected abstract void paintPosition(GC gc, int position, int ticklen);
  }

  
  /////////////////////////////////////////////////////////////////////////////
  // Drawing
  /////////////////////////////////////////////////////////////////////////////
  
  public static interface Drawing {
    
    /** 描画の幅をmmで取得。不定の場合はnullでもよい */
    public Double getWidth();
    
    /** 描画の高さをmmで取得。不定の場合はnullでもよい */
    public Double getHeight();
    
    public double getScale();
    
    
    /** 指定されたGCを使い、offsetをオフセットとして描画 */
    public void draw(GC gc, Point offset);
  }
  

}
