// Created by Cryptomedia Co., Ltd. 2006/05/31
package com.cm55.swt.draw;

import org.eclipse.swt.events.*;
import org.eclipse.swt.graphics.*;
import org.eclipse.swt.widgets.*;

import com.cm55.swt.*;

/**
 * SWTの{@link Canvas}のラッパ
 * 描画イベントによらずに一部描画を行うには、以下のようにする。
 * <pre>
 * public void redrawCanvas (Canvas canvas) {
 *   GC gc = new GC(canvas);
 *   gc.setClipping(90,90,60,60);
 *   gc.drawRectangle(90,90,30,30);
 *   gc.dispose();
 * }
 * </pre>
 * 
 * @author ysugimura
 */
public class SwCanvas extends SwControl<Canvas> {

  @Override
  public Canvas doCreate(Composite parent) {
    final Canvas canvas = new Canvas(parent, 0);
    
    // キャンバスの描画イベントをフックし、独自のイベントを発行する
    canvas.addPaintListener(new PaintListener() {
      public void paintControl(org.eclipse.swt.events.PaintEvent e) {
        Rectangle ca = canvas.getClientArea();
        dispatchEvent(new PaintEvent(
            e.gc,
            ca,
            new Rectangle(e.x, e.y, e.width, e.height)
         ));
      }
    });
    return canvas;
  }

  /** 描画イベント */
  public static class PaintEvent {

    /** 描画用グラフィックコンテキスト 。これは描画イベントの間だけ提供されることに注意。*/
    public final GC gc;
    
    /** クライアント領域 */
    public final Rectangle clientArea;
    
    /** 描画領域 */
    public final Rectangle paint;
    
    private PaintEvent(GC gc, Rectangle clientArea, Rectangle paint) {
      this.gc = gc;
      this.clientArea = clientArea;
      this.paint = paint;
    }

    public String toString() {
      return "PaintEvent clientArea:" + clientArea + ", paint:" + paint;
    }
  }

  /** 全体の再描画を支持する。描画イベントが発行される。 */
  public void redraw() {
    control.redraw();
  }
}
