// Created by Cryptomedia Co., Ltd. 2005/11/14
package com.cm55.swt.draw;

import java.io.*;

import org.eclipse.swt.*;
import org.eclipse.swt.dnd.*;
import org.eclipse.swt.events.*;
import org.eclipse.swt.graphics.*;
import org.eclipse.swt.widgets.*;

import com.cm55.swt.*;


/**
 * イメージ表示用キャンバス
 */
public class SwImageCanvas extends SwControl<Canvas> {

  protected int style = SWT.NULL;
  protected Point imageSize;
  
  /** falseの場合は表示のみでドロップを受け付けない */
  protected DropAcceptor dropAcceptor;

  /** 表示の際にアスペクト比を保持する */
  protected boolean saveAspect = true;

  /** 作成時 */
  public Canvas doCreate(Composite parent) {
    Canvas canvas = new Canvas(parent, style);
    canvas.addPaintListener(new PaintListener() {
      public void paintControl(PaintEvent e) {
        SwImageCanvas.this.paint(e);
      }
    });
    canvas.addDisposeListener(new DisposeListener() {
      public void widgetDisposed(DisposeEvent e) {
        SwImageCanvas.this.disposed(e);
      }
    });
    setupDragDrop(canvas);
    return canvas;
  }

  /** ドロップを受け付けるか */
  public void setDropAcceptor(DropAcceptor d) {
    dropAcceptor = d;
  }

  /** 転送タイプ */
  protected Transfer transferType = FileTransfer.getInstance();

  /** ドラッグドロップのセットアップ */
  protected void setupDragDrop(Canvas canvas) {
    Transfer[] types = new Transfer[] { transferType };

    // ドロップターゲットのセットアップ
    DropTarget dropTarget = new DropTarget(canvas, DND.DROP_COPY);
    dropTarget.setTransfer(types);
    dropTarget.addDropListener(new CanvasDropTarget());
  }

  /** ドロップターゲット */
  protected class CanvasDropTarget extends DropTargetAdapter {

    /** ドラッグエンター */
    public void dragEnter(DropTargetEvent event){
      if (dropAcceptor == null) return;

      if (transferType.isSupportedType(event.currentDataType))
        event.detail = DND.DROP_COPY;
    }

    /** ドロップ */
    public void drop(DropTargetEvent event){

      //if (log.isTraceEnabled()) log.trace("drop " + event.data);

      // 複数のファイルパスを取得
      String[]filePaths = (String[])event.data;

      // 複数ファイルパスのうち「読み込み可能な」最初のものを取得
      for (String filePath: filePaths) {
        File file = new File(filePath);
        if (file.isDirectory()) continue;
        if (!file.exists()) continue;

        /*
        if (setImageFromFile(file)) return;
        break;
        */
        if (dropAcceptor != null) {
          dropAcceptor.acceptDroppedFile(file);
        }
        break;
      }
    }
  }

  /** 画像バイトを設定する。設定できた場合はtrue */
  public boolean setImageBytes(byte[]bytes) {
    imageSize = null;
    if (bytes == null) {
      setImageDescriptor(null);      
      return true;
    }
    ByteArrayInputStream in = new ByteArrayInputStream(bytes);
    try {
      ImageData imageData = new ImageData(in);
      imageSize = new Point(imageData.width, imageData.height);
      if (setImageDescriptor(org.eclipse.jface.resource.ImageDescriptor.createFromImageData(imageData))) {
        return true;
      }
    } catch (OutOfMemoryError er) {
      // 画像が大きすぎる？
    } catch (Exception ex) {
    } finally {
      try {
        in.close();
      } catch (Exception ex) {}
    }
    return false;
  }

  public Point getImageSize() {
    return imageSize;
  }
  

  ////////////////////////////////////////////////////////////////////////////

  /** 設定中のイメージ */
  protected Image image;

  /** イメージを設定する。実際にはImageDescriptorを与える。
   * イメージが設定された場合はtrueを返す。 */
  private boolean setImageDescriptor(org.eclipse.jface.resource.ImageDescriptor imageDesc) {

    Image newImage = null;
    if (imageDesc != null) {
      try {
        newImage = imageDesc.createImage();
      } catch (Exception ex) {
        return false;
      }
    }

    if (image != null) {
      image.dispose();
    }
    image = newImage;
    control.redraw();
    return true;
  }

  /** イメージを持っているか？ */
  public boolean hasImage() {
    return image != null;
  }

  /** 描画 */
  public void paint(PaintEvent e) {
    GC gc = e.gc;
    Rectangle clientArea = control.getClientArea();

    // 表示画像の無い場合
    if (image == null) {
      String text = "画像はありません";
      Font font = gc.getFont();
      Point extent = gc.stringExtent(text);
      gc.drawText(text,
          clientArea.x + clientArea.width / 2 - extent.x / 2,
          clientArea.y + clientArea.height / 2 - extent.y / 2
      );
      return;
    }

    // 画像を描画する

    Rectangle imageBounds = image.getBounds();

    // アスペクト比を無視
    if (!saveAspect) {
      gc.drawImage(image,
        imageBounds.x,
        imageBounds.y,
        imageBounds.width,
        imageBounds.height,
        clientArea.x,
        clientArea.y,
        clientArea.width,
        clientArea.height
      );
    }

    int drawWidth;
    int drawHeight;

    // imageHeight   clientHeight
    // ----------- < ------------
    // imageWidth    clientWidth
    // のときは、クライアント幅に合わせる。そうでない場合はクライアント高さ
    if (imageBounds.height * clientArea.width < clientArea.height * imageBounds.width) {
      drawWidth = clientArea.width;
      drawHeight = drawWidth * imageBounds.height / imageBounds.width;
    } else {
      drawHeight = clientArea.height;
      drawWidth = drawHeight * imageBounds.width / imageBounds.height;
    }

    gc.drawImage(image,
        imageBounds.x,
        imageBounds.y,
        imageBounds.width,
        imageBounds.height,
        (clientArea.width - drawWidth) / 2,
        (clientArea.height - drawHeight) / 2,
        drawWidth,
        drawHeight
      );
  }

  /** キャンバス廃棄時 */
  public void disposed(DisposeEvent e) {
    if (image != null) {
      image.dispose();
      image = null;
    }
  }

  /////////////////////////////////////////////////////////////////////////////

  public static interface DropAcceptor {
    public boolean acceptDroppedFile(File file);
  }

}
