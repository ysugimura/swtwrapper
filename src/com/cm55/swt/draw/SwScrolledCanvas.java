// Created by Cryptomedia Co., Ltd. 2006/05/31
package com.cm55.swt.draw;

import org.eclipse.swt.events.*;
import org.eclipse.swt.graphics.*;
import org.eclipse.swt.widgets.*;

import com.cm55.swt.*;
import com.cm55.swt.color.*;


public class SwScrolledCanvas extends SwControl<ScrolledCanvas> {

  public SwScrolledCanvas() {
  }

  /** 作成 */
  @Override
  public ScrolledCanvas doCreate(Composite parent) {
    ScrolledCanvas scrolledCanvas = new ScrolledCanvas(parent, 0);

    final Canvas canvas = scrolledCanvas.getCanvas();
    canvas.addPaintListener(new PaintListener() {
      public void paintControl(org.eclipse.swt.events.PaintEvent e) {
        Rectangle ca = canvas.getClientArea();
        e.gc.setBackground(ColorStock.white);
        e.gc.fillRectangle(ca);
        e.gc.setForeground(ColorStock.black);
        dispatchEvent(new PaintEvent(
            e.gc,
            ca, //canvas.getClientArea(),
            new Rectangle(e.x, e.y, e.width, e.height)
         ));
      }
    });
    return scrolledCanvas;
  }

  /** 描画イベント */
  public static class PaintEvent {
    public GC gc;
    public Rectangle clientArea;
    public Rectangle paint;
    private PaintEvent(GC gc, Rectangle clientArea, Rectangle paint) {
      this.gc = gc;
      this.clientArea = clientArea;
      this.paint = paint;
    }

    public String toString() {
      return "PaintEvent clientArea:" + clientArea + ", paint:" + paint;
    }
  }

  /** キャンバス位置を設定 */
  public void setCanvasPosition(Point position) {
    control.setCanvasPosition(position);
  }

  /** キャンバスサイズを設定 */
  public void setCanvasSize(Point p) {
    control.setCanvasSize(p);
  }

  /** キャンバスサイズを取得 */
  public Point getCanvasSize() {
    return control.getCanvasSize();
  }

  /** 指定点を表示する */
  public void showPoint(Point p) {
    control.showPoint(p);
  }
}
