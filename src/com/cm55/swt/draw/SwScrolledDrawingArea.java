package com.cm55.swt.draw;

import org.apache.commons.logging.*;
import org.eclipse.swt.widgets.*;

import com.cm55.swt.*;

public class SwScrolledDrawingArea extends SwControl<ScrolledDrawingArea> {

  private static final Log log = LogFactory.getLog(SwScrolledDrawingArea.class);


  public SwScrolledDrawingArea() {}
  
  public ScrolledDrawingArea doCreate(Composite parent) {
    return new ScrolledDrawingArea(parent);
  }
  
  public void setDrawing(ScrolledDrawingArea.Drawing drawing) {
    control.setDrawing(drawing);    
  }
  
//  public static class TestPanel extends GenEditPanel {
//    
//    protected CustomScrolledDrawingArea area;
//    
//    public TestPanel() {
//      panelItem = new UDBorderLayout.V(
//          null,
//          area = new CustomScrolledDrawingArea(),
//          null
//      ));
//    }
//    @Override protected void sessionSet() {      
//    }    
//  }
//  
//  public static class Dialog extends GenEditPanel.Dialog<TestPanel> {
//    public Dialog(Control control) {
//      super(control, "テスト", new TestPanel());
//    }
//  }
}
