package com.cm55.swt.event;

import com.cm55.swt.*;

public class SwModifiedEvent {

  public final SwControl<?> control;
  
  public SwModifiedEvent(SwControl<?> control) {
    this.control = control;
  }

}
