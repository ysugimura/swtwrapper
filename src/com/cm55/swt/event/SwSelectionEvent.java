package com.cm55.swt.event;

import org.eclipse.swt.events.*;
import org.eclipse.swt.widgets.*;

import com.cm55.swt.*;

public class SwSelectionEvent {

  public final SwControl<?> control;
  public final SelectionEvent e;
  public SwSelectionEvent(SwControl<?> control, SelectionEvent e) {
    this.control = control;
    this.e = e;
  }

  public Object getSource() {
    return e.getSource();
  }
  
  public Widget getWidget() {
    return e.widget;
  }
}
