package com.cm55.swt.grid;

import java.util.*;

import org.eclipse.swt.*;
import org.eclipse.swt.events.*;
import org.eclipse.swt.graphics.*;
import org.eclipse.swt.widgets.*;

import com.cm55.swt.color.*;
import com.cm55.swt.misc.*;
import com.cm55.swt.text.*;

/**
 * グリッド
 */
public class Grid extends Composite {

  //private static final Log log = LogFactory.getLog(Grid.class);

  public static final String ELLIPSIS = "…";

  /** 垂直スクロールバー */
  protected Slider scrollBar;

  /** 垂直スクロールバーの占有幅。非表示時は０ */
  protected int scrollBarWidth;

  /** 行数 */
  protected int rowCount;

  /** 列数 */
  protected int colCount;

  /** ヘッダ高さ */
  protected int headerHeight;

  /** 行高さ */
  protected int rowHeight;

  /** フォント高さ */
  protected int fontHeight;

  /** 水平線太さ */
  protected int horLineWidth = 1;

  /** 垂直線太さ */
  protected int verLineWidth = 1;

  /** 先頭行 */
  protected int topRow;

  /** 可視行数 */
  protected int visibleRows;

  /** カーソル行位置 */
  protected int selectionRow = -1;

  /** カーソル列位置 */
  protected int selectionCol = -1;

  /** 現在のエディタ */
  protected Editor currentEditor;

  /** 行の選択反転 */
  protected boolean rowSelection;

  /** フォーカスロスト時に自動フラッシュ */
  protected boolean flushEditorOnFocusLost;

  /** ラップ選択 */
  protected boolean wrapSelection = true;

  /** 列定義 */
  protected Columns columns;

  /** RETURNキーでカーソル移動 */
  //protected boolean moveOnReturn = true;

  /** マウスエンター時にフォーカスを当てる */
  protected boolean focusOnMouseEnter = false;

  public Grid(Composite parent) {
    this(parent, SWT.BORDER|SWT.NO_BACKGROUND);
    flushEditorOnFocusLost = true;
  }

  /** グリッド作成 */
  public Grid(Composite parent, int style) {
    super(parent, style);

    // 垂直スクロールバー作成
    scrollBar = new Slider(this, SWT.VERTICAL|SWT.NO_FOCUS);
    scrollBar.setValues (
      0, //int selection,
      0, // int minimum,
      100, //int maximum,
      10, //int thumb,
      1, //int increment,
      10 //int pageIncrement)
    );

    SWT a;
    // スクロールバーが動作した時にスクロールする。
    scrollBar.addSelectionListener(new SelectionAdapter(){
      @Override
      public void widgetSelected(SelectionEvent e) {
        scrollTo(scrollBar.getSelection());
      }
    });

    // フォーカスリスナの設定
    addFocusListener(new FocusListener() {
      public void focusGained(FocusEvent e) {
        Grid.this.focusGained(e);
      }
      public void focusLost(FocusEvent e) {
        Grid.this.focusLost(e);
      }
    });


    // Gridリサイズ時の動作指定
    addControlListener(new ControlAdapter() {
      public void controlResized(ControlEvent e) {
        Grid.this.resize(e);
      }
    });

    // ペイントイベント受信
    addPaintListener(new PaintListener() {
      public void paintControl(PaintEvent e) {
        Grid.this.paint(e);
      }
    });

    // マウスリスナ設定
    addMouseListener(new MouseAdapter() {
      public void mouseDown(MouseEvent e) {
        Grid.this.mouseDown(e);
      }
      public void mouseUp(MouseEvent e) {
        Grid.this.mouseUp(e);
      }
      public void mouseDoubleClick(MouseEvent e) {
        Grid.this.mouseDoubleClick(e);
      }
    });
    addMouseMoveListener(new MouseMoveListener() {
      public void mouseMove(MouseEvent e) {
        Grid.this.mouseMove(e);
      }
    });

    // キーボードリスナ設定
    addKeyListener (new KeyAdapter() {
      public void keyPressed(KeyEvent e) {
        Grid.this.keyPressed(e);
      }
    });

    // マウスホイールリスナ設定
    addListener(SWT.MouseWheel, new Listener(){
      public void handleEvent (Event event) {
        scroll(-event.count);
        /*
        int c = event.count;
        while (c < 0) {
          scrollDown();
          c++;
        }
        while (c > 0) {
          scrollUp();
          c--;
        }
        */
      }
    });


    // マウスエンター時にフォーカスを当てる
    addMouseTrackListener(new MouseTrackAdapter() {
      public void mouseEnter(MouseEvent e) {
        if (focusOnMouseEnter)
          forceFocus();
      }
    });


    setFont(getFont());
  }


  /** マウスエンター時にフォーカスを当てる */
  public void setFocusOnMouseEnter(boolean value) {
    focusOnMouseEnter = value;
  }

  /** スクロール */
  public void scroll(int c) {
    while (c < 0) {
      scrollUp();
      c++;
    }
    while (c > 0) {
      scrollDown();
      c--;
    }
  }

  public void setFont(Font font) {
    //ystem.out.println("Grid#setFont " + font);
    super.setFont(font);

    // フォント高さを取得
    GC gc = new GC(this);
    gc.setFont(getFont());
    Point extent = gc.stringExtent("漢g");
    fontHeight = extent.y;
    gc.dispose();

    // デフォルトヘッダ高さ・行高さを設定
    headerHeight = fontHeight + 4;
    rowHeight = fontHeight + 4;

  }

  public Font getFont() {
    //ystem.out.println("Grid#getFont " + super.getFont());
    return super.getFont();
  }

  protected void keyPressed(KeyEvent e) {
    //if (log.isTraceEnabled())
      //log.trace("keyPresssed " + e);

    if (colCount <= 0 || rowCount <= 0) return;
    boolean processed = false;
    if (selectionRow >= 0 && selectionCol >= 0) {
      processed = Grid.this.keyPressed(e, selectionRow, selectionCol);
      //if (log.isTraceEnabled())
        //log.trace("processed ? " + processed);
    }

    if (processed) return;

    //if (log.isTraceEnabled())
      //log.trace("keyCode " + e.keyCode);

    switch (e.keyCode) {
    case SWT.PAGE_UP: pageUp(); break;
    case SWT.PAGE_DOWN: pageDown(); break;
    case SWT.ARROW_LEFT:  moveSelection(SWT.LEFT); break;
    case SWT.KEYPAD_CR:
    case '\r': //if (!moveOnReturn) break;
    case '\t':
    case SWT.ARROW_RIGHT: moveSelection(SWT.RIGHT); break;
    case SWT.ARROW_UP:    moveSelection(SWT.UP); break;
    case SWT.ARROW_DOWN:  moveSelection(SWT.DOWN); break;
    }
  }

  protected void focusGained(FocusEvent e) {
  }

  protected void focusLost(FocusEvent e) {
  }

  /** マウスダウン */
  protected void mouseDown(MouseEvent e) {
    // フォーカスを置く
    setFocus();

    // マウスクリック地点のセルを探し、選択する。
    Point p = getCellAtPoint(e.x, e.y);
    if (p == null) return;
    int row = p.y;
    int col = p.x;
    //if (log.isTraceEnabled()) {
      //log.trace("mouseDown " + row + "," + col);
    //}
    setSelection(row,  col);

    if (row >= 0 && col >= 0) {
      Column column = columns.getColumn(col);
      column.clicked(e.button, row);
    }

    // セルクリック
    cellClicked(new Point(e.x, e.y), row, col);
  }

  /** マウスアップ */
  protected void mouseUp(MouseEvent e) {
  }

  /** マウスダブルクリック */
  protected void mouseDoubleClick(MouseEvent e) {
  }

  /** マウス移動 */
  protected void mouseMove(MouseEvent e) {
  }

  /** CompositeのsetFocus処理を修正する。*/
  @Override
  public boolean setFocus() {
    if (currentEditor != null)
      if (currentEditor.setFocus()) return true;
    return forceFocus();
  }

  public void setEnabled(boolean value) {
    if (getEnabled() == value) return;
    super.setEnabled(value);
    redraw();
  }

  /** 行数を設定する */
  public void setRowCount(int count) {
    // 行数同じ、何もしない
    if (rowCount == count) {
      redraw();
      return;
    }

    // 行数設定
    if (count < 0) new IllegalArgumentException();
    rowCount = count;

    // 現在選択行が行範囲外の場合。最終行を選択する。
    if (rowCount <= selectionRow) {
      int newRow = rowCount - 1;
      if (canSelectCell(newRow, selectionCol)) {
        // セルが選択可能な場合はそれを選択
        setSelection(newRow, selectionCol);
      } else {
        // そうでなければ選択なし
        setSelection(-1, -1);
      }
    }

    // レイアウト
    resize(null);
  }

  /** 行数を取得する */
  public int getRowCount() {
    return rowCount;
  }

  /** 列数を設定する */
  public void setColCount(int count) {
    if (colCount == count) return;
    if (count <= 0) throw new IllegalArgumentException();
    colCount = count;
    resize(null);
  }

  /** 列配列を設定する */
  public void setColumns(Column[]columnArray) {
    setColumns(new Columns(columnArray));
  }

  /** 列オブジェクトを設定する */
  public void setColumns(Columns columns) {
    this.columns = columns;
    columns.setGrid(this);
    setColCount(columns.getCount());
  }

  /** 列オブジェクトを取得する */
  public Columns getColumns() {
    return columns;
  }

  /** 行高さ設定 */
  public void setRowHeight(int h) {
    if (rowHeight == h) return;
    rowHeight = h;
    resize(null);
  }

  /** 行高さ取得 */
  public int getRowHeight() {
    return rowHeight;
  }

  /** ヘッダ高さ設定 */
  public void setHeaderHeight(int h) {
    if (headerHeight == h) return;
    headerHeight = h;
    resize(null);
  }

  /** ヘッダ高さ取得 */
  public int getHeaderHeight() {
    return headerHeight;
  }

  /** 選択行取得 */
  public int getSelectionRow() {
    return selectionRow;
  }

  /** 選択列取得 */
  public int getSelectionCol() {
    return selectionCol;
  }

  /** 行選択モード設定 */
  public void setRowSelection(boolean value) {
    if (rowSelection == value) return;
    rowSelection = value;
    redrawRow(selectionRow);
  }

  /** 行選択モード取得 */
  public boolean isRowSelection() {
    return rowSelection;
  }

  /** 選択ラップモード設定 */
  public void setWrapSelection(boolean value) {
    wrapSelection = value;
  }

  /** 選択ラップモード取得 */
  public boolean isWrapSelection() {
    return wrapSelection;
  }

  /////////////////////////////////////////////////////////////////////////////

  /** 各列サイズを取得する */
  protected int[]getColWidths(int size) {
    //if (size != 563) Thread.currentThread().dumpStack();
    //ystem.out.println("getColWidths " + size);
    if (columns != null) return columns.getColWidths(this, size);

    int[]result = new int[colCount];
    int oneSize = size / colCount;
    for (int i = 0; i < result.length; i++)
      result[i] = oneSize;
    result[result.length - 1] += size - (oneSize * colCount);
    return result;
  }


  /** リサイズ時のレイアウト */
  protected void resize(ControlEvent e) {

    // クライアント領域取得
    Rectangle clientArea = getClientArea();
    int left   = clientArea.x;
    int top    = clientArea.y;
    int right  = left + clientArea.width;
    int bottom = top + clientArea.height;

    // ヘッダとセル内容すべてを表示した場合のトータルの高さを計算
    int totalHeight =
      (headerHeight + horLineWidth) + (rowHeight + horLineWidth) * rowCount;

    // 可視行数計算
    visibleRows = (clientArea.height - (headerHeight + horLineWidth))
      / (rowHeight + horLineWidth);

    // topRowを調整
    topRow = Math.min(
      topRow,
      Math.max(0, rowCount - visibleRows)
    );

    // スクロールバーレイアウト
    if (clientArea.height < totalHeight) {

      // スクロールバーのレイアウト
      Point verExtent = scrollBar.computeSize(SWT.DEFAULT, SWT.DEFAULT, false);
      scrollBar.setBounds(right - verExtent.x, top, verExtent.x, clientArea.height);

      scrollBar.setValues (
        topRow,      // selection,
        0,           // minimum,
        rowCount,    // maximum,
        visibleRows, // thumb,
        1,           // increment,
        visibleRows  // pageIncrement)
      );

      scrollBarWidth = verExtent.x;
      scrollBar.setVisible(true);
      //if (log.isTraceEnabled()) log.trace("scrollBar visible");
    } else {
      scrollBarWidth = 0;
      scrollBar.setVisible(false);
      //if (log.isTraceEnabled()) log.trace("scrollBar not visible");
    }

    // エディタがある場合に、ポジショニング
    positionEditor();

    // 再描画
    redraw();
  }

  // ジオメトリ ///////////////////////////////////////////////////////////////

  /** セル範囲の取得。*/
  public Rectangle getCellRect(int row, int col) {
    Rectangle cellArea = getCellArea();

    // 全列の幅から所望の列の位置を取得
    int[]colWidths = getColWidths(cellArea.width - verLineWidth * (colCount - 1));
    int x = cellArea.x;
    for (int c = 0; c < col; c++)
      x += colWidths[c] + verLineWidth;

    // 所望の行の位置を取得
    int y = cellArea.y + (row - topRow) * (rowHeight + horLineWidth);

    return new Rectangle(x, y, colWidths[col], rowHeight);
  }

  /** 行範囲の取得 */
  protected Rectangle getRowRect(int row) {
    Rectangle cellArea = getCellArea();

    // 所望の行の位置を取得
    int y = cellArea.y + (row - topRow) * (rowHeight + horLineWidth);

    return new Rectangle(cellArea.x, y, cellArea.width, rowHeight);
  }

  /** 指定座標位置のセルを求める。セル内でなければnull */
  public Point getCellAtPoint(int x, int y) {

    // ０列の場合、何も描画しない
    if (colCount <= 0) return null;

    // クライアント領域を求める
    Rectangle clientArea = getPaneArea();
    int client_left = clientArea.x;
    int client_top = clientArea.y;
    int client_right = clientArea.x + clientArea.width;
    //if (log.isTraceEnabled()) log.trace("clientArea " + clientArea);

    // x,y補正
    x -= client_left;
    y -= client_top;

    // y位置はヘッダ領域か
    if (y < headerHeight + horLineWidth) {
      return null;
    }
    y -= headerHeight + horLineWidth;

    // 行描画高さを計算
    int lineHeight = rowHeight + horLineWidth;
    int row = topRow + y / lineHeight;
    if (row >= rowCount) return null;


    // 各列サイズを取得する
    int[]colWidths = getColWidths(clientArea.width - verLineWidth * (colCount - 1));
    for (int col = 0; col < colCount; col++) {
      if (x < colWidths[col] + verLineWidth) {
        return new Point(col, row);
      }
      x -= colWidths[col] + verLineWidth;
    }
    return null;
  }

  /** clientAreaからスクロールバー部分を除いたもの */
  protected Rectangle getPaneArea() {
    Rectangle result = getClientArea();
    if (scrollBar.isVisible()) result.width -= scrollBarWidth;

    //ystem.out.println("getPaneArea " + result);

    return result;
  }

  /** paneAreaからヘッダ部を除いたもの */
  protected Rectangle getCellArea() {
    Rectangle result = getPaneArea();
    result.y += headerHeight + horLineWidth;
    return result;
  }

  /** paneAreaのヘッダ部分*/
  protected Rectangle getHeaderArea() {
    Rectangle result = getPaneArea();
    result.height = headerHeight;
    return result;
  }

  // 選択 /////////////////////////////////////////////////////////////////////

  /** 指定セルを選択する */
  public void setSelection(int row, int col) {

    //if (log.isTraceEnabled()) {
    //  log.trace("setSelection " + row + " " + col);
    //}

    // 既に選択中
    if (row == selectionRow && col == selectionCol)
      return;

    // このセルが選択可能か
    if (!canSelectCell(row, col)) return;

    // 以前のエディタがあればフラッシュする。フラッシュできるなら閉じる。
    // フラッシュできなければ選択中止。
    if (!flushEditor()) return;
    closeEditor();

    if (rowSelection) {
      // 行選択モードの場合、行が変わるならば二つとも再描画。そうでなければ
      // 再描画不要
      if (selectionRow != row) {
        if (selectionRow >= 0) redrawRow(selectionRow);
        selectionRow = row;
        selectionCol = col;
        if (selectionRow >= 0) redrawRow(selectionRow);
      }
    } else {
      // セル選択モードの場合、二つのセルを再描画
      if (selectionRow >= 0 && selectionCol >= 0)
        redrawCell(selectionRow, selectionCol);
      selectionRow = row;
      selectionCol = col;
      if (selectionRow >= 0 && selectionCol >= 0)
        redrawCell(selectionRow, selectionCol);
    }

    // このセルを可視領域に移動
    if (row >= 0 && col >= 0)
      makeCellVisible(row, col);

    // このセル用のエディタがあればオープン
    openEditor();

    // UDSelectionEventを送信
    fireSelectionChanged();
  }

  /** 選択を移動する。移動した場合はtrueを返す。*/
  public boolean moveSelection(int direction) {

    //if (log.isTraceEnabled()) log.trace("moveSelection " + direction);

    int newRow = selectionRow;
    int newCol = selectionCol;

    // 何も選択していない場合
    if (newRow < 0 || newCol < 0) {
      if (rowCount <= 0 || colCount <= 0) return false;

      // 検索開始位置を設定する
      switch (direction) {
      case SWT.RIGHT:
      case SWT.DOWN:
        newRow = 0;
        newCol = 0;
        break;
      default:
        newRow = rowCount - 1;
        newCol = colCount - 1;
        break;
      }
      // 初期位置が選択可能であれば選択する。
      if (canSelectCell(newRow, newCol)) {
        setSelection(newRow, newCol);
        return true;
      }
    }

    // 移動
    switch (direction) {
    case SWT.LEFT: // 左へ移動、wrapSelection時は前行へ
      if (wrapSelection)
        while (true) {
          if (0 < newCol) newCol--; // 左端でない
          else if (0 < newRow) { newCol = colCount - 1; newRow--; } // 上端でない
          else {
            // 左端かつ上端である。
            newRow = -1;
            newCol = -1;
            break;
          }
          if (canSelectCell(newRow, newCol)) break;
        }
      else
        while (true) {
          if (0 < newCol) newCol--; // 左端でない
          else {
            // 左端である
            newRow = -1;
            newCol = -1;
            break;
          }
          if (canSelectCell(newRow, newCol)) break;
        }
      break;
    case SWT.RIGHT: // 右へ移動、wrapSelection時は後行へ
      //ystem.out.println("moveRight " + wrapSelection);
      if (wrapSelection) {
        while (true) {
          if (newCol < colCount - 1) newCol++; // 右端でない
          else if (newRow < rowCount - 1) { newRow++; newCol = 0; } // 下端でない
          else {
            // 右端かつ下端である。
            newRow = -1;
            newCol = -1;
            break;
          }
          if (canSelectCell(newRow, newCol)) break;
        }
      } else {
        while (true) {
          if (newCol < colCount - 1) newCol++; // 右端でない
          else {
            // 右端である。
            newRow = -1;
            newCol = -1;
            break;
          }
          if (canSelectCell(newRow, newCol)) break;
        }
      }
      break;
    case SWT.UP: // 上へ移動
      while (true) {
        if (0 < newRow) newRow--; // 上端でない
        else {
          // 上端である
          newRow = -1;
          newCol = -1;
          break;
        }
        if (canSelectCell(newRow, newCol)) break;
      }
      break;
    case SWT.DOWN: // 下へ移動
      while (true) {
        if (newRow < rowCount - 1) newRow++; // 下端でない
        else {
          // 下端である
          newRow = -1;
          newCol = -1;
          break;
        }
        if (canSelectCell(newRow, newCol)) break;
      }
      break;
    }

    //ystem.out.println("" + newRow + " " + newCol);

    if (selectionRow != newRow || selectionCol != newCol) {
      setSelection(newRow, newCol);
      return true;
    }
    return false;
  }

  /** ページアップ */
  public void pageUp() {
    if (rowCount <= 0) return;
    int newTopRow = Math.max(0, topRow - visibleRows);
    makeCellVisible(newTopRow, 0);
  }

  /** ページダウン */
  public void pageDown() {
    if (rowCount <= 0) return;
    int newBottomRow = Math.min(topRow + visibleRows * 2 - 1, rowCount - 1);
    makeCellVisible(newBottomRow, 0);
  }

  /** スクロールアップ */
  public void scrollUp() {
    if (rowCount <= 0) return;
    int newTopRow = Math.max(0, topRow - 1);
    makeCellVisible(newTopRow, 0);
  }

  /** スクロールダウン */
  public void scrollDown() {
    if (rowCount <= 0) return;
    int newBottomRow = Math.min(topRow + visibleRows, rowCount - 1);
    makeCellVisible(newBottomRow, 0);
  }

  /** 指定セルは選択可能か */
  protected boolean canSelectCell(int row, int col) {
    if (!flushEditor()) return false;
    if (col >= 0 && row >= 0) {
      Column column = columns.getColumn(col);
      return column.canSelect();
    }
    return true;
  }

  // 描画 /////////////////////////////////////////////////////////////////////

  private static final int SELECTING = 1;

  /** 描画 */
  protected void paint(PaintEvent e) {

    //ystem.out.println("paint");

    //hread.currentThread().dumpStack();

    //if (log.isTraceEnabled()) log.trace("paint rows:" + rowCount + " cols:" + colCount);

    GC gc = e.gc;

    // ０列の場合、何も描画しない
    if (colCount <= 0) {
      if (getEnabled()) gc.setBackground(getCellBackColor());
      else              gc.setBackground(ColorStock.getSystemColor(SWT.COLOR_WIDGET_BACKGROUND));
      gc.setClipping((Rectangle)null);
      gc.fillRectangle(e.x, e.y, e.width, e.height);
      return;
    }

    // クライアント描画起点を求める

    //Rectangle clientArea = getClientArea();
    Rectangle clientArea = getPaneArea();
    int client_left = clientArea.x;
    int client_top = clientArea.y;
    int client_right = clientArea.x + clientArea.width;
    //if (log.isTraceEnabled()) log.trace("clientArea " + clientArea);


    // クリップ領域を求める。ただし、クライアント描画起点を原点とする。
    int clip_left = e.x - client_left;
    int clip_right = e.x + e.width - client_left;
    int clip_top = e.y - client_top;
    int clip_bottom = e.y + e.height - client_top;
    //if (log.isTraceEnabled()) log.trace("clipArea " + clip_left + " " + clip_right + " " + clip_top + " " + clip_bottom);

    // 各列サイズを取得する
    int[]colWidths = getColWidths(clientArea.width - verLineWidth * (colCount - 1));

    // 行描画高さを計算
    int lineHeight = rowHeight + horLineWidth;

    // 描画開始行・終了行を求める
    int startRow, endRow;
    {
      int offsetTop = Math.max(clip_top - (headerHeight + horLineWidth), 0);
      int offsetBottom = clip_bottom - (headerHeight + horLineWidth);
      //if (offsetBottom <= 0) return;
      startRow = topRow + offsetTop / lineHeight;
      endRow = Math.min(rowCount - 1, topRow + (offsetBottom - 1) / lineHeight);
    }


    // 垂直線、水平線を描画する
    if (horLineWidth > 0) {
      // ヘッダとセルの間
      if (clip_top < headerHeight +  horLineWidth)
        gc.drawLine(client_left, client_top +  headerHeight, client_right, client_top + headerHeight);

      // 水平線、垂直線色
      gc.setBackground(ColorStock.getSystemColor(SWT.COLOR_WIDGET_BACKGROUND));

      // 水平線
      int y = headerHeight + horLineWidth + (startRow - topRow) * lineHeight;
      y += rowHeight;
      for (int row = startRow; row <= endRow; row++) {
        gc.fillRectangle(client_left + clip_left, client_top + y,
          clip_right - clip_left, horLineWidth);
        y += lineHeight;
      }

      // 垂直線
      int x = colWidths[0];
      int startRowY = headerHeight + horLineWidth + (startRow - topRow) * lineHeight;
      int rowsHeight = (endRow - startRow + 1) * lineHeight;
      for (int col = 1; col < colWidths.length; col++) {
        gc.fillRectangle(client_left + x, client_top + startRowY,
          verLineWidth, rowsHeight);
        x += colWidths[col] + verLineWidth;
      }
      gc.fillRectangle(client_left + x, client_top + startRowY,
          verLineWidth, rowsHeight);
    }

    // ヘッダを描画する。
    //if (log.isTraceEnabled()) log.trace("drawing header " + clip_top + " " + headerHeight + " " + horLineWidth);
    if (clip_top < headerHeight + horLineWidth) {
      int y = 0;
      int x = 0;
      for (int col = 0; col < colCount; col++) {
        if (x < clip_right && clip_left < x + colWidths[col]) {
          Rectangle area = new Rectangle(
            client_left + x, client_top + y, colWidths[col], headerHeight
          );
          drawHeader(e.gc, col, area);
        }
        x += colWidths[col];
        if (x < clip_right && clip_left < x + verLineWidth) {
          gc.setClipping((Rectangle)null);
          gc.setBackground(ColorStock.black);
          gc.fillRectangle(client_left + x, client_top + y, verLineWidth, headerHeight);
        }
        x += verLineWidth;
      }
      int noHeader = e.x + e.width - (client_left + x);
      if (noHeader > 0) {
        gc.setClipping((Rectangle)null);
        gc.setBackground(ColorStock.getSystemColor(SWT.COLOR_WIDGET_BACKGROUND));
        gc.fillRectangle(client_left + x, client_top + y, noHeader, headerHeight);
      }
    }

    // 行の描画
    try {
      if (startRow <= endRow)
        beforePaint(startRow, endRow - startRow + 1);

      int y = headerHeight + horLineWidth + (startRow - topRow) * lineHeight;
      for (int row = startRow; row <= endRow; row++) {
        //ystem.out.println("paint row " + row);
        int x = 0;
        // 各セルの描画
        for (int col = 0; col < colCount; col++) {
          // セルがクリップ範囲外でない
          if (x < clip_right && clip_left < x + colWidths[col]) {
            // 描画フラグを決定
            int flags = 0;
            if (row == selectionRow) {
              if (rowSelection || col == selectionCol)
                flags |= SELECTING;
            }
            // セル描画
            drawCell(e.gc, row, col,
              new Rectangle(client_left + x, client_top + y, colWidths[col], rowHeight),
              flags
            );
          }
          x += colWidths[col] + verLineWidth;
        } // end col

        // セルの無い部分
        int noCell = e.x + e.width - (client_left + x);
        if (noCell > 0) {
          gc.setClipping((Rectangle)null);
          fillBackground(gc,
              new Rectangle(client_left + x, client_top + y, noCell, rowHeight), 0, null);
          /*
          gc.setBackground(ColorStock.white);
          gc.fillRectangle(client_left + x, client_top + y, noCell, rowHeight);
          */
        }

        y += lineHeight;
      } // end row

      // 残り部分の描画
      if (y < clip_bottom) {
        gc.setClipping((Rectangle)null);
        if (getEnabled()) gc.setBackground(getCellBackColor());
        else             gc.setBackground(ColorStock.getSystemColor(SWT.COLOR_WIDGET_BACKGROUND));
        gc.fillRectangle(e.x, client_top + y, e.width, clip_bottom - y);
      }
    } finally {
      if (startRow <= endRow)
        afterPaint();
    }
  }

  protected void beforePaint(int row, int count) {
  }

  protected void afterPaint() {
  }

  /** ヘッダの描画を行う */
  protected void drawHeader(GC gc, int col, Rectangle rect) {

    if (columns != null) {
      drawHeaderString(gc, rect, columns.getName(col), SWT.LEFT);
      return;
    }
    drawHeaderString(gc, rect, "COL" + col, SWT.LEFT);

  }

  /** セルの描画を行う */
  protected void drawCell(GC gc, int row, int col, Rectangle rect, int flags) {
    Column column = columns.getColumn(col);
    String content = column.getContent(row);
    if (content != null) {
      int shift = column.getShift(row);
      Color fore = column.getForeground(row);
      Color back = column.getBackground(row);
      drawCellString(gc, row, col, rect, flags, content, shift, back, fore, column.markEditable(row));
      return;
    }
    drawCellString(gc, row, col, rect, flags, "CELL"+ row +"," + col, SWT.LEFT, null, null, false);
  }

  protected void drawSelectable(GC gc, Rectangle rect) {
    gc.setForeground(getSelectionBackColor());
    gc.drawRectangle(new Rectangle(rect.x, rect.y, rect.width - 1, rect.height - 1));
  }

  // 描画ユーティリティ ///////////////////////////////////////////////////////

  /** ヘッダ文字列を描画する */
  protected void drawHeaderString(GC gc, Rectangle rect, String s, int shift) {
    gc.setClipping(rect);
    fill3D(gc, rect);
    /*
    gc.setForeground(ColorStock.getSystemColor(SWT.COLOR_WIDGET_FOREGROUND));
    int offset = (rect.height - fontHeight) / 2;
    gc.drawString(s, rect.x + offset, rect.y + offset);
    */
    drawString(gc, rect, 0, ColorStock.getSystemColor(SWT.COLOR_WIDGET_FOREGROUND),
      s, shift);
  }

  /** セル文字列を描画する 
   * @param editable セルが編集可能であることを示す。*/
  protected void drawCellString(GC gc, int row, int col, Rectangle rect, int flags, String s,
    int alignment, Color backColor, Color foreColor, boolean editable) {

    gc.setClipping(rect);

    // 背景塗りつぶし
    fillBackground(gc, rect, flags, backColor);

    if (editable) {
      gc.setForeground(ColorStock.green);
      gc.drawRectangle(rect.x + 1, rect.y + 1, rect.width - 3, rect.height - 3);
    }
    
    // 文字描画
    if (s != null) {
      drawString(gc, rect, flags, foreColor, s, alignment);
    }
  }

  /** 指定矩形を3D効果で塗りつぶす。クリッピングは設定しない */
  protected void fill3D(GC gc, Rectangle rect) {
    gc.setBackground(ColorStock.getSystemColor(SWT.COLOR_WIDGET_BACKGROUND));
    gc.fillRectangle(rect);
    int left = rect.x;
    int top = rect.y;
    int right = rect.x + rect.width - 1;
    int bottom = rect.y + rect.height - 1;
    gc.setForeground(ColorStock.getSystemColor(SWT.COLOR_WIDGET_HIGHLIGHT_SHADOW));
    gc.drawLine(left, top, right, top);
    gc.drawLine(left, top, left, bottom);
    gc.setForeground(ColorStock.getSystemColor(SWT.COLOR_WIDGET_NORMAL_SHADOW));
    gc.drawLine(right, top, right, bottom);
    gc.drawLine(left, bottom, right, bottom);
  }

  /** 指定矩形内に指定色で塗りつぶす。色指定が無いときはデフォルト色を使う。
   *  クリッピングは設定しない。
   */
  protected void fillBackground(GC gc, Rectangle rect, int flags, Color backColor) {
    if ((flags & SELECTING) != 0) {
      gc.setBackground(getSelectionBackColor());
    } else {
      if (backColor != null) gc.setBackground(backColor);
      else                   gc.setBackground(getCellBackColor());
    }
    gc.fillRectangle(rect);
  }

  /** 指定矩形内に指定色で文字列を描画する。
   *  色指定が無い場合はデフォルト色を使う。
   *  クリッピングは設定しない。 */
  protected void drawString(GC gc, Rectangle rect, int flags, Color foreColor,
    String s, int alignment) {

    if (s == null) return;

    if ((flags & SELECTING) != 0) {
      gc.setForeground(getSelectionForeColor());
    } else {
      if (foreColor != null) gc.setForeground(foreColor);
      else                   gc.setForeground(getCellForeColor());
    }

    switch (alignment) {
    case 0: alignment = SWT.LEFT; break;
    case 1: alignment = SWT.CENTER; break;
    case 2: alignment = SWT.RIGHT; break;
    }

    int offset = (rect.height - fontHeight) / 2;
    Point extent = gc.stringExtent(s);
    Point ellipsis_extent = gc.stringExtent(ELLIPSIS);
    switch (alignment) {
    default: // SWT.LEFT
      gc.drawString(s, rect.x + offset, rect.y + offset);
      if (extent.x > rect.width - offset) {
        /*
        gc.drawString(ELLIPSIS, rect.x + rect.width - ellipsis_extent.x,
          rect.y + offset);
        */
        rightEllipsis(gc, rect.x + rect.width, rect.y + offset, extent.y);
      }
      break;
    case SWT.RIGHT:
      gc.drawString(s, rect.x + rect.width - offset - extent.x, rect.y + offset);
      if (extent.x > rect.width - offset) {
        leftEllipsis(gc, rect.x, rect.y + offset, extent.y);
      }
      break;
    case SWT.CENTER:
      gc.drawString(s, rect.x + rect.width / 2 - extent.x / 2, rect.y + offset);
      if (extent.x > rect.width) {
        //gc.drawString(ELLIPSIS, rect.x, rect.y + offset);
        leftEllipsis(gc, rect.x, rect.y + offset, extent.y);
        //gc.drawString(ELLIPSIS, rect.x + rect.width - ellipsis_extent.x, rect.y + offset);
        rightEllipsis(gc, rect.x + rect.width, rect.y + offset, extent.y);
      }
      break;
    }
  }

  protected void leftEllipsis(GC gc, int x, int y, int height) {
    //gc.drawString(ELLIPSIS, x, y);
    Color bcolor = gc.getBackground();
    Color fcolor = gc.getForeground();
    int half = height / 2;
    gc.fillRectangle(x, y, half + 2, height);

    int[]points = new int[] {
      x + half + 1, y, x + half + 1, y + height, x + 1, y + half
    };

    gc.setBackground(ColorStock.pink);
    gc.fillPolygon(points);
    gc.setBackground(bcolor);
    gc.drawPolygon(points);
  }

  protected void rightEllipsis(GC gc, int x, int y, int height) {
    /*
    Point ellipsis_extent = gc.stringExtent(ELLIPSIS);
    gc.drawString(ELLIPSIS, x - ellipsis_extent.x, y);
    */
    Color bcolor = gc.getBackground();
    Color fcolor = gc.getForeground();
    int half = height / 2;
    gc.fillRectangle(x - half - 2, y, half + 2, height);
    int[]points = new int[] { x - half - 1, y, x - half - 1, y + height, x - 1, y + half };
    gc.setBackground(ColorStock.pink);
    gc.fillPolygon(points);
    gc.setBackground(bcolor);
    gc.drawPolygon(points);
  }

  /** セル文字の背景色を取得 */
  protected Color getCellBackColor() {
    return ColorStock.getSystemColor(SWT.COLOR_LIST_BACKGROUND);
  }

  /** セル文字の前景色を取得 */
  protected Color getCellForeColor() {
    return ColorStock.getSystemColor(SWT.COLOR_LIST_FOREGROUND);
  }

  /** セル文字の選択中背景色を取得 */
  protected Color getSelectionBackColor() {
    return ColorStock.getSystemColor(SWT.COLOR_LIST_SELECTION);
  }

  /** セル文字の選択中前景色を取得 */
  protected Color getSelectionForeColor() {
    return ColorStock.getSystemColor(SWT.COLOR_LIST_SELECTION_TEXT);
  }

  /** 行全体を再描画 */
  public void redrawRow(int row) {
    Rectangle r = getRowRect(row);
    redraw(r.x, r.y, r.width, r.height, false);
  }

  /** セルを再描画 */
  public void redrawCell(int row, int col) {
    Rectangle r = getCellRect(row, col);
    redraw(r.x, r.y, r.width, r.height, false);
  }

  /** セル範囲を再描画 */
  public void redrawCells(int row0, int col0, int row1, int col1) {
    Rectangle r0 = getCellRect(row0, col0);
    Rectangle r1 = getCellRect(row1, col1);
    int right = r1.x + r1.width;
    int bottom = r1.y + r1.height;
    redraw(r0.x, r0.y, right - r0.x, bottom - r0.y, false);
  }

  /** ヘッダ全体を再描画 */
  public void redrawHeader() {
    Rectangle r = getHeaderArea();
    //ystem.out.println("rect " + r);
    redraw(r.x, r.y, r.width, r.height, false);
  }

  /** ヘッダセルを再描画 */
  public void redrawHeaderCell(int col) {
    Rectangle r = getHeaderCellRect(col);
    redraw(r.x, r.y, r.width, r.height, false);
  }

  /** ヘッダセル範囲を再描画 */
  public void redrawHeaderCells(int col0, int col1) {
    Rectangle r0 = getHeaderCellRect(col0);
    Rectangle r1 = getHeaderCellRect(col1);
    int right = r1.x + r1.width;
    redraw(r0.x, r0.y, right - r0.x, r0.height, false);
  }

  public Rectangle getHeaderCellRect(int col) {
    Rectangle headerArea = getHeaderArea();

    // 全列の幅から所望の列の位置を取得
    int[]colWidths = getColWidths(headerArea.width - verLineWidth * (colCount - 1));
    int x = headerArea.x;
    for (int c = 0; c < col; c++)
      x += colWidths[c] + verLineWidth;

    Rectangle r =  new Rectangle(x, headerArea.y, colWidths[col], headerHeight);
    //if (log.isTraceEnabled()) log.trace("getHeaderCellRect " + col + ":" + r);
    return r;
  }

  // スクロール ///////////////////////////////////////////////////////////////

  /** スクロール実行。指定行がトップ行となるようにスクロールする。
   *  スクロールバーの操作は行わない。 */
  protected void scrollTo(int newTopRow) {

    if (topRow == newTopRow) return;

    //if (log.isTraceEnabled()) log.trace("newTopRow " + newTopRow);

    // クライアント領域を求める
    Rectangle clientArea = getClientArea();
    int left   = clientArea.x;
    int top    = clientArea.y;
    int right  = left + clientArea.width;
    int bottom = top + clientArea.height;

    // ヘッダ部分しか表示されてない
    top += headerHeight + horLineWidth;
    if (bottom <= top) {
      // エディタをポジショニング
      positionEditor();
      return;
    }

    // 新旧topRowの距離を求める
    int distance = newTopRow - topRow;

    if (Math.abs(distance) >= visibleRows) {
      // 距離がvisibleRows以上の場合、すべてを再描画
      //if (log.isTraceEnabled()) log.trace("redraw all " + distance);
      topRow = newTopRow;
      redraw(left, top, right - left, bottom - top, false);
    } else {
      // コピー操作を行い、必要部分だけを描画
      //if (log.isTraceEnabled()) log.trace("redraw part " + distance);
      int lineHeight = rowHeight + horLineWidth;
      int copyOffset = Math.abs(distance) * lineHeight;
      //int copySize = (visibleRows - Math.abs(distance)) * lineHeight;
      int copySize = (bottom - top) - Math.abs(distance) * lineHeight;
      GC gc = new GC(this);
      gc.setFont(getFont());
      if (distance > 0) {
        // 上方向にスクロール
        gc.copyArea(left, top + copyOffset, clientArea.width, copySize, left, top);
        redraw(left, top + copySize, clientArea.width, bottom - top - copySize, false);
      } else {
        // 下方向にスクロール
        gc.copyArea(left, top, clientArea.width, copySize, left, top +  copyOffset);
        redraw(left, top, clientArea.width, copyOffset, false);
      }
      gc.dispose();
      topRow = newTopRow;
    }

    // エディタをポジショニング
    positionEditor();
  }

  /** トップ行を取得 */
  public int getTopRow() {
    return topRow;
  }
  
  /** 指定行をトップ行にする */
  public void makeRowTop(int row) {

    //if (log.isTraceEnabled()) log.trace("makeRowTop " + row);

    if (row == topRow) return;
    if (rowCount <= visibleRows) return;

    // 上側にはみだしている
    if (row < topRow) {
      scrollBar.setSelection(row);
      scrollTo(row);
      return;
    }

    // 要望行を補正
    if (row + visibleRows > rowCount) {
      row = rowCount - visibleRows;
    }

    // スクロール
    scrollBar.setSelection(row);
    scrollTo(row);
  }

  /** 指定セルを可視にする */
  public void makeCellVisible(int row, int col) {

    // 上側にはみ出している
    if (row < topRow) {
      scrollBar.setSelection(row);
      scrollTo(row);
      return;
    }

    // 下側にはみ出している
    if (topRow + visibleRows <= row) {
      int newTopRow = row - visibleRows + 1;
      scrollBar.setSelection(newTopRow);
     scrollTo(newTopRow);
    }
  }

  // 編集 /////////////////////////////////////////////////////////////////////

  /** セル用のエディタを取得 */
  protected Editor getEditor(int row, int col) {
    //if (log.isTraceEnabled()) log.trace("getEditor " + row + ", " + col);
    
    Column column = columns.getColumn(col);
    //if (log.isTraceEnabled()) log.trace("column " + column);
    
    Editor editor = column.getEditor(row);
    //if (log.isTraceEnabled()) log.trace("editor " + editor);
    
    return editor;
  }

  /** エディタを開く、既に開いている場合やエディタが無い場合は何もしない */
  public boolean openEditor() {
    //if (log.isTraceEnabled()) log.trace("openEditor " + selectionRow + ", " + selectionCol);
    if (currentEditor != null) {
      //if (log.isTraceEnabled()) log.trace("currentEditor exists");
      return true;
    }

    if (selectionRow < 0 || selectionCol < 0) {
      //if (log.isTraceEnabled()) log.trace("cell out of range");
      return false;
    }

    // このセル用のエディタをチェック
    currentEditor = getEditor(selectionRow, selectionCol);
    if (currentEditor != null) {
      //if (log.isTraceEnabled()) log.trace("got editor " + currentEditor);
      currentEditor.setGrid(this);

      // エディタがある場合、オープン
      //if (log.isTraceEnabled()) log.trace("opening editor");
      currentEditor.setArea(getCellRect(selectionRow, selectionCol));
      currentEditor.open(selectionRow, selectionCol);
      return true;
    }

    //if (log.isTraceEnabled()) log.trace("Could not get editor");
    
    // エディタが無い場合、グリッドにフォーカスを移す。
    setFocus();
    return false;
  }

  /** エディタをフラッシュする。エディタがあり、かつフラッシュできない場合はfalse */
  public boolean flushEditor() {
    if (currentEditor != null)
      if (!currentEditor.flush()) return false;
    return true;
  }

  /** エディタを閉じる。エディタの状態が正常でなくとも強制する。 */
  public void closeEditor() {
    if (currentEditor != null) {
      currentEditor.close();
      currentEditor = null;
    }
  }

  /** エディタのポジショニング */
  protected void positionEditor() {
    if (currentEditor == null) return;
    Rectangle area;
    if (selectionRow < topRow) area = new Rectangle(0,0,0,0);
    else area = getCellRect(selectionRow, selectionCol);
    currentEditor.setArea(area);
  }

  // エディタ /////////////////////////////////////////////////////////////////

  /** 一般エディタ */
  public abstract static class Editor {

    /** 対象グリッド */
    protected Grid grid;

    /** 編集対象行 */
    protected int editingRow;

    /** 編集対象列 */
    protected int editingCol;

    /** グリッドを指定して作成 */
    protected Editor(Grid grid) {
      setGrid(grid);
    }

    protected Editor() {
    }

    /** 後からグリッドを指定 */
    protected void setGrid(Grid grid) {
      assert(this.grid == null || this.grid == grid);
      this.grid = grid;
    }

    public abstract void setArea(Rectangle rect);

    /** エディタをオープンする */
    public void open(int row, int col) {
      this.editingRow = row;
      this.editingCol = col;
    }

    /** エディタをフラッシュする。フラッシュできない場合はfalseを返す。
     *  falseを返すと、そのセル選択を解除できなくなる。 */
    public abstract boolean flush();

    /** エディタを強制的にクローズする。*/
    public abstract void close();

    /** フォーカスを移す */
    public abstract boolean setFocus();
  }

  /** CustomTextを使用したテキストエディタ */
  public static class CustomTextEditor extends Editor {

    protected SwText customText;
    protected Class<? extends SwText> controlClass;
    protected Object object;

    public CustomTextEditor(final Grid grid, Class<? extends SwText> controlClass, Object object) {
      this.controlClass = controlClass;
      this.object = object;
      setGrid(grid);
    }

    public CustomTextEditor(Class<? extends SwText> controlClass, Object object) {
      this.controlClass = controlClass;
      this.object = object;
    }

    @Override
    protected void setGrid(Grid aGrid) {
      
      //if (log.isTraceEnabled())
        //log.trace("CustomTextEditor#setGrid " + aGrid);
      
      assert(grid == null || grid == aGrid);
      if (grid ==  aGrid) return;

      grid = aGrid;

      try {
        customText = controlClass.newInstance();
      } catch (Exception ex) {
        throw new RuntimeException();
      }
      customText.create(grid);
      //customText = (CustomText)CustomControl.create(grid, controlClass);
      
      //if (log.isTraceEnabled())
        //log.trace("customText created " + customText);
      
      customText.setVisible(false);

      Text editor = customText.getTextEdit();
      editor.addKeyListener (new KeyAdapter() {
        public void keyPressed(KeyEvent e) {
          CustomTextEditor.this.keyPressed(e);
        }
      });
      editor.addTraverseListener(new TraverseListener() {
        public void keyTraversed(TraverseEvent e) {
          CustomTextEditor.this.keyTraversed(e);
          // タブによる他コンポーネントへのフォーカス移動を防ぐ

          //ystem.out.println("keyTraversed " + e.doit);

          e.doit = false;
        }
      });

      editor.addFocusListener(new FocusListener() {
        public void focusGained(FocusEvent e) {
          //ystem.out.println("CustomTextEditor: got focus");
        }
        public void focusLost(FocusEvent e) {
          //ystem.out.println("CustomTextEditor: lost focus");
          if (grid.flushEditorOnFocusLost)
            grid.flushEditor();
        }
      });

    }

    public boolean setFocus() {
      return customText.setFocus();
    }

    /** テキスト設定 */
    public void setText(String s) {
      customText.setText(s);
    }

    /** テキスト取得 */
    public String getText() {
      return customText.getText();
    }

    /** 変更があるか */
    public boolean isModified() {
      return customText.isModified();
    }

    /** エディタ描画エリアを設定 */
    public void setArea(Rectangle rect) {
      customText.setBounds(rect);
    }

    /** エディタオープン */
    public void open(int row, int col) {
      super.open(row, col);
      onOpen(row, col);
      customText.setVisible(true);
      customText.setFocus();
    }

    protected void onOpen(int row, int col) {
    }

    /** エディタクローズ */
    public void close() {
      customText.setVisible(false);
    }

    /** エディタフラッシュ。編集文字列が正常かどうかチェック */
    public boolean flush() {
      if (!customText.isSane()) {
        SwUtil.beep(grid);
        return false;
      }
      if (customText.isModified())
        onFlush(editingRow, editingCol);
      return true;
    }

    protected void onFlush(int row, int col) {
    }

    /* キーのハンドリング。
     *  以下のような動作にしたい
     *  <ul>
     *  <li>Tab、Enter、上、下キーは無条件でセルを移動する。
     *  <li>左、右キーはセルが全選択されている場合はセル移動、そうでない場合は
     *  Text内でのカーソル移動。
     *  </ul>
     *  <p>
     *  しかし、Textコンポーネントのカーソル移動を禁止する方法がない。
     *  また、keyPressedイベントが起こった時には既にTextコンポーネント内での
     *  カーソル移動が済んでしまっている。仮に全選択されていても、keyPressed
     *  イベントの時点では全選択は解除されている。
     *  </p>
     *  <p>
     *  しかし、キートラバースイベントは、Textコンポーネント内でのカーソル移動
     *  以前に発生するようである。そこで、このイベントの時点での選択数を取得し
     *  ておき、引き続いて発生するkeyPressedイベントでその情報を利用する。
     *  </p>
     */

    private int selectionCount;

    /** キートラバースのハンドリング */
    protected void keyTraversed(TraverseEvent e) {
      selectionCount = customText.getSelectionCount();
    }

    protected void keyPressed(KeyEvent e) {

      //if (log.isTraceEnabled())
        //log.trace("CustomTextEditor.keyPressed " + e);

      int keyCode = e.keyCode;
      boolean moved;

      // 上下カーソルキーの場合、無条件に選択セルを移動
      // Enter,Tabの場合無条件に選択セルを右に移動。
      // 左右キーの場合、テキストが全選択状態であれば選択セルを移動
      // ただし、セル移動の前にフラッシュする必要がある。フラッシュできなければ
      // 移動はしない。
      switch (keyCode) {
      case SWT.ARROW_UP:
        if (!flush()) return;
        moved = grid.moveSelection(SWT.UP);
        break;
      case SWT.ARROW_DOWN:
        if (!flush()) return;
        moved = grid.moveSelection(SWT.DOWN);
        break;
      case SWT.KEYPAD_CR:
      case '\r':
      case '\t':
        if (!flush()) return;
        moved = grid.moveSelection(SWT.RIGHT);
        break;
      case SWT.ARROW_LEFT:
      case SWT.ARROW_RIGHT:
        // 横方向カーソルキーの場合、テキスト全選択中であればセル選択を移動する。
        // そうでなければ何もしない。
        if (customText.getCharCount() == selectionCount) {
          if (!flush()) return;
          moved = grid.moveSelection(keyCode == SWT.ARROW_LEFT? SWT.LEFT:SWT.RIGHT);
        } else
          return;
        break;
      default:
        return;
      }

      // これはあまり意味がない。
      if (!moved) customText.selectAll();
    }
  }


  // 列定義 ///////////////////////////////////////////////////////////////////

  public static class Column {

    protected String name;
    protected String widstr;
    protected Columns columns;

    public Column(String name, String widstr) {
      this.name = name;
      this.widstr = widstr;
    }

    protected String getColumnName() {
      return name;
    }

    /** グリッドを取得 */
    protected Grid getGrid() {
      return columns.grid;
    }

    /** エディタを取得する */
    protected Editor getEditor(int row) {
      return null;
    }

    /** セル内容を取得する */
    protected String getContent(int row) {
      return null;
    }

    /** セルシフトを取得する */
    protected int getShift(int row) {
      return SWT.LEFT;
    }

    /** セルの選択が可能か */
    protected boolean canSelect() {
      return true;
    }

    /** セルクリック時 */
    protected void clicked(int button, int row) {
      clicked(row);
    }

    protected void clicked(int row) {

    }

    /** 列インデックスを取得 */
    protected int getCellIndex() {
      return columns.getIndex(this);
    }

    /** 列矩形を取得 */
    public Rectangle getRect(int row) {
      return getGrid().getCellRect(row, getCellIndex());
    }

    /** 指定行を再描画 */
    protected void redrawRow(int row) {
      getGrid().redrawRow(row);
    }

    protected Color getForeground(int row) { return null; }
    protected Color getBackground(int row) { return null; }
    
    protected boolean markEditable(int row) { return false; }
  }

  public static class CenterColumn extends Column {
    public CenterColumn(String a, String b) {
      super(a, b);
    }
    @Override protected int getShift(int row) { return 1; }
  }

  public abstract static class BooleanColumn extends CenterColumn {
    public BooleanColumn(String a, String b) {
      super(a, b);
    }
    @Override protected String getContent(int row) {
      return getBoolean(row)? "○":"";
    }
    protected abstract boolean getBoolean(int row);
  }

  public static class RightColumn extends Column {
    public RightColumn(String a, String b) {
      super(a, b);
    }
    @Override protected int getShift(int row) { return 2; }
  }

  public static class NumberColumn extends RightColumn {
    public NumberColumn(String a, String b) {
      super(a, b);
    }
    @Override protected String getContent(int row) {
      Number number = getNumber(row);
      if (number == null) return "";
      return format(number);
    }
    protected String format(Number n) {
      return n.toString();
    }
    protected Number getNumber(int row) { return 0; }
  }

  public static class PriceColumn extends NumberColumn {
    public PriceColumn(String a, String b) {
      super(a, b);
    }
    @Override
    protected String format(Number n) {
      return _PriceUtil.toString(n.longValue());
    }
  }


  public static class Columns {

    protected Column[]columns;
    protected Grid grid;

    public Columns(String[]names, String[]widstr) {
      if (names.length != widstr.length) throw new InternalError();
      columns = new Column[names.length];
      for (int i = 0; i < columns.length;  i++)
        columns[i] = new Column(names[i], widstr[i]);
    }

    public Columns(Column[]columns) {
      this.columns = columns;
    }

    protected void setGrid(Grid grid) {
      this.grid = grid;
      for (Column col: columns) col.columns = this;
    }

    public String getName(int col) {
      return columns[col].getColumnName();
    }

    public Column getColumn(int col) {
      return columns[col];
    }

    public int getIndex(Column col) {
      for (int i = 0; i < columns.length; i++)
        if (columns[i] == col) return i;
      return -1;
    }

    public int getCount() {
      return columns.length;
    }

    int[]getColWidths(Grid grid, int size) {
      int[]widths = new int[columns.length];
      GC gc = new GC(grid);
      gc.setFont(grid.getFont());
      try {
        int totalWidth = 0;
        int nospec = 0;
        for (int i = 0; i < columns.length; i++) {
          if (columns[i].widstr == null) {
            nospec++;
            continue;
          }
          Point extent = gc.stringExtent(columns[i].widstr);
          widths[i] = extent.x;
          totalWidth += extent.x;
        }
        if (nospec > 0) {
          int nospec_width = (size - totalWidth) / nospec;
          int nospec_amari = (size - totalWidth) % nospec;
          for (int i = 0; i < widths.length; i++) {
            if (widths[i] != 0) continue;
            nospec--;
            widths[i] = nospec_width;
            if (nospec == 0) widths[i] += nospec_amari;
          }
        }
        return widths;
      } finally {
        gc.dispose();
      }
    }
  }

  // その他 ///////////////////////////////////////////////////////////////////

  /** セルがクリックされた。
   *  このセルが選択されなくとも（canSelectCellがfalseであっても）、
   *  このイベントは発生する */
  protected void cellClicked(Point p, int row, int col) {
  }

  /** セル内でキーが押された。キーを処理した場合はtrueを返す。 */
  protected boolean keyPressed(KeyEvent e, int row, int col) {
    return false;
  }

  // リスナ ///////////////////////////////////////////////////////////////////
  // グリッドの選択状態が変更されたときに呼ばれる

  protected ArrayList<SelectionListener> listeners = new ArrayList<SelectionListener>();

  public void addSelectionListener(SelectionListener listener) {
    listeners.add(listener);
  }

  protected void fireSelectionChanged() {
    for (int i = 0; i < listeners.size(); i++)
      ((SelectionListener)listeners.get(i)).widgetSelected(null);
  }


  // デバッグ /////////////////////////////////////////////////////////////////

  private static void println(String s) {
    System.out.println(s);
  }

}
