package com.cm55.swt.grid;

import org.eclipse.swt.events.*;
import org.eclipse.swt.graphics.*;
import org.eclipse.swt.widgets.*;

import com.cm55.swt.*;
import com.cm55.swt.misc.*;

public class SwGridAbstract<T extends Grid> extends SwControl<T>  implements WheelTarget {


  public T doCreate(Composite parent) {
    T grid = createGrid(parent);

    // 選択時にイベントを発生
    grid.addSelectionListener(new SelectionAdapter() {
      @Override
      public void widgetSelected(org.eclipse.swt.events.SelectionEvent e) {
        genSelectedEvent();
      }
    });

    // 「Enter」キーで入力イベント発生
    grid.addKeyListener(new KeyAdapter() {
      public void keyPressed(KeyEvent e) {
        if (e.character == '\r') genInputEvent();
      }
    });

    // ダブルクリックで入力イベント発生
    grid.addMouseListener(new MouseAdapter() {
      @Override
      public void mouseDoubleClick(MouseEvent e) {
        genInputEvent();
      }
      @Override
      public void mouseDown(MouseEvent e) {
        Point cell = grid.getCellAtPoint(e.x, e.y);
        if (cell == null) return;
        dispatchEvent(new CellClickedEvent(cell.y, cell.x));
      }
    });

    return grid;
  }

  public void addKeyListener(KeyListener listener) {
    control.addKeyListener(listener);
  }
  
  /** グリッドの作成 */
  @SuppressWarnings("unchecked")
  protected T createGrid(Composite parent) {
    return (T)new Grid(parent);
  }

  public int getTopRow() {
    return control.getTopRow();
  }
  
  public void makeRowTop(int row) {
    control.makeRowTop(row);
  }

  /** 選択する */
  public void setSelection(int row, int col) {
    control.setSelection(row, col);
  }

  /** 選択行を取得 */
  public int getSelectionRow() {
    return control.getSelectionRow();
  }

  /** 選択列を取得 */
  public int getSelectionCol() {
    return control.getSelectionCol();
  }

  public void makeRowVisible(int row) {
    control.makeCellVisible(row, 0);
  }
  
  public void setRowCount(int rowCount) {
    control.setRowCount(rowCount);
  }

  public int getRowCount() {
    return control.getRowCount();
  }

  public void setColumns(Grid.Columns columns) {
    control.setColumns(columns);
  }

  public void setColumns(Grid.Column[]columns) {
    control.setColumns(columns);
  }

  public boolean moveSelection(int direction) {
    return control.moveSelection(direction);    
  }
  
  /** グリッド選択イベントを発生 */
  protected void genSelectedEvent() {
    int row = control.getSelectionRow();
    int col = control.getSelectionCol();
//    if (row < 0 || col < 0) return;
    dispatchEvent(new SelectedEvent(row, col));
  }

  /** グリッド入力イベントを発生 */
  protected void genInputEvent() {
    int row = control.getSelectionRow();
    int col = control.getSelectionCol();
    if (row < 0 || col < 0) return;
    dispatchEvent(new InputEvent(row, col));
  }

  /** グリッド選択イベント */
  public static class SelectedEvent {
    public int row;
    public int col;
    protected SelectedEvent(int row, int col) {
      this.row = row;
      this.col = col;
    }

    public String toString() {
      return "CustomGrid.SelectedEvent " + row + "," + col;
    }
  }

  /** グリッド入力イベント */
  public static class InputEvent {
    public int row;
    public int col;
    protected InputEvent(int row, int col) {
      this.row = row;
      this.col = col;
    }

    public String toString() {
      return "CustomGrid.InputEvent " + row + "," + col;
    }
  }

  /** スクロール */
  @Override
  public void scroll(int count) {
    control.scroll(count);
  }

  /** マウスエンター時に自動フォーカス */
  public void setFocusOnMouseEnter(boolean value) {
    control.setFocusOnMouseEnter(value);
  }

  public void redraw() {
    control.redraw();
  }

  public void redrawRow(int row) {
    control.redrawRow(row);

  }

  public boolean flushEditor() {
    return control.flushEditor();
  }

  public void closeEditor() {
    control.closeEditor();
  }

  public void setRowSelection(boolean value) {
    control.setRowSelection(value);
  }

  public Rectangle getCellRect(int row, int col) {
    return control.getCellRect(row, col);
  }
  /////////////////////////////////////////////////////////////////////////////
  // イベント
  /////////////////////////////////////////////////////////////////////////////

  public static class CellClickedEvent {
    public int row;
    public int col;
    protected CellClickedEvent(int row, int col) {
      this.row = row;
      this.col = col;
    }

    public String toString() {
      return "CellClickedEvent row=" + row + ", col=" + col;
    }
  }
}

