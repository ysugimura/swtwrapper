// Created by Cryptomedia Co., Ltd. 2006/08/04
package com.cm55.swt.grid;

import org.eclipse.swt.widgets.*;

import com.cm55.swt.color.*;

public class SwZebraGrid extends SwGridAbstract<ZebraGrid> {
 
  protected ZebraGrid createGrid(Composite parent) {
    return new ZebraGrid(parent);
  }

  public void setRowColoring(RowColoring rowColoring) {
    control.setRowColoring(rowColoring);
  }
}
