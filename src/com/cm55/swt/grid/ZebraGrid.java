package com.cm55.swt.grid;

import org.eclipse.swt.graphics.*;
import org.eclipse.swt.widgets.*;

import com.cm55.swt.color.*;

/**
 * シマウマグリッド
 */
public class ZebraGrid extends Grid {

  protected RowColoring rowColoring = new StdRowColoring();

  public ZebraGrid(Composite parent) {
    super(parent);
  }

  public void setRowColoring(RowColoring rowColoring) {
    this.rowColoring = rowColoring;
  }
  
  protected void drawCellString(
    GC gc, int row, int col, Rectangle rect, int flags, String s,
    int alignment, Color backColor, Color foreColor, boolean editable) {
    if (backColor == null) backColor = rowColoring.get(row);
    super.drawCellString(gc, row, col, rect, flags, s, alignment, backColor, foreColor, editable);
  }
}
