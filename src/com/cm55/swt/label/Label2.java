package com.cm55.swt.label;

import org.eclipse.swt.*;
import org.eclipse.swt.events.*;
import org.eclipse.swt.graphics.*;
import org.eclipse.swt.widgets.*;

import com.cm55.swt.color.*;

/**
 * <p>タイトル: </p>
 * <p>説明: </p>
 * <p>著作権: Copyright (c) 2003</p>
 * <p>会社名: cryptomedia</p>
 * @author yuichi sugimura
 * @version 1.0
 */
public class Label2 extends Canvas {

  protected String text = "";
  protected int style;
  protected Color fgColor;
  protected Color bgColor;
  
  public Label2(Composite parent, int style) {
    super(parent, style);
    this.style = style;
    
    bgColor = ColorStock.getSystemColor(SWT.COLOR_WIDGET_BACKGROUND);
    fgColor = ColorStock.getSystemColor(SWT.COLOR_WIDGET_FOREGROUND);
    
    addPaintListener(new PaintListener() {
      public void paintControl(PaintEvent e) {
        Label2.this.paint(e);
      }
    });
  }
  
  public void setAlignment(int align) {
    int mask = SWT.RIGHT|SWT.CENTER|SWT.LEFT;
    style &= ~mask;
    style |= align & mask;
  }
  
  public Point computeSize (int wHint, int hHint, boolean changed) {
     int width = 0, height = 0;
     if (text != null) {
         GC gc = new GC(this);
         Point extent = gc.stringExtent(text);
         gc.dispose();
         width += extent.x;
        height = Math.max(height, extent.y);
     }
     if (wHint != SWT.DEFAULT) width = wHint;
     if (hHint != SWT.DEFAULT) height = hHint;
     return new Point(width + 2, height + 2);
  }
  
  public void setText(String text) {
    this.text = text;
    redraw();
  }
  
  public String getText() {
    return text;
  }
  
  @Override public void setForeground(Color color) { fgColor = color; redraw(); }
  @Override public Color getForeground() { return fgColor; }
  @Override public void setBackground(Color color) { bgColor = color; redraw(); }
  @Override public Color getBackground() { return bgColor; }
  
  protected void paint(PaintEvent e) {
    Rectangle ca = getClientArea();
    GC gc = e.gc;
    
    gc.setBackground(bgColor);
    gc.fillRectangle(ca);
    gc.setForeground(fgColor);
    
    Point p = gc.stringExtent(text);
    int yOffset = (ca.height - p.y) / 2;
    if ((style & (SWT.CENTER | SWT.RIGHT)) == 0) {
      gc.drawString(text, ca.x, ca.y + yOffset);
    } else if ((style & SWT.CENTER) != 0) {
      int xOffset = (ca.width - p.x) / 2;
      gc.drawString(text, ca.x + xOffset, ca.y + yOffset);
    } else {
      int xOffset = ca.width - p.x;
      gc.drawString(text, ca.x + xOffset, ca.y + yOffset);
    }
  }
}
