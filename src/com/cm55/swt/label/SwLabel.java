package com.cm55.swt.label;

import org.eclipse.swt.*;
import org.eclipse.swt.graphics.*;
import org.eclipse.swt.widgets.*;

import com.cm55.swt.*;

/**
 * ラベルコントロール
 */
public class SwLabel extends SwControl<Label> {

  /** ラベルテキスト */
  public String text; 

  /** イメージ */
  public Image image;

  
  public SwLabel(String text) {  
    this.text = text;
  }
  
  public SwLabel(Image image) {    
    this.image = image;
  }
 
  
  public void setText(String text) {
    getControl().setText(text);
  }
  
  public void setForeground(Color color) {
    getControl().setForeground(color);
  }

  public void setAlignment(int value) {
    getControl().setAlignment(value);
  }
  
  @Override
  protected Label doCreate(Composite parent) {
    SwLabel udLabel = this;
    // if (log.isTraceEnabled()) {
    //   if (udLabel.text != null && udLabel.text.length() > 0)
    //     log.trace("createLabel " + udLabel.text);
    // }
     
     Label label = new Label(parent, SWT.NULL);
     udLabel.control = label;
     if (udLabel instanceof SwLabel.Right) label.setAlignment(SWT.RIGHT);
     if (udLabel.text != null) label.setText(udLabel.text);
     if (udLabel.image != null) {
       label.setImage(udLabel.image);
       label.setAlignment(SWT.CENTER);
     }

     return label;    
  };
  
  public static class Right extends SwLabel {
    public Right(String text) {
      super(text);
    }
  }
}
