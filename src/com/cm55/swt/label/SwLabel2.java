package com.cm55.swt.label;

import org.eclipse.swt.*;
import org.eclipse.swt.graphics.*;
import org.eclipse.swt.widgets.*;

import com.cm55.swt.*;

/**
 * <p>タイトル: </p>
 * <p>説明: </p>
 * <p>著作権: Copyright (c) 2003</p>
 * <p>会社名: cryptomedia</p>
 * @author yuichi sugimura
 * @version 1.0
 */

public class SwLabel2 extends SwControl<Label2> {
  
  /** ラベルテキスト */
  public String text;
  
  public SwLabel2(String text) {
    
    this.text = text;
  }
  
  public SwLabel2() {
    
  }


  public void setText(String text) {
    getControl().setText(text);
  }

  public void setAlignment(int align) {
    getControl().setAlignment(align);
  }

  public String getText() {
    return getControl().getText();
  }
  
  public void setForeground(Color color) {
    getControl().setForeground(color);
  }

  
  @Override
  protected Label2 doCreate(Composite parent) {
    SwLabel2 udLabel = this;
    Label2 label = new Label2(parent, SWT.NULL);
    udLabel.control = label;
    if (udLabel instanceof SwLabel2.Right) label.setAlignment(SWT.RIGHT);
    if (udLabel.text != null) label.setText(udLabel.text);

    return label;
  };
  
  public static class Right extends SwLabel2 {
    public Right(String text) {
      super(text);
    }

  }
}
