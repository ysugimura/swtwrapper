// Created by Cryptomedia Co., Ltd. 2005/09/29
package com.cm55.swt.label;

import org.eclipse.swt.*;
import org.eclipse.swt.graphics.*;
import org.eclipse.swt.widgets.*;

import com.cm55.swt.*;

/**
 * カスタマイズ用ラベル
 */
public class SwLabel3 extends SwControl<Label> {

  protected int style = SWT.NULL;

  public Label doCreate(Composite parent) {
    Label label = new Label(parent, style);
    return label;
  }

  public void setText(String s) {
    if (s == null) s = "";
    control.setText(s);
  }

  public void setAlignment(int value) {
    control.setAlignment(value);
  }
  
  public void setForeground(Color color) {
    control.setForeground(color);
  }
}
