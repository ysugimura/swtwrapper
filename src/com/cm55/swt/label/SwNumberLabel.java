// Created by Cryptomedia Co., Ltd. 2007/03/02
package com.cm55.swt.label;

import org.eclipse.swt.*;
import org.eclipse.swt.widgets.*;

public class SwNumberLabel extends SwLabel3 {

  @Override
  public Label doCreate(Composite parent) {
    Label label = super.doCreate(parent);
    label.setAlignment(SWT.RIGHT);
    return label;
  }

  public void setNumber(Number value) {
    setText(value.toString());
  }

  public void clear() {
    setText("");
  }
}
