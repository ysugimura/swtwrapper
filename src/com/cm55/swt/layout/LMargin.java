package com.cm55.swt.layout;

/**
 * レイアウト用マージン
 */
public class LMargin {

  public int width, height;
  public LMargin(int width, int height) {
    this.width = width;
    this.height = height;
  }
  
  public static final LMargin NO_MARGIN = new LMargin(0, 0);
  public static final LMargin MARGIN_5 = new LMargin(5, 5);
}
