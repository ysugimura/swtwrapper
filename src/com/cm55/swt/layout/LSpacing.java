package com.cm55.swt.layout;

/**
 * スペーシング
 */
public class LSpacing {

  public int hSpacing, vSpacing;
  
  public LSpacing(int hSpacing, int vSpacing) {
    this.hSpacing = hSpacing;
    this.vSpacing = vSpacing;
  }
  
  public static final LSpacing NO_SPACING = new LSpacing(0, 0);
  public static final LSpacing SPACING_5 = new LSpacing(5, 5);
}
