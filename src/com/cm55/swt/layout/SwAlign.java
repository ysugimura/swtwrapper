package com.cm55.swt.layout;

import org.eclipse.swt.*;
import org.eclipse.swt.graphics.*;
import org.eclipse.swt.widgets.*;

import com.cm55.swt.*;

/**
 * <p>タイトル: </p>
 * <p>説明: </p>
 * <p>著作権: Copyright (c) 2003</p>
 * <p>会社名: cryptomedia</p>
 * @author yuichi sugimura
 * @version 1.0
 */

public abstract class SwAlign extends SwLayout {

  public LAlign align;
  public SwItem item;
  
  public SwAlign(LAlign align, SwItem item) {
    super();
    if (align == null) align = LAlign.CENTER_CENTER;
    this.align = align;  
    this.item = item;
  }
  
  public static class CenterCenter extends SwAlign {
    public CenterCenter(SwItem item) {
      super(LAlign.CENTER_CENTER, item);
    }
  }
  public static class FillCenter extends SwAlign {
    public FillCenter(SwItem item) {
      super(LAlign.FILL_CENTER, item);
    }
  }
  
  public static class CenterFill extends SwAlign {
    public CenterFill(SwItem item) {
      super(LAlign.CENTER_FILL, item);
    }
  }
  public static class CenterLeft extends SwAlign {
    public CenterLeft(SwItem item) {
      super(LAlign.CENTER_LEFT, item);
    }
  }
  public static class CenterRight extends SwAlign {
    public CenterRight(SwItem item) {
      super(LAlign.CENTER_RIGHT, item);
    }
  }
  public static class TopCenter extends SwAlign {
    public TopCenter(SwItem item) {
      super(LAlign.TOP_CENTER, item);
    }
  }
  public static class TopFill extends SwAlign {
    public TopFill(SwItem item) {
      super(LAlign.TOP_FILL, item);
    }
  }
  public static class BottomCenter extends SwAlign {
    public BottomCenter(SwItem item) {
      super(LAlign.BOTTOM_CENTER, item);
    }
  }
  public static class BottomFill extends SwAlign {
    public BottomFill(SwItem item) {
      super(LAlign.BOTTOM_FILL, item);
    }
  }
  public static class BottomLeft extends SwAlign {
    public BottomLeft(SwItem item) {
      super(LAlign.BOTTOM_LEFT, item);
    }
  }
  public static class BottomRight extends SwAlign {
    public BottomRight(SwItem item) {
      super(LAlign.BOTTOM_RIGHT, item);
    }
  }
  
  
  public static class TopLeft extends SwAlign {
    public TopLeft(SwItem item) {
      super(LAlign.TOP_LEFT, item);
    }
  }
  

  public static class LAlign {

    public static final int CENTER = 0;
    public static final int LEFT = 1;
    public static final int RIGHT = 2;
    public static final int TOP = 1;
    public static final int BOTTOM = 2;
    public static final int FILL = 3;
    
    public int horizontal;
    public int vertical;
    
    private LAlign(int vertical, int horizontal) {
      this.vertical = vertical;
      this.horizontal = horizontal;
      
    }

    public static final LAlign CENTER_CENTER = new LAlign(CENTER, CENTER);  
    public static final LAlign CENTER_FILL = new LAlign(CENTER, FILL);
    public static final LAlign CENTER_LEFT = new LAlign(CENTER, LEFT);
    public static final LAlign CENTER_RIGHT = new LAlign(CENTER, RIGHT);
    public static final LAlign TOP_CENTER = new LAlign(TOP, CENTER);
    public static final LAlign TOP_FILL = new LAlign(TOP, FILL);
    public static final LAlign BOTTOM_CENTER = new LAlign(BOTTOM, CENTER);
    public static final LAlign BOTTOM_FILL = new LAlign(BOTTOM, FILL);
    public static final LAlign BOTTOM_LEFT = new LAlign(BOTTOM,LEFT);
    public static final LAlign BOTTOM_RIGHT = new LAlign(BOTTOM, RIGHT);
    public static final LAlign TOP_LEFT = new LAlign(TOP, LEFT);
    public static final LAlign FILL_CENTER = new LAlign(FILL, CENTER);
  }


  /** アラインレイアウト */
  @Override
  public  void layout(Composite composite, LMargin margin) {
    SwAlign al = this;
    AlignLayout layout = new AlignLayout();

    margin = ensureMargin(margin);
    layout.marginWidth = margin.width;
    layout.marginHeight = margin.height;
    switch (al.align.horizontal) {
    case LAlign.LEFT:  layout.hAlign = SWT.LEFT;   break;
    default:            layout.hAlign = SWT.CENTER; break;
    case LAlign.RIGHT: layout.hAlign = SWT.RIGHT;  break;
    case LAlign.FILL:  layout.hAlign = SWT.FILL; break;
    }
    switch (al.align.vertical) {
    case LAlign.TOP:    layout.vAlign = SWT.TOP;    break;
    default:             layout.vAlign = SWT.CENTER; break;
    case LAlign.BOTTOM: layout.vAlign = SWT.BOTTOM; break;
    case LAlign.FILL: layout.vAlign = SWT.FILL; break;
    }
    composite.setLayout(layout);
    SwLayouter.create(composite, al.item);
  }
  
  /**
   * <p>タイトル: </p>
   * <p>説明: </p>
   * <p>著作権: Copyright (c) 2003</p>
   * <p>会社名: cryptomedia</p>
   * @author yuichi sugimura
   * @version 1.0
   */

  public static class AlignLayout extends Layout{

    //private static final Log log = LogFactory.getLog(AlignLayout.class);
    
    public int hAlign = SWT.CENTER;
    public int vAlign = SWT.CENTER;
    public int marginWidth;
    public int marginHeight;
    
    protected Point prefSize;
    
    public AlignLayout() {
    }
    
    public AlignLayout(int hAlign, int vAlign) {
      this.hAlign = hAlign;
      this.vAlign = vAlign;
    }
    
    /** サイズ取得 */
    protected Point computeSize(Composite composite, int wHint, int hHint, boolean flushCache) {
      /*
      if (log.isTraceEnabled())
        log.trace("computeSize " + wHint + "," + hHint + "," + flushCache);
        */
      calcSize(composite.getChildren(), flushCache);
      return new Point(prefSize.x + marginWidth * 2, prefSize.y + marginHeight * 2);
    }
    
    /** レイアウト */
    protected void layout(Composite composite, boolean flushCache) {
      /*
      if (log.isTraceEnabled())
        log.trace("layout " + flushCache);
      */
      Control[]controls = composite.getChildren();
      if (controls.length == 0) return;
      calcSize(controls, flushCache);
      
      Rectangle clientRect = composite.getClientArea();
      Rectangle controlRect = new Rectangle(
        clientRect.x + marginWidth, clientRect.y + marginHeight,
        clientRect.width - marginWidth * 2, clientRect.height - marginHeight * 2);
        

      int controlWidth = Math.min(prefSize.x, controlRect.width);
      int controlHeight = Math.min(prefSize.y, controlRect.height);

      /*
      if (log.isTraceEnabled()) {
        log.trace("controlRect:" + controlRect);
        log.trace("controlW/H " + controlWidth + "," + controlHeight);
      }
      */
      
      int x, y;
      switch (hAlign) {
      case SWT.LEFT:  x = controlRect.x;                                          break;
      default:        x = controlRect.x + (controlRect.width - controlWidth) / 2; break;
      case SWT.RIGHT: x = controlRect.x + (controlRect.width - controlWidth);     break;
      case SWT.FILL:  x = controlRect.x; controlWidth = controlRect.width; break;
      }
      switch (vAlign) {
      case SWT.TOP:    y = controlRect.y;                                     break;
      default:         y = controlRect.y + (controlRect.height - controlHeight) / 2; break;
      case SWT.BOTTOM: y = controlRect.y + (controlRect.height - controlHeight);     break;
      case SWT.FILL:   y = controlRect.y; controlHeight = controlRect.height; break;
      }
      controls[0].setBounds(x, y, controlWidth, controlHeight);
    }
    
    private void calcSize(Control[]controls, boolean flushCache) {
      /*if (log.isTraceEnabled()) {
        log.trace("calcSize " + flushCache);
        for (Control c: controls)
          log.trace(" control:" + c);
      }*/
      
      if (flushCache) prefSize = null;
      if (prefSize != null) return;
      if (controls.length == 0)
        prefSize = new Point(0, 0);
      else
        prefSize = controls[0].computeSize(SWT.DEFAULT, SWT.DEFAULT, true);
      
      /*if (log.isTraceEnabled())
        log.trace("  prefSize:" + prefSize);*/
    }
  }

}
