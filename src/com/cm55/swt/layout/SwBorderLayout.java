package com.cm55.swt.layout;

import org.eclipse.swt.layout.*;
import org.eclipse.swt.widgets.*;

import com.cm55.swt.*;

/**
 * ボーダーレイアウト。以下のようにレイアウトする。
 * +-----------------+
 * |       top       |
 * +----+------+-----+
 * |    |      |     |
 * |left|center|right|
 * |    |      |     |
 * +----+------+-----+
 * |     bottom      |
 * +-----------------+
 */
public class SwBorderLayout extends SwCompositeLayout {

  /** 中身 */
  public SwItem center, left, right, top, bottom;
  
  public SwBorderLayout(LSpacing spacing,
    SwItem top, SwItem left, SwItem center, SwItem right, SwItem bottom) {
    super(spacing);
    this.center = center;
    this.left = left;
    this.right = right;
    this.top = top;
    this.bottom = bottom;  
  }
  public SwBorderLayout(
      SwItem top, SwItem left, SwItem center, SwItem right, SwItem bottom) {
    this(null, top, left, center, right, bottom);
  }
  
  public static class V extends SwBorderLayout {
    public V(SwItem top, SwItem center, SwItem bottom) {
      this(null, top, center, bottom);
    }
    public V(LSpacing spacing,SwItem top, SwItem center, SwItem bottom) {
      super(spacing, top, null, center, null, bottom);
    }
  }
  
  public static class H extends SwBorderLayout {
    public H(SwItem left, SwItem center, SwItem right) {
      this(null, left, center, right);
    }
    public H(LSpacing spacing,SwItem left, SwItem center, SwItem right) {
      super(spacing, null, left, center, right, null);
    }
  }
  

  /** ボーダーレイアウトを行う */
  public void layout(Composite composite, LMargin margin)  {
    SwBorderLayout bl = this;
    Control center = null, left = null, right = null, top = null, bottom = null;

    if (bl.center != null) center = SwLayouter.create(composite, bl.center);
    if (bl.left != null)   left   = SwLayouter.create(composite, bl.left);
    if (bl.right != null)  right  = SwLayouter.create(composite, bl.right);
    if (bl.top != null)    top    = SwLayouter.create(composite, bl.top);
    if (bl.bottom != null) bottom = SwLayouter.create(composite, bl.bottom);

    margin = ensureMargin(margin);
    int leftMargin = margin.width;
    int rightMargin = margin.width;
    int topMargin = margin.height;
    int bottomMargin = margin.height;

    LSpacing spacing = ensureSpacing(bl.spacing);
    int hSpacing = spacing.hSpacing;
    int vSpacing = spacing.vSpacing;

    composite.setLayout(new FormLayout());

    // 中央のレイアウト
    if (center != null) {
      FormData data = new FormData();
      if (left == null) data.left = new FormAttachment(0, leftMargin);
      else              data.left = new FormAttachment(left, hSpacing);
      if (right == null) data.right = new FormAttachment(100, -rightMargin);
      else               data.right = new FormAttachment(right, -hSpacing);
      if (top == null) data.top = new FormAttachment(0, topMargin);
      else             data.top = new FormAttachment(top, vSpacing);
      if (bottom == null) data.bottom = new FormAttachment(100, -bottomMargin);
      else                data.bottom = new FormAttachment(bottom, -vSpacing);
      center.setLayoutData(data);
    }

    // 左側のレイアウト
    if (left != null) {
      FormData data = new FormData();
      data.left = new FormAttachment(0, leftMargin);
      if (top == null) data.top = new FormAttachment(0, topMargin);
      else             data.top = new FormAttachment(top, vSpacing);
      if (bottom == null) data.bottom = new FormAttachment(100, -bottomMargin);
      else                data.bottom = new FormAttachment(bottom, -vSpacing);
      left.setLayoutData(data);
    }

    // 右側のレイアウト
    if (right != null) {
      FormData data = new FormData();
      data.right = new FormAttachment(100, -rightMargin);
      if (top == null) data.top = new FormAttachment(0, topMargin);
      else             data.top = new FormAttachment(top, vSpacing);
      if (bottom == null) data.bottom = new FormAttachment(100, -bottomMargin);
      else                data.bottom = new FormAttachment(bottom, -vSpacing);
      right.setLayoutData(data);
    }

    // 上側のレイアウト
    if (top != null) {
      FormData data = new FormData();
      data.top = new FormAttachment(0, topMargin);
      data.left = new FormAttachment(0, leftMargin);
      data.right = new FormAttachment(100, -rightMargin);
      top.setLayoutData(data);
    }

    // 下側のレイアウト
    if (bottom != null) {
      FormData data = new FormData();
      data.bottom = new FormAttachment(100, -bottomMargin);
      data.left = new FormAttachment(0, leftMargin);
      data.right = new FormAttachment(100, -rightMargin);
      bottom.setLayoutData(data);
    }
  }
}
