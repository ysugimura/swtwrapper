package com.cm55.swt.layout;

public abstract class SwCompositeLayout extends SwLayout {
  public LSpacing spacing;
  protected SwCompositeLayout(LSpacing spacing) {
    this.spacing = spacing;
  }
}
