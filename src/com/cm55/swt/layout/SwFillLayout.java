package com.cm55.swt.layout;

import org.eclipse.swt.layout.*;
import org.eclipse.swt.widgets.*;

import com.cm55.swt.*;

/**
 *
 */
public class SwFillLayout extends SwLayout {

  public SwItem item;
  
  public SwFillLayout(LSpacing spacing, SwItem item) {
    super();
    this.item = item;
  }
  public SwFillLayout(SwItem item) {
    super();
    this.item = item;
  }
  


  /** フィルレイアウト */
  @Override
  public void layout(Composite composite, LMargin margin) {
    SwFillLayout fl = this;
    GridLayout layout = new GridLayout(1, false);
    margin = ensureMargin(margin);
    layout.marginWidth = margin.width;
    layout.marginHeight = margin.height;
    composite.setLayout(layout);

    Control control = SwLayouter.create(composite, fl.item);
    GridData data = new GridData();
    data.horizontalAlignment = GridData.FILL;
    data.verticalAlignment = GridData.FILL;
    data.grabExcessHorizontalSpace = true;
    data.grabExcessVerticalSpace = true;
    control.setLayoutData(data);
  }
}
