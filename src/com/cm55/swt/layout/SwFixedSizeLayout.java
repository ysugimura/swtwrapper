// Created by Cryptomedia Co., Ltd. 2006/02/20
package com.cm55.swt.layout;

import org.eclipse.swt.graphics.*;
import org.eclipse.swt.widgets.*;

import com.cm55.swt.*;

/** 固定サイズレイアウト */
public class SwFixedSizeLayout extends SwCompositeLayout {
  
  public int xsize;
  public int ysize;
  public SwItem item;
  
  public SwFixedSizeLayout(int xsize, int ysize, SwItem item) {
    this(null, xsize, ysize, item);
  }
  public SwFixedSizeLayout(LSpacing spacing, int xsize, int ysize, SwItem item) {
    super(spacing);
    this.xsize = xsize;
    this.ysize = ysize;
    this.item = item;
  }
  

  /** 固定サイズレイアウト */
  @Override
  public  void layout(Composite composite, LMargin aMargin) {
    //ystem.out.println("fixedSize " + fs);
    final SwFixedSizeLayout fs = this;

    final LMargin margin = ensureMargin(aMargin);

    Layout layout = new Layout() {

      protected Point computeSize (Composite composite, int wHint, int hHint, boolean flushCache) {
        return new Point(fs.xsize, fs.ysize);
      }

      protected void layout (Composite composite, boolean flushCache) {
        Control[]controls = composite.getChildren();
        if (controls.length <= 0) return;
        Control control = controls[0];
        Rectangle rect = composite.getClientArea ();
        control.setBounds(
            rect.x + margin.width, rect.y + margin.height,
            rect.width - margin.width * 2, rect.height - margin.height * 2);
      }
    };

    composite.setLayout(layout);
    Control control = SwLayouter.create(composite, fs.item);

  }

}
