package com.cm55.swt.layout;

import org.eclipse.swt.layout.*;
import org.eclipse.swt.widgets.*;

import com.cm55.swt.*;

/**
 * 水平レイアウト
 */
public class SwHorizontalLayout extends SwCompositeLayout {
  
  /** 中身 */
  public SwItem[]items;
  
  public SwHorizontalLayout(LSpacing spacing, SwItem[]items) {
    super(spacing);
    this.items = items;
  }
  public SwHorizontalLayout(SwItem[]items) {
    super(null);
    this.items = items;
  }
  

  /** 水平レイアウト */
  @Override
  public  void layout(Composite composite, LMargin margin) {

    SwHorizontalLayout hl = this;
    
    int count = 0;
    for (int itemIndex = 0; itemIndex <  hl.items.length; itemIndex++) {
      if (hl.items[itemIndex] == null) continue;
      Control[]controls = createArray( composite, hl.items[itemIndex]);
      for (int cindex = 0; cindex < controls.length; cindex++) {
        /*
        GridData data = new GridData();
        data.horizontalAlignment = GridData.FILL;
        */
        GridData data = new GridData(
          GridData.HORIZONTAL_ALIGN_FILL |
          GridData.VERTICAL_ALIGN_FILL |
          GridData.GRAB_VERTICAL |
          GridData.GRAB_HORIZONTAL// !!!
        );

        controls[cindex].setLayoutData(data);
        count++;
      }
    }
    GridLayout layout = new GridLayout(count, true);
    margin = ensureMargin(margin);
    layout.marginWidth = margin.width;
    layout.marginHeight = margin.height;
    LSpacing spacing = ensureSpacing(hl.spacing);
    layout.horizontalSpacing = spacing.hSpacing;
    layout.verticalSpacing = spacing.vSpacing;
    composite.setLayout(layout);
  }
}
