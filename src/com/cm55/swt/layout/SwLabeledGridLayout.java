package com.cm55.swt.layout;

import org.eclipse.swt.layout.*;
import org.eclipse.swt.widgets.*;

import com.cm55.swt.*;

/**
 * ラベル付きグリッドレイアウト
 */
public class SwLabeledGridLayout extends SwCompositeLayout {
  
  public SwItem[]items;
  
  public SwLabeledGridLayout(LSpacing spacing, SwItem[]items) {
    super(spacing);
    
    if (items.length % 2 != 0) 
      throw new IllegalArgumentException("Size of items is not even number");
    
    this.items = items;
  }
  
  public SwLabeledGridLayout(SwItem[]items) {
    this(null, items);
  }
  
  /** ラベル付グリッドレイアウト */
  @Override
  public  void layout(Composite composite, LMargin margin) {

    SwLabeledGridLayout lgl = this;
    
    GridLayout layout = new GridLayout(2, false);
    margin = ensureMargin(margin);
    layout.marginHeight = margin.height;
    layout.marginWidth  = margin.width;
    LSpacing spacing = ensureSpacing(lgl.spacing);
    layout.horizontalSpacing = spacing.hSpacing;
    layout.verticalSpacing   = spacing.vSpacing;
    composite.setLayout(layout);
    for (int i = 0; i < lgl.items.length; i += 2) {
      if (lgl.items[i] == null) continue;
      GridData data;
      Control label = SwLayouter.create(composite, lgl.items[i + 0]);
      data = new GridData();
      data.horizontalAlignment = GridData.FILL;
      label.setLayoutData(data);

      Control content = SwLayouter.create(composite, lgl.items[i + 1]);
      data = new GridData();
      data.horizontalAlignment = GridData.FILL;
      data.grabExcessHorizontalSpace = true;
      content.setLayoutData(data);
    }
  }
}
