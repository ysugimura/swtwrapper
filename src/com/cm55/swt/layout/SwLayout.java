package com.cm55.swt.layout;

import org.eclipse.swt.widgets.*;

import com.cm55.swt.*;

/**
 * レイアウト
 */
public abstract class SwLayout extends SwItem {
  
  public abstract void layout(Composite composite, LMargin margin);
  
  /** マージンがnullの場合、NO_MARGINを返す */
  protected LMargin ensureMargin(LMargin margin) {
    if (margin == null) return LMargin.NO_MARGIN;
    return margin;
  }
  
  /** スペーシングがnullの場合、SPACING_5を返す */
  protected LSpacing ensureSpacing(LSpacing spacing) {
    if (spacing == null) return LSpacing.SPACING_5;
    return spacing;
  }
  
  public Control[]createArray(Composite parent, SwItem udItem) {
    Control c = SwLayouter.create(parent, udItem);
    if (c == null) return new Control[0];
    return new Control[] { c };
  }
}
