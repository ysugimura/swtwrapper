package com.cm55.swt.layout;

import org.eclipse.swt.widgets.*;

import com.cm55.swt.*;

/**
 * マージンを定義する
 */
public class SwMarginLayout extends SwLayout {
  public LMargin margin;
  
  public SwItem item;
  public SwMarginLayout(LMargin margin, SwItem item)  {
    super();
    this.margin = margin;
    this.item = item;
  }
  public SwMarginLayout(SwItem item) {
    this(null, item);
  }
  

  /** マージンレイアウト */
  @Override
  public  void layout(Composite composite, LMargin margin) {
  //  if (log.isTraceEnabled()) log.trace("marginLayout " + ml);
    SwMarginLayout ml = this;
    margin = ml.margin;
    if (margin == null) margin = SwLayouter.getDefaultMargin();

    if (ml.item instanceof SwLayout) {
      // アイテムがレイアウトの場合、レイアウトに対して自マージンを指定する。
      SwLayouter.layout(composite, (SwLayout)ml.item, margin);
      return;
    } else {
      // アイテムがコントロールの場合、フィルレイアウトをはさむ
      SwLayouter.layout(composite, new SwFillLayout(ml.item), margin);
    }

  }
}
