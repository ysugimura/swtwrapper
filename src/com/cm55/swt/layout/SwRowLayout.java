package com.cm55.swt.layout;

import org.eclipse.swt.*;
import org.eclipse.swt.layout.*;
import org.eclipse.swt.widgets.*;

import com.cm55.swt.*;

/**
 * 行
 */
public class SwRowLayout extends SwCompositeLayout {
  
  /** 中身 */
  public final SwItem[]items;
  public boolean wrap = false;
  
  public SwRowLayout(LSpacing spacing, SwItem[]items) {
    super(spacing);
    this.items = items;
  }
  public SwRowLayout(SwItem[]items) {
    super(null);
    this.items = items;
  }
  
  public SwRowLayout setWrap(boolean value) {
    wrap = value;
    return this;
  }
  
  public boolean getWrap() {
    return wrap;
  }
  

  
  /** 行レイアウト */
  @Override
  public  void layout(Composite composite, LMargin margin) {
    SwRowLayout rl = this;
    
    int count = 0;
    for (int itemIndex = 0; itemIndex <  rl.items.length; itemIndex++) {
      if (rl.items[itemIndex] == null) continue;
      Control[]controls = createArray( composite, rl.items[itemIndex]);
      count += controls.length;
    }
    RowLayout layout = new RowLayout(SWT.HORIZONTAL);
    layout.wrap = rl.getWrap();
    margin = ensureMargin(margin);
    layout.marginWidth = margin.width;
    layout.marginHeight = margin.height;
    LSpacing spacing = ensureSpacing(rl.spacing);
    layout.spacing = spacing.hSpacing;
    composite.setLayout(layout);
  }
}
