package com.cm55.swt.layout;

import org.eclipse.swt.layout.*;
import org.eclipse.swt.widgets.*;

import com.cm55.swt.*;
import com.cm55.swt.misc.*;

/**
 * 垂直レイアウト
 */
public class SwVerticalLayout extends SwCompositeLayout {
  
  /** 中身 */
  public SwItem[]items;
  
  public SwVerticalLayout(LSpacing spacing, SwItem[]items) {
    super(spacing);
    this.items = items;
  }
  public SwVerticalLayout(SwItem...items) {
    this(null, items);
  }
  

  /** 垂直レイアウトを行う */
  @Override
  public void layout(Composite composite, LMargin margin) {
    SwVerticalLayout vl = this;
    GridLayout layout = new GridLayout(1, false);

    margin = ensureMargin(margin);
    layout.marginWidth = margin.width;
    layout.marginHeight = margin.height;

    LSpacing spacing = ensureSpacing(vl.spacing);
    layout.horizontalSpacing = spacing.hSpacing;
    layout.verticalSpacing = spacing.vSpacing;
    composite.setLayout(layout);
    SwItem[]vlItems = ArraysEx.dropNull(vl.items);
    for (int itemIndex = 0; itemIndex < vlItems.length; itemIndex++) {
      Control[]controls = createArray( composite, vlItems[itemIndex]);
      for (int cindex = 0; cindex < controls.length; cindex++) {
        GridData data = new GridData();
        data.horizontalAlignment = GridData.FILL;
        data.grabExcessHorizontalSpace = true;
        controls[cindex].setLayoutData(data);
      }
    }
  }
}
