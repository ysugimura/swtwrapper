package com.cm55.swt.menu;

import java.util.*;
import java.util.List;
import java.util.function.*;
import java.util.stream.*;

import org.apache.commons.logging.*;
import org.eclipse.swt.events.*;
import org.eclipse.swt.graphics.*;
import org.eclipse.swt.widgets.*;

import com.cm55.swt.*;

/**
 * 抽象メニュー
 */
public abstract class AbstractMenu<T> extends TopMenuHolder implements ShowMenu<T> {

  private static final Log log = LogFactory.getLog(AbstractMenu.class);

  /** ユーザ側リスナ */
  protected Consumer<T> userListener;

  /** アイテム選択時のリスナ */
  protected final SelectionListener menuItemListener = new SelectionAdapter() {
    @SuppressWarnings("unchecked")
    @Override
    public void widgetSelected(SelectionEvent e) {
      selected((T)e.widget.getData());
    }
  };
  
  /** オブジェクト作成 */
  protected AbstractMenu(Shell shell) {
    super(shell);
  }
  
  /** メニュー選択イベント */
  protected void selected(T object) {
    if (userListener != null) userListener.accept(object);
    es.dispatchEvent(new MenuSelectEvent<T>(object));
  }


  /** 指定コントロールの指定場所にメニューを表示する。 */
  @Override
  public void showFor(Control control, int controlSide) {
    MenuUtil.showFor(getTopMenu(),  control, controlSide);
  }
  
  @Override
  public void showFor(SwControl<?> control, int controlSide, Consumer<T> l) {
    userListener = l;
    MenuUtil.showFor(getTopMenu(), control.getControl(), controlSide);
  }
  
  @Override
  public void showFor(Control control, int controlSide, Consumer<T> l) {
    userListener = l;
    MenuUtil.showFor(getTopMenu(), control, controlSide);
  }

  /** 指定コントロールの指定位置にメニューを表示する */
  @Override
  public void showAt(Control control, Point position, Consumer<T> l) {
    userListener = l;
    MenuUtil.showAt(getTopMenu(), control, position);
  }

  /////////////////////////////////////////////////////////////////////////////
  // ユーティリティ
  /////////////////////////////////////////////////////////////////////////////

  protected List<MenuItem> createPushMenuItems(Menu menu, Object[]objects) {    
    return createPushMenuItems(menu, Arrays.stream(objects).map(o->new MenuItemElement(o)).collect(Collectors.toList()));
  }

  /** 指定されたMenu下に指定オブジェクトのプッシュメニューを作成する。
   *  オブジェクト数が多すぎる場合は適当にカスケードメニューを作成し、
   *  その下に作成する。
   */
  protected List<MenuItem> createPushMenuItems(Menu menu, Object[]objects, String[]labels) {
    return createPushMenuItems(menu, new MenuItemSource(objects, labels));
  }
  
  protected List<MenuItem> createPushMenuItems(Menu menu, List<MenuItemElement>list) {
    return createPushMenuItems(menu, new MenuItemSource(list));
  }
  
  protected List<MenuItem> createPushMenuItems(Menu menu, MenuItemSource source) {
    return MenuUtil.createPushMenuItems(menu, source, e->menuItemListener.widgetSelected(e));
  }
  
  
}
