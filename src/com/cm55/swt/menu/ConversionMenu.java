package com.cm55.swt.menu;

import java.util.function.*;

import org.eclipse.swt.graphics.*;
import org.eclipse.swt.widgets.*;

import com.cm55.swt.*;

public abstract class ConversionMenu<I,O> implements ShowMenu<O> {
  protected AbstractMenu<I>menu;
  public ConversionMenu(AbstractMenu<I> menu) {
    this.menu = menu;
  }
  public abstract O getOutput(I i);
  
  @Override
  public void showFor(Control control, int controlSide) {
    throw new RuntimeException();      
  }
  @Override
  public void showFor(SwControl<?> control, int controlSide, Consumer<O> l) {
    menu.showFor(control, controlSide, i->l.accept(getOutput(i)));      
  }
  @Override
  public void showFor(Control control, int controlSide, Consumer<O> l) {
    menu.showFor(control, controlSide, i->l.accept(getOutput(i)));    
  }
  @Override
  public void showAt(Control control, Point position, Consumer<O> l) {
    menu.showAt(control, position,  i->l.accept(getOutput(i)));   
  }    
}