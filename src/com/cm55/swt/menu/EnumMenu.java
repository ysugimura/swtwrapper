// Created by Cryptomedia Co., Ltd. 2006/01/01
package com.cm55.swt.menu;

import java.lang.reflect.*;
import java.util.*;

import org.eclipse.swt.widgets.*;

/**
 * 指定されたEnumの全アイテムから一つを選択するメニュー。
 * noItem指定のあるときは「選択なし」のメニューアイテムも作成される。
 * @author Administrator
 *
 * @param <S>
 */
@SuppressWarnings("rawtypes")
public class EnumMenu<S extends Enum> extends AbstractMenu<S> {
  
  @SuppressWarnings("unchecked")
  public EnumMenu(Shell shell, Class<S> clazz, boolean noItem) {
    super(shell);
    
    S[]items;
    try {
      Method valuesMethod = clazz.getMethod("values");
      items = (S[])valuesMethod.invoke(null);
    } catch (Throwable th) {
      throw new RuntimeException(th);
    }
    
    ArrayList<S>slots = new ArrayList<S>();
    ArrayList<String>names = new ArrayList<String>();
    if (noItem) {
      slots.add(null);
      names.add("（なし）");
    }
    for (S item: items) {
      slots.add(item);
      names.add(getLabel(item));
    }    
    createPushMenuItems(createTopMenu(), 
        slots.toArray(new Enum[0]), 
        names.toArray(new String[0])
    );    
  } 
  
  /** Enumアイテムからメニューアイテム文字列を取得する */
  protected String getLabel(S item) {
    return item.name();
  }
}
