package com.cm55.swt.menu;

public class MenuItemElement {
  public final Object object;
  public final String label;
  public MenuItemElement(Object object, String label) {
    this.object = object;
    this.label = label;
  }
  
  public MenuItemElement(Object object) {
    this(object, object.toString());
  }
}