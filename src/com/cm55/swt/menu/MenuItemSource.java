package com.cm55.swt.menu;

import java.util.*;

public class MenuItemSource {

  MenuItemElement[]elements;
  
  public MenuItemSource(List<MenuItemElement>list) {
    this.elements = list.toArray(new MenuItemElement[0]);
  }
  
  public MenuItemSource(Object[]objects, String[]labels) {
    if (labels != null) {
      if (objects.length != labels.length) throw new InternalError();
    }
    elements = new MenuItemElement[objects.length];
    for (int i = 0; i < elements.length; i++) {
      String label;
      if (labels == null) 
        label = objects[i].toString();
      else 
        label = labels[i];
      elements[i] = new MenuItemElement(objects[i], label);
    }
  }
  


}
