// Created by Cryptomedia Co., Ltd. 2005/09/28
package com.cm55.swt.menu;

public final class MenuSelectEvent<T> {
  public T object;
  public MenuSelectEvent(T o) {
    object = o;
  }
  
  public String toString() {
    return "MenuSelectEvent " + object;
  }
}
