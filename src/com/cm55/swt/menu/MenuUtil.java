package com.cm55.swt.menu;

import java.util.*;
import java.util.List;
import java.util.function.*;

import org.apache.commons.logging.*;
import org.eclipse.swt.*;
import org.eclipse.swt.events.*;
import org.eclipse.swt.graphics.*;
import org.eclipse.swt.widgets.*;

import com.cm55.swt.*;
import com.cm55.swt.misc.*;

public class MenuUtil {

  static Log log = LogFactory.getLog(MenuUtil.class);
  
  static final int MAX_ITEM_COUNT = 40;
  static final String AUTO_CASCADED_LABEL = ">>>";
  
  public static class StartCount {
    public final int start;
    public final int count;
    StartCount(int start, int count) {
      this.start = start;
      this.count = count;
    }
    StartCount advance(int amount) {
      return new StartCount(start + amount, count - amount);
    }
  }
  
  public static final List<MenuItem> createPushMenuItems(Menu menu, MenuItemSource menuItemSources, Consumer<SelectionEvent> menuItemListener) {
    return createPushMenuItems(menu, menuItemSources, menuItemListener, new StartCount(0, menuItemSources.elements.length));
  }
  
  private static final List<MenuItem> createPushMenuItems(Menu menu, MenuItemSource menuItemSources, Consumer<SelectionEvent> menuItemListener,
      StartCount sc) {
    java.util.List<MenuItem>menuItems = new ArrayList<MenuItem>();

    MenuItemElement[]elements = menuItemSources.elements;
    /*
    if (log.isTraceEnabled()) {
      log.trace("createPushMenuItems " +
          "objects:" + objects.length + ", labels:" + labels.length +
          ", start:" + start + " count:" + count);
      for (int i = 0; i < objects.length; i++) {
        log.trace("  object:" + objects[i] + ", label:" + labels[i]);
      }
    }
    */
  
    int space = MAX_ITEM_COUNT - menu.getItemCount();

    if (log.isTraceEnabled()) log.trace(" space " + space);
    
    // 分割不要
    if (sc.count <= space) {
      for (int i = 0; i < sc.count; i++) {
        /*
        if (log.isTraceEnabled())
          log.trace("createPushMenuItem " + labels[start + i]);
        */
        MenuItemElement e = elements[sc.start + i];
        MenuItem item = MenuUtil.createPushMenuItem(menu, e.label, menuItemListener);
        item.setData(e.object);

        
        menuItems.add(item);
      }
      if (log.isTraceEnabled()) log.trace("menu " + System.identityHashCode(menu) + " itemCount:" + menu.getItemCount());
      return menuItems;
    }

    // 分割数を決定する。親側の分割数ができるだけ小さくなるようにする。
    // まず、countを格納可能な最小のMAX_ITEM_COUNTの累乗を求める。
    int childAmount = 1;
    while (childAmount < sc.count) childAmount *= MAX_ITEM_COUNT;

    // 親側の分割数を調べるため、累乗数を-1する。
    childAmount /= MAX_ITEM_COUNT;
    if (childAmount <= 0) childAmount = 1;
    
    // 親側の分割数を調べ、それが現在のスペースに格納可能かチェックする。
    // 格納可能でなければ累乗数を+1する。
    if (log.isTraceEnabled()) log.trace("childAmount " + childAmount);
    int divided = (sc.count + childAmount - 1) / childAmount;
    if (divided > space) {
      childAmount *= MAX_ITEM_COUNT;
      divided = (sc.count + childAmount - 1) / childAmount;
    }

    // 作成
    for (int i = 0; i < divided; i++) {
      //ystem.out.println("i " + i);
      MenuItem cascadeItem = MenuUtil.createCascadeMenuItem(menu, AUTO_CASCADED_LABEL);
      Menu submenu = new Menu(cascadeItem);
      cascadeItem.setMenu(submenu);
      menuItems.addAll(
          createPushMenuItems(submenu, menuItemSources, menuItemListener, new StartCount(sc.start, Math.min(childAmount, sc.count)))
      );
      sc = sc.advance(childAmount);
      /*
      start += childAmount;
      count -= childAmount;
      */
    }
    
    return menuItems;
  }
  
  /** 単純なメニューアイテムを作成する */
  public static MenuItem createPushMenuItem(Menu menu, String text, SelectionListener listener) {
    return createPushMenuItem(menu, text, e->listener.widgetSelected(e));
  }

  public static MenuItem createPushMenuItem(Menu menu, String text, Consumer<SelectionEvent>listener) {
    //if (log.isTraceEnabled())
     // log.trace("createPushMenuItem " + menu + "," + text + "," + listener);

    assert(listener != null);

    MenuItem item = new MenuItem(menu, SWT.PUSH);
    item.setText(text);
    if (listener != null) {
      item.addSelectionListener(new SelectionAdapter() {
        @Override
        public void widgetSelected(SelectionEvent e) {
          listener.accept(e);
        }
      });
    }
    return item;
  }
  
  public static MenuItem createCascadeMenuItem(Menu menu, String text) {

   //if (log.isTraceEnabled())
    //  log.trace("createCascadeMenuItem " + menu + "," + text);

    MenuItem item = new MenuItem(menu, SWT.CASCADE);
    item.setText(text);
    /*
    if (listener != null)
      item.addSelectionListener(listener);
    */
    return item;
  }
  

  
  public static void showFor(Menu topMenu, SwControl<?> control, int controlSide) {
    showFor(topMenu, control.getControl(), controlSide);
  }
  
  public static void showFor(Menu topMenu, Control control, int controlSide) {
    if (topMenu == null) return;
    Point position = new Point(0, 0);
    Point size = control.getSize();
    switch (controlSide) {
    default:
      position.y += size.y;
      break;
    case SWT.RIGHT:
      position.x += size.x;
      break;
    }
    showAt(topMenu, control, position);
  }

  /** 指定コントロールの指定位置にメニューを表示する */
  public static void showAt(Menu topMenu, Control control, Point position) {
    if (topMenu == null) return;

    if (log.isTraceEnabled())
      log.trace("showAt topMenu:" + System.identityHashCode(topMenu));

    if (topMenu.getItemCount() == 0) {
      SwUtil.showError(topMenu.getShell(), "メニューアイテムがありません");
      return;
    }


    //Point screenPos = SWTUtil.toDisplay(control, position.x, position.y);
    Point screenPos = control.toDisplay(position.x, position.y);
    topMenu.setLocation(screenPos.x, screenPos.y);
    topMenu.setVisible(true);
  }
}
