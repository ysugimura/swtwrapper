package com.cm55.swt.menu;

import java.util.function.*;

import org.eclipse.swt.graphics.*;
import org.eclipse.swt.widgets.*;

import com.cm55.swt.*;

public interface ShowMenu<T> {

  /** 指定コントロールの指定場所にメニューを表示する */
  public void showFor(Control control, int controlSide);
  
  /** 指定コントロールの指定場所にメニューを表示し、選択時にハンドラを呼び出す */
  public void showFor(SwControl<?> control, int controlSide, Consumer<T> l);
  
  /** 指定コントロールの指定場所にメニューを表示する。選択時にハンドラを呼び出す */
  public void showFor(Control control, int controlSide, Consumer<T> l) ;

  /** 指定コントロールの指定位置にメニューを表示する。選択時にハンドラを呼び出す */
  public void showAt(Control control, Point position, Consumer<T> l);

}
