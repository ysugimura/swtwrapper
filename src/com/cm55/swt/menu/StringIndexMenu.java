package com.cm55.swt.menu;

import org.eclipse.swt.widgets.*;

/**
 * 文字列配列のメニュー
 */
public class StringIndexMenu extends AbstractMenu<Integer> {
  public StringIndexMenu(Shell shell, String[]strings) {
    super(shell);
    Integer[]objects = new Integer[strings.length];
    for (int i = 0; i < strings.length; i++)
      objects[i] = new Integer(i);
    createPushMenuItems(createTopMenu(), objects, strings);
  }
}
