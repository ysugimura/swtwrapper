// Created by Cryptomedia Co., Ltd. 2005/12/24
package com.cm55.swt.menu;

import java.util.*;
import java.util.stream.*;

import org.eclipse.swt.widgets.*;

public class StringObjectMenu<T> extends AbstractMenu<T> {
  
  public StringObjectMenu(Shell shell, T[]objects) {
    this(shell, objects, objectString(objects));
  }
  
  static <T>String[]objectString(T[]objects) {
    return Arrays.stream(objects).map(o->o.toString()).collect(Collectors.toList()).toArray(new String[0]);
  }
  
  public StringObjectMenu(Shell shell, T[]objects, String[]strings) {
    super(shell);
    assert(strings.length == objects.length);
    createPushMenuItems(createTopMenu(), objects, strings);
  }
}
