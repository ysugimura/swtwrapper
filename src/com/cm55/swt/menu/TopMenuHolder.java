package com.cm55.swt.menu;

import java.util.function.*;

import org.apache.commons.logging.*;
import org.eclipse.swt.*;
import org.eclipse.swt.events.*;
import org.eclipse.swt.widgets.*;

import com.cm55.eventBus.*;
import com.cm55.swt.misc.*;

public class TopMenuHolder {
  
  private static final Log log = LogFactory.getLog(TopMenuHolder.class);
  
  protected final Shell shell;

  
  protected final EventBus es = new SwEventBus();
  
  public <S> void listen(Class<S> eventType, Consumer<S> consumer) {
    es.listen(eventType, consumer);
  }



  /** メニュー */
  private Menu topMenu;
  
  protected TopMenuHolder(Shell shell) {
    this.shell = shell;
  }
  
  public Menu getTopMenu() {
    return topMenu;
  }

  protected Menu createTopMenu() {
    dispose();
    topMenu = new Menu(shell, SWT.POP_UP);
    if (log.isTraceEnabled())
      log.trace("createTopMenu " + System.identityHashCode(topMenu));
    topMenu.addDisposeListener(new DisposeListener() {
      public void widgetDisposed(DisposeEvent e) {
        es.dispatchEvent(new TopMenuDisposedEvent());
      }
    });
    topMenu.addMenuListener(new MenuListener() {
      public void menuHidden(MenuEvent e) {
        // こうしないとメニューアイテム選択イベントより先にHiddenイベントが送信されてしまう。
        topMenu.getDisplay().asyncExec(new Runnable() {
          public void run() {
            es.dispatchEvent(new MenuHiddenEvent());
          }
        });
      }
      public void menuShown(MenuEvent e) {
        es.dispatchEvent(new MenuShownEvent());
      }
    });
    es.dispatchEvent(new TopMenuCreatedEvent());
    return topMenu;
  }

  /** 廃棄 */
  public void dispose() {
    if (topMenu != null) topMenu.dispose();
  }
}
