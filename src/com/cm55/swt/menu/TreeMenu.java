package com.cm55.swt.menu;

import java.util.*;
import java.util.List;

import org.apache.commons.logging.*;
import org.eclipse.swt.*;
import org.eclipse.swt.widgets.*;

public class TreeMenu<NODE, OBJ> extends AbstractMenu<OBJ> {

  private static final Log log = LogFactory.getLog(TreeMenu.class);

  private TreeMenuAdapter<NODE, OBJ>adapter;
  
  public TreeMenu(Shell shell, NODE root, TreeMenuAdapter<NODE, OBJ>adapter) {
    super(shell);
    this.adapter = adapter;
    create(root);
  }

  private final void create(NODE root) {
    createChildren(createTopMenu(), root);
  }
   
  protected TreeMenu(Shell shell) {
    super(shell);
  }
  
  protected void createChildren(Menu menu, NODE parent, TreeMenuAdapter<NODE, OBJ>adapter) {
      this.adapter = adapter;
      createChildren(menu, parent);
  }
  
  private void createChildren(Menu menu, NODE parent) {
    
    if (parent == null) return;
    

    NODE[]children = adapter.getChildren(parent);

    // PushItemのストック。一つずつ作成していっても良いのだが、
    // 大量のPushItemが出現した場合に、それを適当にカスケードメニューにわける
    // ために一時的に保持する。
    List<MenuItemElement> pushItemStock = new ArrayList<>();

    // 親の選択用PushItemを作成する。
    if (adapter.selectParent()) {
      MenuItem item = MenuUtil.createPushMenuItem(menu, adapter.getLabel(parent) + "を選択", menuItemListener);
      item.setData(adapter.getObject(parent));
      new MenuItem(menu, SWT.SEPARATOR);
    }

    for (int i = 0; i < children.length; i++) {
      // 子ノードと中身オブジェクトを取り出す
      NODE child = children[i];

      // PushItemにする必要がある。
      if (adapter.isSubject(child)) {

        //ystem.out.println("isSubhect " + object);

        // 子を持たない場合、普通のPushItemとする。
        if (adapter.getChildren(child).length == 0) {
          pushItemStock.add(new MenuItemElement(adapter.getObject(child), adapter.getLabel(child)));
          continue;
        }

        flushStockItems(menu, pushItemStock);

        // 子を持つ場合、カスケードアイテムにする。
        MenuItem item = MenuUtil.createCascadeMenuItem(menu, adapter.getLabel(child));
        Menu submenu = new Menu(item);
        item.setMenu(submenu);
        createChildren(submenu, child);
        continue;
      }

      // PushItem以外のものが出現した。
      // ストックされているPushItemについて作成してストックをクリア
      flushStockItems(menu, pushItemStock);

      // CascadeItemを作成する。子があればそれらも作成
      if (shouldCreateCascade(child)) {
        MenuItem item = MenuUtil.createCascadeMenuItem(menu, adapter.getLabel(child));
        if (adapter.getChildren(child).length == 0) continue;
        Menu submenu = new Menu(item);
        item.setMenu(submenu);
        createChildren(submenu, child);
      }
    }

    // 未処理分のプッシュメニューを作成
    flushStockItems(menu, pushItemStock);
  }

  protected void flushStockItems(Menu menu, List<MenuItemElement>pushItemStock) {
    if (pushItemStock.size() > 0) {
      createPushMenuItems(menu, pushItemStock);
      pushItemStock.clear();
    }
  }

  /** カスケードメニューアイテムを作成すべきか判定する。 */
  private boolean shouldCreateCascade(NODE node) {
    NODE[]children = adapter.getChildren(node);
    for (int i = children.length - 1; i >= 0; i--) {
      NODE child = children[i]; 
      if (adapter.isSubject(child)) return true;
      if (shouldCreateCascade(child)) return true;
    }
    return false;
  }

}
