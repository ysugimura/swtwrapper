package com.cm55.swt.menu;

public interface TreeMenuAdapter<NODE, OBJ> {

  public OBJ getObject(NODE node);
  public boolean isSubject(NODE node);
  public String getLabel(NODE node);
  public boolean selectParent();
  public NODE[]getChildren(NODE parent);

}
