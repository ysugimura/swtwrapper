package com.cm55.swt.menu;

/**
 * <p>タイトル: </p>
 * <p>説明: </p>
 * <p>著作権: Copyright (c) 2003</p>
 * <p>会社名: cryptomedia</p>
 * @author yuichi sugimura
 * @version 1.0
 */

public class UDCascadeMenuItem extends UDMenuItem {
  public UDMenuItem[]cascadeItems;
  public UDCascadeMenuItem(String text, UDMenuItem[]cascadeItems) {
    super(text, null);
    this.cascadeItems = cascadeItems;
  }
}
