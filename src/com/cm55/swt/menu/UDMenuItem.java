package com.cm55.swt.menu;

import java.util.function.*;

import com.cm55.swt.event.*;

/**
 * <p>タイトル: </p>
 * <p>説明: </p>
 * <p>著作権: Copyright (c) 2003</p>
 * <p>会社名: cryptomedia</p>
 * @author yuichi sugimura
 * @version 1.0
 */

public abstract class UDMenuItem  {
  

  public String text;
  public Consumer<SwSelectionEvent>handler;
    
  public UDMenuItem(String text, Consumer<SwSelectionEvent>handler) {

    this.text = text;
    this.handler = handler;
  }
}
