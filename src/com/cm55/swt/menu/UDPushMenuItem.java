package com.cm55.swt.menu;

import java.util.function.*;

import com.cm55.swt.event.*;

/**
 * <p>タイトル: </p>
 * <p>説明: </p>
 * <p>著作権: Copyright (c) 2003</p>
 * <p>会社名: cryptomedia</p>
 * @author yuichi sugimura
 * @version 1.0
 */

public class UDPushMenuItem extends UDMenuItem {

  public UDPushMenuItem(String text, Consumer<SwSelectionEvent>handler) {
    super(text, handler);
  }
}
