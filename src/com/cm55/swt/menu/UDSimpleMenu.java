package com.cm55.swt.menu;

import java.lang.reflect.*;
import java.util.function.*;

import org.eclipse.swt.*;
import org.eclipse.swt.events.*;
import org.eclipse.swt.widgets.*;

import com.cm55.swt.event.*;

public class UDSimpleMenu {

  /////////////////////////////////////////////////////////////////////////////
  // メニュー
  /////////////////////////////////////////////////////////////////////////////

  /** メニュー */
  public static Menu createMenu(Shell shell, UDMenu udMenu) {
    int style;

    // メニュースタイルを決定
    if (udMenu instanceof UDMenuBar) style = SWT.BAR;
    else throw new InternalError();

    // メニュー作成
    Menu menu = new Menu(shell, style);
    if (udMenu instanceof UDMenuBar) shell.setMenuBar(menu);

    // アイテム作成
    for (int i = 0; i < udMenu.items.length; i++)
      createMenuItem(menu, udMenu.items[i]);
    return menu;
  }

  /** メニューアイテム作成 */
  private static MenuItem createMenuItem(Menu menu, UDMenuItem udItem) {
    int style;

    // メニューアイテムのスタイルを決定
    if (udItem instanceof UDPushMenuItem) style = SWT.PUSH;
    else if (udItem instanceof UDCascadeMenuItem) style = SWT.CASCADE;
    else if (udItem instanceof UDCheckMenuItem) style = SWT.CHECK;
    else if (udItem instanceof UDRadioMenuItem) style = SWT.RADIO;
    else throw new InternalError();

    // アイテム作成
    MenuItem item = new MenuItem(menu, style);
    item.setText(udItem.text);
    if (udItem.handler != null)
      setSelectionMethod(item, udItem.handler);

    // カスケードアイテムの場合は子アイテムを作成
    if (udItem instanceof UDCascadeMenuItem) {
      UDCascadeMenuItem udCascadeItem = (UDCascadeMenuItem)udItem;
      Menu cascadeMenu = new Menu(item);
      item.setMenu(cascadeMenu);
      for (int i = 0; i < udCascadeItem.cascadeItems.length; i++) {
        createMenuItem(cascadeMenu, udCascadeItem.cascadeItems[i]);
      }
    }
    return item;
  }

  /** 指定されたメソッドのaddSelectionlistenerを使用して{@link Consumer}への通知を行う　*/
  public static void setSelectionMethod(Widget widget, Consumer<SwSelectionEvent>consumer) {
    try {      
      // addListener(SelectionListener)はWidgetにあるわけではなく、その下位のそれぞれのクラスに別々に実装されている。
      Method addListenerMethod = widget.getClass().getMethod(
          "addSelectionListener", new Class[] { SelectionListener.class });
      addListenerMethod.invoke(widget, new Object[] {
        new SelectionAdapter() {
          @Override
          public void widgetSelected(SelectionEvent e) { consumer.accept(new SwSelectionEvent(null, e)); }
        }
      });
    } catch (Throwable th) {
      throw new RuntimeException(th);
    }
  }
}
