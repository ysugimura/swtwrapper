package com.cm55.swt.misc;

/**
 *
 */
public class ArraysEx {

//  private static final Log log = LogFactory.getLog(ArraysEx.class);

  @SuppressWarnings("unchecked")
  public static <T>T[] dropNull(T[]input) {
  //  if (log.isTraceEnabled()) log.trace("dropNull " + input.length);

    int notNulls = 0;
    for (T in: input) {
    //  if (log.isTraceEnabled()) log.trace(" ... " + in);
      if (in != null) notNulls++;
    }

    //if (log.isTraceEnabled()) log.trace("notNulls " + notNulls);
    if (notNulls == input.length) return input;

    T[]output = (T[])java.lang.reflect.Array.newInstance(
        input.getClass().getComponentType(), notNulls
    );
    int index = 0;
    for (T in: input) if (in != null) output[index++] = in;
    return output;
  }

}
