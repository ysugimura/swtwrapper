package com.cm55.swt.misc;

import org.apache.commons.logging.*;

import com.cm55.eventBus.*;

public class SwEventBus extends EventBus {

  protected static Log log = LogFactory.getLog(SwEventBus.class);
  
  @Override
  protected Log log() {
    return log;
  }

}
