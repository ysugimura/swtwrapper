// Created by Cryptomedia Co., Ltd. 2005/12/31
package com.cm55.swt.misc;

import java.util.*;

import org.eclipse.swt.*;

/**
 * SWTキーの定義
 *
 */
public enum SwKey {

  ARROW_UP(SWT.ARROW_UP, '\0', "↑", "上矢印キー")
  ,ARROW_DOWN(SWT.ARROW_DOWN, '\0', "↓", "下矢印キー")
  ,ARROW_LEFT(SWT.ARROW_LEFT, '\0', "←", "左矢印キー")
  ,ARROW_RIGHT(SWT.ARROW_RIGHT, '\0', "→", "右矢印キー")
  ,PAGE_UP(SWT.PAGE_UP, '\0', "PUp", "Page Up")
  ,PAGE_DOWN(SWT.PAGE_DOWN, '\0', "PDown", "Page Down")
  ,HOME(SWT.HOME, '\0', "Home")
  ,END(SWT.END, '\0', "End")
  ,INSERT(SWT.INSERT, '\0', "Ins", "Insert")
  ,F1(SWT.F1, '\0', "F1")
  ,F2(SWT.F2, '\0', "F2")
  ,F3(SWT.F3, '\0', "F3")
  ,F4(SWT.F4, '\0', "F4")
  ,F5(SWT.F5, '\0', "F5")
  ,F6(SWT.F6, '\0', "F6")
  ,F7(SWT.F7, '\0', "F7")
  ,F8(SWT.F8, '\0', "F8")
  ,F9(SWT.F9, '\0', "F9")
  ,F10(SWT.F10, '\0', "F10")
  ,F11(SWT.F11, '\0', "F11")
  ,F12(SWT.F12, '\0', "F12")
  ,F13(SWT.F13, '\0', "F13")
  ,F14(SWT.F14, '\0', "F14")
  ,F15(SWT.F15, '\0', "F15")
  ,KEYPAD_MULTIPLY(SWT.KEYPAD_MULTIPLY, '*', "P*", "キーパッドの「＊」")
  ,KEYPAD_ADD(SWT.KEYPAD_ADD, '+', "P+", "キーパッドの「＋」")
  ,KEYPAD_SUBTRACT(SWT.KEYPAD_SUBTRACT, '-', "P-", "キーパッドの「－」")
  ,KEYPAD_DECIMAL(SWT.KEYPAD_DECIMAL, '.', "P.", "キーパッドの「．」")
  ,KEYPAD_DIVIDE(SWT.KEYPAD_DIVIDE, '/', "P/", "キーパッドの「／」")
  ,KEYPAD_0(SWT.KEYPAD_0, '0', "0", "キーパッドの「０」")
  ,KEYPAD_1(SWT.KEYPAD_1, '1', "1", "キーパッドの「１」")
  ,KEYPAD_2(SWT.KEYPAD_2, '2', "2", "キーパッドの「２」")
  ,KEYPAD_3(SWT.KEYPAD_3, '3', "3", "キーパッドの「３」")
  ,KEYPAD_4(SWT.KEYPAD_4, '4', "4", "キーパッドの「４」")
  ,KEYPAD_5(SWT.KEYPAD_5, '5', "5", "キーパッドの「５」")
  ,KEYPAD_6(SWT.KEYPAD_6, '6', "6", "キーパッドの「６」")
  ,KEYPAD_7(SWT.KEYPAD_7, '7', "7", "キーパッドの「７」")
  ,KEYPAD_8(SWT.KEYPAD_8, '8', "8", "キーパッドの「８」")
  ,KEYPAD_9(SWT.KEYPAD_9, '9', "9", "キーパッドの「９」")
  ,KEYPAD_EQUAL(SWT.KEYPAD_EQUAL, '=', "=", "キーパッドの「＝」")
  ,KEYPAD_ENTER(SWT.KEYPAD_CR, '\0', "Ent", "キーパッドの「Enter」")
  ,HELP(SWT.HELP, '\0', "Help")
  ,CAPS_LOCK(SWT.CAPS_LOCK, '\0', "Caps", "Caps Lock")
  ,NUM_LOCK(SWT.NUM_LOCK, '\0', "Num", "Num Lock")
  ,SCROLL_LOCK(SWT.SCROLL_LOCK, '\0', "SLock", "Scroll Lock")
  ,PAUSE(SWT.PAUSE, '\0', "Pause")
  ,BREAK(SWT.BREAK, '\0', "Break")
  ,PRINT_SCREEN(SWT.PRINT_SCREEN, '\0', "PScr", "Print Screen")

  ,BACKSPACE(SWT.BS, '\0', "Back", "Back Space")
  ,ENTER(SWT.CR, '\0', "Ent", "Enter")
  ,DELETE(SWT.DEL, '\0', "Delete", "Delete")
  ,ESC(SWT.ESC, '\0', "Esc")

  ,CHAR_0('0', '0', "0", "キャラクタの「０」")
  ,CHAR_1('1', '1', "1", "キャラクタの「１」")
  ,CHAR_2('2', '2', "2", "キャラクタの「２」")
  ,CHAR_3('3', '3', "3", "キャラクタの「３」")
  ,CHAR_4('4', '4', "4", "キャラクタの「４」")
  ,CHAR_5('5', '5', "5", "キャラクタの「５」")
  ,CHAR_6('6', '6', "6", "キャラクタの「６」")
  ,CHAR_7('7', '7', "7", "キャラクタの「７」")
  ,CHAR_8('8', '8', "8", "キャラクタの「８」")
  ,CHAR_9('9', '9', "9", "キャラクタの「９」")
  ,CHAR_DECIMAL('.', '.', ".", "キャラクタの「．」")
  
  ,CHAR_A('a', 'A', "A")  
  ,CHAR_B('b', 'B', "B")
  ,CHAR_C('c', 'C', "C")
  ,CHAR_D('d', 'D', "D")
  ,CHAR_E('e', 'E', "E")
  ,CHAR_F('f', 'F', "F")
  ,CHAR_G('g', 'G', "G")
  ,CHAR_H('h', 'H', "H")
  ,CHAR_I('i', 'I', "I")
  ,CHAR_J('j', 'J', "J")
  ,CHAR_K('k', 'K', "K")
  ,CHAR_L('l', 'L', "L")
  ,CHAR_M('m', 'M', "M")
  ,CHAR_N('n', 'N', "N")
  ,CHAR_O('o', 'O', "O")
  ,CHAR_P('p', 'P', "P")
  ,CHAR_Q('q', 'Q', "Q")
  ,CHAR_R('r', 'R', "R")
  ,CHAR_S('s', 'S', "S")
  ,CHAR_T('t', 'T', "T")
  ,CHAR_U('u', 'U', "U")
  ,CHAR_V('v', 'V', "V")
  ,CHAR_W('w', 'W', "W")
  ,CHAR_X('x', 'X', "X")
  ,CHAR_Y('y', 'Y', "Y")
  ,CHAR_Z('z', 'Z', "Z")
  ,CHAR_HYPHEN(45, '-', "-", "キャラクタの「-」")
  ;

  /** 画面上にファンクションキー対応ボタンを表示するときの順序 */
  public static final SwKey[]FUNCKEYS_ORDER = new SwKey[] {
    F1, F2, F3, F4, F5, F6, F7, F8, F9, F10, F11, F12, F13, F14, F15
  };
  
  /** 画面上にその他キー対応ボタンを表示するときの順序 */
  public static final SwKey[]OTHERKEYS_ORDER = new SwKey[] {
    CAPS_LOCK,
    ESC,
    CHAR_1,
    CHAR_2,
    CHAR_3,
    CHAR_4,
    CHAR_5,
    CHAR_6,
    CHAR_7,
    CHAR_8,
    CHAR_9,
    CHAR_0,
    CHAR_HYPHEN,
    CHAR_A,
    CHAR_B,
    CHAR_C,
    CHAR_D,
    CHAR_E,
    CHAR_F,
    CHAR_G,
    CHAR_H,
    CHAR_I,
    CHAR_J,
    CHAR_K,
    CHAR_L,
    CHAR_M,
    CHAR_N,
    CHAR_O,
    CHAR_P,
    CHAR_Q,
    CHAR_R,
    CHAR_S,
    CHAR_T,
    CHAR_U,
    CHAR_V,
    CHAR_W,
    CHAR_X,
    CHAR_Y,
    CHAR_Z,
    BACKSPACE,
    ENTER,
    CHAR_DECIMAL,
    PRINT_SCREEN,
    SCROLL_LOCK,
    PAUSE,
    INSERT,
    DELETE, 
    HOME,
    END, 
    PAGE_UP,
    PAGE_DOWN,
    ARROW_UP,
    ARROW_LEFT,
    ARROW_DOWN,
    ARROW_RIGHT,
    NUM_LOCK,
    KEYPAD_DIVIDE,
    KEYPAD_MULTIPLY,
    KEYPAD_SUBTRACT,
    KEYPAD_7,
    KEYPAD_8,
    KEYPAD_9,
    KEYPAD_4,
    KEYPAD_5,
    KEYPAD_6,
    KEYPAD_1,
    KEYPAD_2,
    KEYPAD_3,
    KEYPAD_0,
    KEYPAD_DECIMAL,
    KEYPAD_ADD,
    KEYPAD_ENTER,
    KEYPAD_EQUAL,
    HELP,
    BREAK,
  };
  
  /** チェック */
  static {
    // キーコードが重複していないことをチェック
    {
      Set<Integer>keySet = new HashSet<Integer>();
      for (SwKey key: SwKey.values()) {
        if (keySet.contains(key.keyCode)) {
          System.err.println("keyCode duplication:" + key.keyCode);
        }
        keySet.add(key.keyCode);
      }
      if (keySet.size() != SwKey.values().length) {
        System.err.println("Something wrong!");
      }
    }
    
    // キー表示順序配列が全キーを網羅していることをチェック 
    {
      Set<SwKey>set = new HashSet<SwKey>();
      for (SwKey key: FUNCKEYS_ORDER) set.add(key);
      for (SwKey key: OTHERKEYS_ORDER) set.add(key);
      for (SwKey key: SwKey.values()) {
        if (!set.contains(key)) {
          System.err.println("Not contained:" + key.name());
        }
      }
    }
  }
    
  /** キーコード */
  public final int keyCode;
  
  /** 文字。存在しないものは０ */
  public final char character;
  
  /** 略称 */
  public final String abbr;

  /** 説明 */
  public final String desc;
  
  private SwKey(int keyCode, char character, String abbr) {
    this(keyCode, character, abbr, null);
  }
  
  private SwKey(int keyCode, char character, String abbr, String desc) {
    this.keyCode = keyCode;
    this.character = character;
    this.abbr = abbr;
    this.desc = desc == null? abbr:desc;
  }

  
  /** 文字列化 */
  public String toString() {
    return name() + "=" + Integer.toHexString(keyCode);
  }

  /////////////////////////////////////////////////////////////////////////////
  // ユーティリティ
  /////////////////////////////////////////////////////////////////////////////

  private static HashMap<Integer,SwKey>keyCodeMap;

  /** キーコードからSWTKeyを取得する */
  public static SwKey getByKeyCode(int keyCode) {
    if (keyCodeMap == null) {
      keyCodeMap = new HashMap<Integer,SwKey>();
      for (SwKey def: SwKey.values())
        keyCodeMap.put(def.keyCode, def);
    }
    return keyCodeMap.get(keyCode);
  }


  /** SWTKey配列を取得する */
  public static SwKey[]getArray() {
    ArrayList<SwKey>list = new ArrayList<SwKey>();
    for (SwKey def: values()) list.add(def);
    return list.toArray(new SwKey[0]);
  }
}
