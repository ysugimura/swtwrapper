package com.cm55.swt.misc;

import org.eclipse.swt.*;
import org.eclipse.swt.events.*;
import org.eclipse.swt.graphics.*;
import org.eclipse.swt.layout.*;
import org.eclipse.swt.widgets.*;

import com.cm55.swt.*;




/**
 * SWTユーティリティ
 */
public class SwUtil {



  /////////////////////////////////////////////////////////////////////////////
  // メッセージダイアログ
  /////////////////////////////////////////////////////////////////////////////

  /** 情報ダイアログを表示 */
  public static void showMessage(Control parent, String s) {
    createMessageBox(parent, s, SWT.ICON_INFORMATION|SWT.OK).open();
  }

  public static void showError(SwControl<?> c, String s) {
    showError(c.getControl(), s);
  }
  
  /** エラーダイアログを表示 */
  public static void showError(Control parent, String s) {
    createMessageBox(parent, s, SWT.ICON_ERROR|SWT.OK).open();
  }

  /** 警告ダイアログを表示 */
  public static void showWarning(Control parent, String s) {
    createMessageBox(parent, s, SWT.ICON_WARNING|SWT.OK).open();
  }

  /** OK/Cancelダイアログを表示 */
  public static boolean confirmOK(Control parent, String s) {
    return createMessageBox(parent, s, SWT.ICON_QUESTION|SWT.OK|SWT.CANCEL).open() == SWT.OK;
  }

  /** Yes/Noダイアログを表示 */
  public static boolean confirmYes(Control parent, String s) {
    return createMessageBox(parent, s, SWT.ICON_QUESTION|SWT.YES|SWT.NO).open() == SWT.YES;
  }

  /** MessageBoxを作成する */
  private static MessageBox createMessageBox(Control parentControl, String s, int style) {
    Shell shell = parentControl.getShell();
    MessageBox box = new MessageBox(shell, style);
    box.setMessage(s);
    return box;
  }




  /////////////////////////////////////////////////////////////////////////////
  //
  /////////////////////////////////////////////////////////////////////////////

  // シェルのセンタリング */
  public static void centering(Shell shell) {

    // シェルのサイズを取得
    Point size = shell.getSize();

    // プライマリモニタの境界を取得
    Display display = shell.getDisplay();
    Monitor primary = display.getPrimaryMonitor();
    Rectangle displayBounds = primary.getBounds();

    Point position = new Point(
      displayBounds.x + (displayBounds.width - size.x) / 2,
      displayBounds.y + (displayBounds.height - size.y) / 2
    );

    // シェル位置を決定
    shell.setLocation(position.x, position.y);
  }

  /////////////////////////////////////////////////////////////////////////////
  // ディスパッチスレッドの制御
  /////////////////////////////////////////////////////////////////////////////

  /*
   * SWTでは、プログラム側でDisplayを作成するが、作成したスレッドとDisplay
   * が結びつけられ、以後のそのDisplay以下のGUI操作は、そのスレッド(GUIスレッド)から
   * しか認められない。
   * このため、メインループはそのGUIスレッドにて構築する必要がある。また、
   * このGUIスレッド以外のスレッドからGUIスレッドにて処理させたい場合は、
   * そのDisplayを指定しなくてはいけない。
   * ところが、Display.getCurrent()としたのでは、「そのスレッドに結びつけられている
   * Display」という意味になってしまうため、そもそもDisplayを作成したスレッド
   * 以外のスレッドからはDisplayが取得できない。
   * このために、メインのディスプレイとして、ここに記録しておく。
   */

  private static Display mainDisplay;
  
  public static void setMainDisplay(Display display) {
    // 二重登録を禁止
    if (mainDisplay != null) throw new InternalError();
    mainDisplay = display;
  }
    
  /** SwingUtilities.invokeLaterの機能 */
  public static void invokeLater(Runnable runnable) {
    mainDisplay.asyncExec(runnable);
  }
  
  /** ただちにGUIスレッドで実行する */
  public static void runOnGuiThread(Runnable runnable) {
    mainDisplay.syncExec(runnable);
  }

  public static void invokeLater(Display display, Runnable runnable) {
    display.asyncExec(runnable);
  }

  /////////////////////////////////////////////////////////////////////////////
  // その他
  /////////////////////////////////////////////////////////////////////////////

 // private static final Log log = LogFactory.getLog(SWTUtil.class);

  /** Displayを取得する */
  public static Display getDisplay(Control control) {
    return control.getShell().getDisplay();
  }

  /** ベルを鳴らす */
  public static void beep(Control control) {
    //Thread.dumpStack();
    getDisplay(control).beep();
  }

  /** コントロール中の指定された座標をスクリーン座標に変換する。
   *  Control#toDisplayにはひどいバグがある。*/
  public static Point toDisplay(Control control, int x, int y) {
    Point p = getOrigin(control);
    p.x += x;
    p.y += y;
    return p;
  }

  /** 指定コントロールの左上を取得する。*/
  private static Point getOrigin(Control control) {

   // if (log.isTraceEnabled())
   //   log.trace("getOrigin of control " + control.getClass());

    if (control instanceof Shell) return control.toDisplay(0, 0);


    Rectangle bounds = control.getBounds();
    int x = bounds.x;
    int y = bounds.y;

   // if (log.isTraceEnabled()) log.trace("position " + x + "," + y);

    Composite parent = control.getParent();
    Composite parentParent = parent.getParent();
    if (!(parent instanceof Shell)) {

      Layout layout = parent.getLayout();
      if (layout instanceof FormLayout) {
        //if (log.isTraceEnabled()) log.trace("FormLayout ...");

        FormLayout fl = (FormLayout)layout;

        //ystem.out.println("FormLayout " + fl.marginWidth + " " + fl.marginHeight);
        x -= fl.marginWidth;
        y -= fl.marginHeight;

      } else if (layout instanceof GridLayout) {
        //if (log.isTraceEnabled())
         // log.trace("GridLayout ... " + parent.getChildren().length);

        GridLayout gl = (GridLayout)layout;
        //if (!(parentParent instanceof Composite) || parentParent instanceof Group) {
          x -= gl.marginWidth;
          y -= gl.marginHeight;
        //}

        // 兄弟コントロールをすべて取得し、このコントロールのインデックスを求める。
        Control[]children = parent.getChildren();
        int index;
        for (index = 0; index < children.length; index++)
          if (children[index] == control)
            break;

        // 行方向の補正。２列以上あるときのみ？？？
        if (gl.numColumns > 1) {
          int controlRow = index / gl.numColumns;
          for (int row = 0; row < controlRow; row++) {
            int maxHeight = 0;
            for (int col = 0; col < gl.numColumns; col++) {
              Rectangle b = children[row * gl.numColumns + col].getBounds();
              maxHeight = Math.max(maxHeight, b.height);
            }
            y -= maxHeight;
          }
          if (controlRow > 0) y -= gl.verticalSpacing * controlRow;
        }

        // 列方向の補正。２行以上あるときのみ？？？
        int totalRows = (children.length + gl.numColumns - 1) / gl.numColumns;
        if (totalRows > 1) {
          int controlCol = index % gl.numColumns;
          for (int col = 0; col < controlCol; col++) {
            int maxWidth = 0;
            for (int row = 0; row < totalRows; row++) {
              int i = row * gl.numColumns + col;
              if (i >= children.length) continue;
              Rectangle b = children[i].getBounds();
              maxWidth = Math.max(maxWidth, b.width);
            }
            //ystem.out.println("col " + col + " maxWidth " + maxWidth);
            x -= maxWidth;
          }
          if (controlCol > 0) x -= gl.horizontalSpacing * controlCol;
        }
      }
    }

    Point parentOrigin = getOrigin(parent);
   // if (log.isTraceEnabled())
     // log.trace("parentOrigin " + parentOrigin + " ...  "+ x + "," + y);

    Point result = new Point(parentOrigin.x + x, parentOrigin.y + y);
   // if (log.isTraceEnabled()) log.trace("result " + result);
    return result;
  }

  public static Point toControl(Control control, int x, int y) {
    return control.toControl(new Point(x, y));
  }

  /////////////////////////////////////////////////////////////////////////////
  // デバッグ用
  /////////////////////////////////////////////////////////////////////////////

  /** TraverseEventの詳細表示 */
  public static String traverseEventToString(TraverseEvent e) {
    String s;
    switch (e.detail) {
    case SWT.TRAVERSE_NONE: s = "TRAVERSE_NONE"; break;
    case SWT.TRAVERSE_ESCAPE: s = "TRAVERSE_ESCAPE"; break;
    case SWT.TRAVERSE_RETURN: s = "TRAVERSE_RETURN"; break;
    case SWT.TRAVERSE_TAB_PREVIOUS: s = "TRAVERSE_TAB_PREVIOUS"; break;
    case SWT.TRAVERSE_TAB_NEXT:  s = "TRAVERSE_TAB_NEXT"; break;
    case SWT.TRAVERSE_ARROW_PREVIOUS: s = "TRAVERSE_ARROW_PREVIOUS"; break;
    case SWT.TRAVERSE_ARROW_NEXT: s =  "TRAVERSE_ARROW_NEXT"; break;
    case SWT.TRAVERSE_MNEMONIC: s = "TRAVERSE_MNEMONIC"; break;
    case SWT.TRAVERSE_PAGE_PREVIOUS: s = "TRAVERSE_PAGE_PREVIOUS"; break;
    case SWT.TRAVERSE_PAGE_NEXT: s = "TRAVERSE_PAGE_NEXT"; break;
    default: s = "unknown"; break;
    }
    return s + " " + e.doit;
  }

  /** VerifyEventの詳細表示 */
  public static String verifyEventToString(VerifyEvent e) {
    String s;
    s = "character:" + e.character + " keyCode:" + Integer.toHexString(e.keyCode) +
      " stateMask: " + Integer.toHexString(e.stateMask) + " start:" + e.start +
      " end:" + e.end + " text:" + e.text + " doit:" + e.doit;
    return s;
    /*
    	this.character = e.character;
	this.keyCode = e.keyCode;
	this.stateMask = e.stateMask;
	this.start = e.start;
	this.end = e.end;
	this.text = e.text;
	this.doit = e.doit;
    */
  }

}
