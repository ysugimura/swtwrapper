package com.cm55.swt.misc;

import static com.cm55.swt.text.SwIme.*;

import org.eclipse.swt.*;
import org.eclipse.swt.internal.win32.*;

public class SwUtilTest {
  private static void osImeMode(int mode) {
    boolean imeOn = mode != SWT.NONE && mode != SWT.ROMAN;
    if (imeOn) {
  int [] lpfdwConversion = new int [1], lpfdwSentence = new int [1];
  if (true) {
    int newBits = 0;
    int oldBits = OS.IME_CMODE_NATIVE | OS.IME_CMODE_KATAKANA;
    if ((mode & SWT.PHONETIC) != 0) {
      newBits = OS.IME_CMODE_KATAKANA | OS.IME_CMODE_NATIVE;
      oldBits = 0;
    } else {
      if ((mode & SWT.NATIVE) != 0) {
        newBits = OS.IME_CMODE_NATIVE;
        oldBits = OS.IME_CMODE_KATAKANA;
      }
    }
    if ((mode & SWT.DBCS) != 0) {
      newBits |= OS.IME_CMODE_FULLSHAPE;
    } else {
      oldBits |= OS.IME_CMODE_FULLSHAPE;
    }
    if ((mode & SWT.ROMAN) != 0) {
      newBits |= OS.IME_CMODE_ROMAN;
    } else {
      oldBits |= OS.IME_CMODE_ROMAN;
    }
    lpfdwConversion [0] |= newBits;  lpfdwConversion [0] &= ~oldBits;
    //OS.ImmSetConversionStatus (hIMC, lpfdwConversion [0], lpfdwSentence [0]);
          System.out.println("newBits ..");
          if ((newBits & OS.IME_CMODE_NATIVE) != 0)    System.out.println("  IME_CMODE_NATIVE");
          if ((newBits & OS.IME_CMODE_KATAKANA) != 0)  System.out.println("  IME_CMODE_KATAKANA");
          if ((newBits & OS.IME_CMODE_FULLSHAPE) != 0) System.out.println("  IME_CMODE_FULLSHAPE");
          if ((newBits & OS.IME_CMODE_ROMAN) != 0)     System.out.println("  IME_CMODE_ROMAN");
  }
    }
      //OS.ImmReleaseContext (handle, hIMC);
  }

  public static void main(String[]args) {
    System.out.print("IME_DONTCARE "); osImeMode(IME_DONTCARE);
    System.out.print("IME_ZENKAKU_HIRAGANA "); osImeMode(IME_ZENKAKU_HIRAGANA);
    System.out.print("IME_ZENKAKU_KATANAKA "); osImeMode(IME_ZENKAKU_KATAKANA);
    System.out.print("IME_ZENKAKU_EISU "); osImeMode(IME_ZENKAKU_EISU);
    System.out.print("IME_HANKAKU_KATAKANA "); osImeMode(IME_HANKAKU_KATAKANA);
    System.out.print("IME_OFF ");               osImeMode(IME_OFF);
    System.out.print("IME_ON ");                osImeMode(IME_ON);
  }

}
