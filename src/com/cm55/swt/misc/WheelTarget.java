package com.cm55.swt.misc;

public interface WheelTarget {

  void scroll(int count);

}
