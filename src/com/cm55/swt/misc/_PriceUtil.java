package com.cm55.swt.misc;

import java.text.*;



public class _PriceUtil {

//  private static final Log log = LogFactory.getLog(PriceUtil.class);
  
  public static final DecimalFormat format = new DecimalFormat("###,###");
  
  /** 価格を文字列化。０円のときは""、負の時は前に▲ */
  public static String toString(long price) {
    if (price == 0) return "";
    boolean minus = price < 0;
    String result = format.format(Math.abs(price));
    
  //  if (log.isTraceEnabled()) 
    //  log.trace("toString:" + result);
   
    if (minus) return "▲" + result;
    else       return result;
  }

  public static String toString(char minusSign, long price) {
    if (price == 0) return "";
    boolean minus = price < 0;
    String result = format.format(Math.abs(price));
    if (minus) return "" + minusSign + "\\" + result;
    else return "\\" + result;

  }
}
