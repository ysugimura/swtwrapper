package com.cm55.swt.other;

import org.eclipse.swt.*;
import org.eclipse.swt.events.*;
import org.eclipse.swt.widgets.*;

import com.cm55.swt.*;

/**
 * コンボボックス。
 * 文字列の選択肢からなるコンボボックス
 * @author ysugimura
 */
public class AbstractCombo extends SwControl<Combo> {

  protected int style = SWT.READ_ONLY;
  
  /** 
   * コンボボックスの選択肢が選択されたときにフォーカスを移すコントロール
   * {@link #setSelectionFocus(SwControl)}を参照のこと。
   */
  protected SwControl<?>selectionFocus;

  /** コンボボックスを作成する */
  protected Combo doCreate(Composite parent) {
    Combo combo = new Combo(parent, style);
    combo.addSelectionListener(new SelectionAdapter() {
      @Override
      public void widgetSelected(SelectionEvent e) {
        if (selectionFocus != null) selectionFocus.setFocus();
        int index = combo.getSelectionIndex();
        dispatchEvent(new SelectedEvent(index));
      }
    });
    return combo;
  }

  /**
   * コンボボックスの選択肢が選択されたときに指定されたコントロールにフォーカスを移す。
   * {@link #setFocusDelegate(Control)}を使用してしまうと、マウスでのコンボボックスドロップダウン操作ができなくなってしまう。
   * これを避けるため、マウスで選択された後で、指定されたフォーカスにコントロールを移すようにする。
   * @param value
   */
  public void setSelectionFocus(SwControl<?> value) {
    selectionFocus = value;
  }
  
  /** 選択肢ラベルを設定する */
  protected void setItems(String[]items) {
    control.setItems(items);
  }
  
  /** 選択肢の数を取得する */
  public int getItemCount() {
    return control.getItemCount();
  }

  /** プログラムからインデックスを選択する */
  public void select(int index) {
    control.select(index);
  }

  /** 現在の選択インデックスを取得する */
  public int getSelectionIndex() {
    return control.getSelectionIndex();
  }
  
  public static class SelectedEvent {
    public int index;
    
    private SelectedEvent(int index) {
      this.index = index;
    }
  }
}
