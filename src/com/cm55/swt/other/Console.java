package com.cm55.swt.other;

import java.util.*;
import java.util.List;

import org.eclipse.swt.*;
import org.eclipse.swt.events.*;
import org.eclipse.swt.graphics.*;
import org.eclipse.swt.widgets.*;

import com.cm55.swt.color.*;


public class Console extends Composite {

  //private static final Log log = LogFactory.getLog(Console.class);

  /** 垂直スクロールバー */
  protected Slider scrollBar;

  /** 垂直スクロールバーの占有幅。非表示時は０ */
  protected int scrollBarWidth;

  private List<String>rows = new ArrayList<String>();
  
  /** 先頭行 */
  protected int topRow;

  /** 可視行数 */
  protected int visibleRows;

  /** フォント高さ */
  protected int fontHeight;

  /** 行高さ */
  protected int rowHeight;
  
  /** 行追加をフォローする */
  protected boolean followAdding = true;
  
  /** 最大行数 */
  protected int maxRows = 2000;
  
  public Console(Composite parent) {
    this(parent, SWT.BORDER|SWT.NO_BACKGROUND);
  }

  public Console(Composite parent, int style) {
    super(parent, style);

    // 垂直スクロールバー作成
    scrollBar = new Slider(this, SWT.VERTICAL|SWT.NO_FOCUS);
    scrollBar.setValues (
      0, //int selection,
      0, // int minimum,
      100, //int maximum,
      10, //int thumb,
      1, //int increment,
      10 //int pageIncrement)
    );

    // スクロールバーが動作した時にスクロールする。
    scrollBar.addSelectionListener(new SelectionAdapter(){
      @Override
      public void widgetSelected(SelectionEvent e) {
        scrollTo(scrollBar.getSelection());
      }
    });

    // Gridリサイズ時の動作指定
    addControlListener(new ControlAdapter() {
      public void controlResized(ControlEvent e) {
        Console.this.resize(e);
      }
    });

    // ペイントイベント受信
    addPaintListener(new PaintListener() {
      public void paintControl(PaintEvent e) {
        Console.this.paint(e);
      }
    });

    // キーボードリスナ設定
    addKeyListener (new KeyAdapter() {
      public void keyPressed(KeyEvent e) {
        //Console.this.keyPressed(e);
      }
    });

    // マウスホイールリスナ設定
    addListener(SWT.MouseWheel, new Listener(){
      public void handleEvent (Event event) {
        scroll(-event.count);
      }
    });

    setFont(getFont());
    rowHeight = fontHeight;
  }

  /** 行追加に追随してスクロール */
  public void setFollowAdding(boolean value) {
    followAdding = value;
  }
  
  public boolean getFollowAdding() {
    return followAdding;
  }
  
  /** 最大行数 */
  public void setMaxRows(int value) {
    maxRows = value;
  }
  
  public int getMaxRows() {
    return maxRows;
  }
  
  /** スクロール */
  public void scroll(int c) {
    while (c < 0) {
      scrollUp();
      c++;
    }
    while (c > 0) {
      scrollDown();
      c--;
    }
  }

  public void clear() {
    if (isDisposed()) return;
    rows = new ArrayList<String>();
    resize(null);
  }
  
  public void addRow(String row) {
    if (isDisposed()) return;
    while (rows.size() >= maxRows) 
      rows.remove(0);
    rows.add(row);    
    resize(null);
    if (followAdding) {
      makeRowVisible(rows.size() - 1);
    }
  }
  
  public void setFont(Font font) {
    //ystem.out.println("Grid#setFont " + font);
    super.setFont(font);

    // フォント高さを取得
    GC gc = new GC(this);
    gc.setFont(getFont());
    Point extent = gc.stringExtent("漢g");
    fontHeight = extent.y;
    
    gc.dispose();
  }

  public Font getFont() {
    //ystem.out.println("Grid#getFont " + super.getFont());
    return super.getFont();
  }

  public void setEnabled(boolean value) {
    if (getEnabled() == value) return;
    super.setEnabled(value);
    redraw();
  }


  /** 行数を取得する */
  public int getRowCount() {
    return rows.size();
  }

  public String getRow(int i) {
    return rows.get(i);
  }
  
  public String[]getRows() {
    return rows.toArray(new String[0]);
  }

  /** 行高さ設定 */
  public void setRowHeight(int h) {
    if (rowHeight == h) return;
    rowHeight = h;
    resize(null);
  }

  /** 行高さ取得 */
  public int getRowHeight() {
    return rowHeight;
  }

  /** リサイズ時のレイアウト */
  protected void resize(ControlEvent e) {

    // クライアント領域取得
    Rectangle clientArea = getClientArea();
    int left   = clientArea.x;
    int top    = clientArea.y;
    int right  = left + clientArea.width;
    int bottom = top + clientArea.height;

    // ヘッダとセル内容すべてを表示した場合のトータルの高さを計算
    int totalHeight = rowHeight * rows.size();

    // 可視行数計算
    visibleRows = clientArea.height /rowHeight;

    // topRowを調整
    topRow = Math.min(
      topRow,
      Math.max(0, rows.size() - visibleRows)
    );

    // スクロールバーレイアウト
    if (clientArea.height < totalHeight) {

      // スクロールバーのレイアウト
      Point verExtent = scrollBar.computeSize(SWT.DEFAULT, SWT.DEFAULT, false);
      scrollBar.setBounds(right - verExtent.x, top, verExtent.x, clientArea.height);

      scrollBar.setValues (
        topRow,      // selection,
        0,           // minimum,
        rows.size(),    // maximum,
        visibleRows, // thumb,
        1,           // increment,
        visibleRows  // pageIncrement)
      );

      scrollBarWidth = verExtent.x;
      scrollBar.setVisible(true);
      //if (log.isTraceEnabled()) log.trace("scrollBar visible");
    } else {
      scrollBarWidth = 0;
      scrollBar.setVisible(false);
      //if (log.isTraceEnabled()) log.trace("scrollBar not visible");
    }


    // 再描画
    redraw();
  }

  // ジオメトリ ///////////////////////////////////////////////////////////////


  /** clientAreaからスクロールバー部分を除いたもの */
  protected Rectangle getPaneArea() {
    Rectangle result = getClientArea();
    if (scrollBar.isVisible()) result.width -= scrollBarWidth;
    return result;
  }

  /** ページアップ */
  public void pageUp() {
    if (rows.size() <= 0) return;
    int newTopRow = Math.max(0, topRow - visibleRows);
    makeRowVisible(newTopRow);
  }

  /** ページダウン */
  public void pageDown() {
    if (rows.size() <= 0) return;
    int newBottomRow = Math.min(topRow + visibleRows * 2 - 1, rows.size() - 1);
    makeRowVisible(newBottomRow);
  }

  /** スクロールアップ */
  public void scrollUp() {
    if (rows.size() <= 0) return;
    int newTopRow = Math.max(0, topRow - 1);
    makeRowVisible(newTopRow);
  }

  /** スクロールダウン */
  public void scrollDown() {
    if (rows.size() <= 0) return;
    int newBottomRow = Math.min(topRow + visibleRows, rows.size() - 1);
    makeRowVisible(newBottomRow);
  }

  /** 描画 */
  protected void paint(PaintEvent e) {

    GC gc = e.gc;

    // クライアント描画起点を求める
    Rectangle clientArea = getPaneArea();
    int client_left = clientArea.x;
    int client_top = clientArea.y;
    int client_right = clientArea.x + clientArea.width;
    //if (log.isTraceEnabled()) log.trace("clientArea " + clientArea);


    // クリップ領域を求める。ただし、クライアント描画起点を原点とする。
    int clip_left = e.x - client_left;
    int clip_right = e.x + e.width - client_left;
    int clip_top = e.y - client_top;
    int clip_bottom = e.y + e.height - client_top;
    //if (log.isTraceEnabled()) log.trace("clipArea " + clip_left + " " + clip_right + " " + clip_top + " " + clip_bottom);

    // 背景描画
    gc.setBackground(ColorStock.getSystemColor(SWT.COLOR_INFO_BACKGROUND));
    gc.fillRectangle(clip_left, clip_top, clip_right - clip_left, clip_bottom - clip_top);
    

    // 描画開始行・終了行を求める
    int startRow, endRow;
    {
      int offsetTop = Math.max(clip_top, 0);
      int offsetBottom = clip_bottom;
      startRow = topRow + offsetTop / rowHeight;
      endRow = Math.min(rows.size() - 1, topRow + (offsetBottom - 1) / rowHeight);
    }
    
    // 行の描画
    int y = (startRow - topRow) * rowHeight;
    for (int row = startRow; row <= endRow; row++) {
      String s = rows.get(row);
      gc.drawString(s, 0, y);
      y += rowHeight;
    } // end row
  }

  // スクロール ///////////////////////////////////////////////////////////////

  /** スクロール実行。指定行がトップ行となるようにスクロールする。
   *  スクロールバーの操作は行わない。 */
  protected void scrollTo(int newTopRow) {

    if (topRow == newTopRow) return;

    //if (log.isTraceEnabled()) log.trace("newTopRow " + newTopRow);

    // クライアント領域を求める
    Rectangle clientArea = getClientArea();
    int left   = clientArea.x;
    int top    = clientArea.y;
    int right  = left + clientArea.width;
    int bottom = top + clientArea.height;

    // ヘッダ部分しか表示されてない
    if (bottom <= top) {
      return;
    }

    // 新旧topRowの距離を求める
    int distance = newTopRow - topRow;

    if (Math.abs(distance) >= visibleRows) {
      // 距離がvisibleRows以上の場合、すべてを再描画
      //if (log.isTraceEnabled()) log.trace("redraw all " + distance);
      topRow = newTopRow;
      redraw(left, top, right - left, bottom - top, false);
    } else {
      // コピー操作を行い、必要部分だけを描画
      //if (log.isTraceEnabled()) log.trace("redraw part " + distance);
      int lineHeight = rowHeight;
      int copyOffset = Math.abs(distance) * lineHeight;
      //int copySize = (visibleRows - Math.abs(distance)) * lineHeight;
      int copySize = (bottom - top) - Math.abs(distance) * lineHeight;
      GC gc = new GC(this);
      gc.setFont(getFont());
      if (distance > 0) {
        // 上方向にスクロール
        gc.copyArea(left, top + copyOffset, clientArea.width, copySize, left, top);
        redraw(left, top + copySize, clientArea.width, bottom - top - copySize, false);
      } else {
        // 下方向にスクロール
        gc.copyArea(left, top, clientArea.width, copySize, left, top +  copyOffset);
        redraw(left, top, clientArea.width, copyOffset, false);
      }
      gc.dispose();
      topRow = newTopRow;
    }
  }

  /** 指定行をトップ行にする */
  public void makeRowTop(int row) {

    //if (log.isTraceEnabled()) log.trace("makeRowTop " + row);

    if (row == topRow) return;
    if (rows.size() <= visibleRows) return;

    // 上側にはみだしている
    if (row < topRow) {
      scrollBar.setSelection(row);
      scrollTo(row);
      return;
    }

    // 要望行を補正
    if (row + visibleRows > rows.size()) {
      row = rows.size() - visibleRows;
    }

    // スクロール
    scrollBar.setSelection(row);
    scrollTo(row);
  }

  /** 指定行を可視にする */
  public void makeRowVisible(int row) {

    // 上側にはみ出している
    if (row < topRow) {
      scrollBar.setSelection(row);
      scrollTo(row);
      return;
    }

    // 下側にはみ出している
    if (topRow + visibleRows <= row) {
      int newTopRow = row - visibleRows + 1;
      scrollBar.setSelection(newTopRow);
     scrollTo(newTopRow);
    }
  }
}
