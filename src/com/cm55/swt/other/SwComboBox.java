package com.cm55.swt.other;

import java.util.function.*;

import org.eclipse.swt.*;
import org.eclipse.swt.events.*;
import org.eclipse.swt.widgets.*;

import com.cm55.swt.*;
import com.cm55.swt.event.*;

/**
 * 編集不可能なコンボボックス
 */
public class SwComboBox extends SwControl<Combo> {
  
  /** 表示テキスト */
  public String[]texts;
  
  /** 作成 */
  public SwComboBox(String[]texts, Consumer<SwSelectionEvent>consumer) {    
    this.texts = texts;
    listen(SwSelectionEvent.class, consumer);
  }

  public int getSelectionIndex() {
    return getControl().getSelectionIndex();
  }
  
  @Override
  protected Combo doCreate(Composite parent) {
    SwComboBox udCombo = this;
    Combo combo = new Combo(parent, SWT.READ_ONLY);
    for (int i = 0; i < udCombo.texts.length; i++) combo.add(udCombo.texts[i]);
    
    combo.addSelectionListener(new SelectionAdapter() {
      @Override
      public void widgetSelected(SelectionEvent e) { bus.dispatchEvent(new SwSelectionEvent(SwComboBox.this, e)); }
    });
    return combo;
  };
  
}
