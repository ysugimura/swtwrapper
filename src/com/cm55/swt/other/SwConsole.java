package com.cm55.swt.other;

import org.eclipse.swt.widgets.*;

import com.cm55.swt.*;
import com.cm55.swt.misc.*;



public class SwConsole extends SwControl<Console> implements WheelTarget {
 
  public Console doCreate(Composite parent) {
    Console console = createConsole(parent);
    return console;
  }
  
  public void setFollowAdding(boolean value) {
    control.setFollowAdding(value);
  }
  
  public boolean getFollowAdding() {
    return control.getFollowAdding();
  }
  
  public void setMaxRows(int value) {
    control.setMaxRows(value);
  }
  
  public int getMaxRows() {
    return control.getMaxRows();
  }
  
  public void clear() {
    control.clear();
  }
  
  public void addRow(String s) {
    control.addRow(s);
  }
  
  protected Console createConsole(Composite parent) {
    return new Console(parent);
  }

  public void makeRowTop(int row) {
    control.makeRowTop(row);
  }

  public int getRowCount() {
    return control.getRowCount();
  }

  public String getRow(int row) {
    return control.getRow(row);
  }
  
  public String[]getRows() {
    return control.getRows();
  }
  
  /** スクロール */
  @Override
  public void scroll(int count) {
    control.scroll(count);
  }

  public void redraw() {
    control.redraw();
  }
}
