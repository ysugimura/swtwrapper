package com.cm55.swt.other;

import org.eclipse.swt.*;
import org.eclipse.swt.events.*;
import org.eclipse.swt.widgets.*;

import com.cm55.swt.*;

/**
 * カスタムリストボックス
 */
public class SwList extends SwControl<List>  {

  protected int style = SWT.SINGLE|SWT.BORDER|SWT.V_SCROLL; //SWT.READ_ONLY;

  protected List doCreate(Composite parent) {
    List list = new List(parent, style);
    list.addSelectionListener(new SelectionAdapter() {
      @Override
      public void widgetSelected(SelectionEvent e) {
    //    setModified();
        int index = list.getSelectionIndex();
        selected(index);
        dispatchEvent(new SelectedEvent(index));
      }
    });
    return list;
  }

  public void setItems(String[]items) {
    control.setItems(items);
  }

  public void setItem(int index, String item) {
    control.setItem(index, item);
  }

  public void addItem(String item) {
    control.add(item);
  }

  public void addItem(int index, String item) {
    control.add(item, index);
  }

  public void upItem(int index) {
    assert(0 < index && index < count());
    String item = control.getItem(index);
    control.remove(index);
    control.add(item, index - 1);
  }

  public void downItem(int index) {
    assert(0 <= index && index < count() - 1);
    String item = control.getItem(index);
    control.remove(index);
    control.add(item, index + 1);
  }

  public String getItem(int index) {
    return control.getItem(index);
  }
  public void clearItems() {
    control.setItems(new String[0]);
  }

  public void removeItem(int index) {
    control.remove(index);
  }
  public int count() {
    return control.getItemCount();
  }

  public String[]getItems() {
    String[]items = new String[control.getItemCount()];
    for (int i = 0; i < items.length; i++) {
      items[i] = control.getItem(i);
    }
    return items;
  }
  
  protected void selected(int index) {
  }

  public void select(int index) {
    if (index < 0) control.deselectAll();
    else          control.select(index);
  }

  public int getSelectionIndex() {
    return control.getSelectionIndex();
  }

  public static class SelectedEvent {
    public int index;
    protected SelectedEvent(int index) {
      this.index = index;
    }
  }


}
