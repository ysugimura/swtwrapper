// Created by Cryptomedia Co., Ltd. 2006/02/21
package com.cm55.swt.other;

import java.util.function.*;

/**
 * 任意のオブジェクトの配列を登録するコンボボックス。
 * コンボのアイテムとして表示される文字列をオブジェクトから取得するコールバックを指定できる。
 * @author ysugimura
 *
 * @param <T>
 */
public class SwObjectCombo<T> extends AbstractCombo {

  /** 選択肢オブジェクト配列 */
  protected T[]objects;

  /** 単純にオブジェクト配列を登録する。ラベルはオブジェクトのtoString()で得られる文字列になる　 */
  public void setObjects(@SuppressWarnings("unchecked") T...objects) {
    setObjects(null, objects);
  }

  /** オブジェクト配列を登録し、ラベル文字列を取得するコールバックを指定する */
  public void setObjects(Function<T,String>labelGet, @SuppressWarnings("unchecked") T...objects) {    
    if (labelGet == null) labelGet = t->t.toString();
    this.objects = objects;
    String[]str = new String[objects.length];
    for (int i = 0; i < str.length; i++) str[i] = labelGet.apply(objects[i]);
    setItems(str);    
  }
  
  /** 登録されたオブジェクト配列を取得する */
  public T[]getObjects() {
    return objects;
  }

  /** 指定したオブジェクトを選択状態にする。比較は==で行われる */
  public void select(T o) {
    for (int i = 0; i < objects.length; i++) {
      if (objects[i] == o) {
        select(i);
        return;
      }
    }
  }

  /** 選択中のオブジェクトを取得する。選択中が無い場合はnullを返す */
  public T getSelection() {
    int index = getSelectionIndex();
    if (index < 0) return null;
    return objects[index];
  }
}
