package com.cm55.swt.other;

import org.eclipse.swt.*;
import org.eclipse.swt.widgets.*;

import com.cm55.swt.*;

public class SwProgressBar extends SwControl<ProgressBar> {

  private int max = 100;
  
  public SwProgressBar() {}

  
  public void setMaximum(int value) {
    getControl().setMaximum(value);
  }
  
  public void setSelection(int value) {
    getControl().setSelection(value);
  }
  
  @Override
  protected ProgressBar doCreate(Composite parent) {
    
    ProgressBar bar = new ProgressBar(parent, SWT.SMOOTH);
    bar.setMinimum(0);
    bar.setMaximum(max);

    return bar;
  };
  
}
