// Created by Cryptomedia Co., Ltd. 2005/12/26
package com.cm55.swt.other;

import java.util.function.*;

import org.eclipse.swt.*;
import org.eclipse.swt.events.*;
import org.eclipse.swt.widgets.*;

import com.cm55.swt.*;
import com.cm55.swt.event.*;

public class SwSpinner extends SwControl<Spinner> {
  
  public SwSpinner(Consumer<SwSelectionEvent>handler) {    
    listen(SwSelectionEvent.class, handler);
  } 
  
  public int getSelection() {
    return control.getSelection();
  }
  
  public void setSelection(int value) {
    control.setSelection(value);
  }
  
  public void setMinimum(int value) {
    control.setMinimum(value);
  }
  
  public void setMaximum(int value) {
    control.setMaximum(value);
  }
  
  @Override
  protected Spinner doCreate(Composite parent) {
    int style = SWT.BORDER;
    Spinner spinner = new Spinner(parent, style);
    spinner.addSelectionListener(new SelectionAdapter() {
      @Override
      public void widgetSelected(SelectionEvent e) { bus.dispatchEvent(new SwSelectionEvent(SwSpinner.this, e)); }
    });
    return spinner;
  };
}
