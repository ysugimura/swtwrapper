// Created by Cryptomedia Co., Ltd. 2005/11/11
package com.cm55.swt.table;

import org.apache.commons.logging.*;
import org.eclipse.swt.*;
import org.eclipse.swt.events.*;
import org.eclipse.swt.graphics.*;
import org.eclipse.swt.widgets.*;

import com.cm55.swt.*;
import com.cm55.swt.color.*;

/**
 * 文字列配列の配列をデータとしてSWTのテーブルに簡易に表示する。
 * 表示前に列定義（名称・サイズ・アライン）を指定しておくこと。
 *
 */
public class StringTable extends SwControl<Table> {

  private static final Log log = LogFactory.getLog(StringTable.class);

  /** スタイル定義 */
  protected int style = SWT.H_SCROLL | SWT.V_SCROLL | SWT.BORDER;

  protected RowColoring rowColoring = new StdRowColoring();

  public StringTable() {
  }

  /** 作成 */
  public Table doCreate(Composite parent) {
    Table table = new Table(parent, style);
    table.addSelectionListener(new SelectionAdapter() {
      @Override
      public void widgetSelected(SelectionEvent e) {
        dispatchEvent(new SelectionChangedEvent());
      }
    });
    return table;
  }

  public int getSelectionIndex() {
    return control.getSelectionIndex();
  }
  
  /** 列定義 */
  public static class Column {

    /** 名称 */
    public String name;

    /** サイズ */
    public int size;

    /** アライン */
    public int shift;

    public Column(String name, int size) {
      this(name, size, 0);
    }

    public Column(String name, int size, int shift) {
      this.name = name;
      this.size = size;
      this.shift = shift;
    }
  }

  /** 列定義を設定する */
  public void setColumns(Column[]columns) {
    clearRows();
    clearColumns();

    control.setHeaderVisible(true);
    for (Column column: columns) {
      int shift;
      switch (column.shift) {
      case 0: shift = SWT.LEFT; break;
      case 1: shift = SWT.CENTER; break;
      default: shift = SWT.RIGHT;
      }
      TableColumn tableColumn = new TableColumn(control, shift);
      tableColumn.setText(column.name);
      tableColumn.setWidth(column.size);
    }
  }

  /** データ行を設定する */
  public void setRows(String[][]rows) {

    clearRows();

    for (int i = 0; i < rows.length; i++) {
      TableItem item = new TableItem(control, SWT.NULL);
      item.setText(rows[i]);
      item.setBackground(rowColoring.get(i));
    }
  }

  public void setRowColoring(RowColoring rowColoring) {
    this.rowColoring = rowColoring;
  }

  public void setBackground(int index, Color color) {
    if (log.isTraceEnabled())
      log.trace("setBackground itemCount:" + control.getItemCount() + ", index:" + index);

    control.getItem(index).setBackground(color);
  }

  public void showRow(int index) {
    control.showItem(control.getItem(index));
  }

  public void setSelection(int start, int end) {
    control.setSelection(start, end);
  }

  protected void clearRows() {
    for (int i = control.getItemCount() - 1; i >= 0; i--)
      control.remove(i);
  }

  protected void clearColumns() {
    for (int i = control.getColumnCount() - 1; i >= 0; i--) {
      control.getColumn(i).dispose();
    }
  }

  public void setData(String[]data) {
    int colCount = control.getColumnCount();
    assert(data.length % colCount == 0);
    int rowCount = data.length / colCount;

    for (int i = control.getItemCount() - 1; i >= 0; i--)
      control.remove(i);

    /*
    for (int i = 0; i < rows.length; i++) {
      TableItem item = new TableItem(table, SWT.NULL);
      item.setText(rows[i]);
    }
    */

    int rowIndex = 0;
    for (int index = 0; index < data.length; index += colCount) {
      String[]row = new String[colCount];
      System.arraycopy(data, index, row, 0, colCount);
      TableItem item = new TableItem(control, SWT.NULL);
      item.setText(row);
      item.setBackground(rowColoring.get(rowIndex));
      rowIndex++;
    }

  }
  
  public class SelectionChangedEvent {
    
  }
}
