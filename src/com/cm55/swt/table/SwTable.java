// Created by Cryptomedia Co., Ltd. 2005/10/15
package com.cm55.swt.table;

import java.util.*;

import org.apache.commons.logging.*;
import org.eclipse.jface.viewers.*;
import org.eclipse.swt.*;
import org.eclipse.swt.events.*;
import org.eclipse.swt.widgets.*;


import com.cm55.swt.*;

/**
 * TableViewerのラッパ
 * @author ysugimura
 */
public class SwTable extends SwControl<Table> {

  private static final Log log = LogFactory.getLog(SwTable.class);
  
  protected TableViewer tableViewer;
  protected boolean multipleSelection = true;
  
  /** スタイル定義 */
  protected int style = SWT.H_SCROLL | SWT.V_SCROLL | SWT.BORDER | SWT.FULL_SELECTION;

  public SwTable() {}

  /** 作成 */
  public Table doCreate(Composite parent) {
    if (multipleSelection) style |= SWT.MULTI;
    else                   style |= SWT.SINGLE;

    tableViewer = new TableViewer(parent, style);
    
    // テーブル選択変更の通知
    tableViewer.addPostSelectionChangedListener(e->
      dispatchEvent(new TableSelectionChangedEvent(
        control.getSelectionIndex(), 
        control.getSelectionCount(),
        control.getSelectionIndices()
      ))
    );
    
    // ダブルクリックの通知
    tableViewer.addDoubleClickListener(e->
      dispatchEvent(new TableDoubleClickedEvent(
        control.getSelectionIndex(),
        control.getSelectionCount(),
        control.getSelectionIndices()
      ))
    );

    // ラベルプロバイダの設定
    tableViewer.setLabelProvider(getLabelProvider());
    
    // コンテンツプロバイダの設定
    tableViewer.setContentProvider(getContentProvider());

    return tableViewer.getTable();
  }
  
  protected IBaseLabelProvider getLabelProvider() {
    return new LabelProvider();
  }
  
  protected IContentProvider getContentProvider() {
    return new MyContentProvider();
  }  

  /** コンテンツプロバイダ */
  public class MyContentProvider implements ILazyContentProvider {

    /** 
     * VIRTUALテーブルの中の新たに表示が必要になった行について、一行ずつ
     * 呼び出される。
     * @param row 行インデックス
     */
    public void updateElement(int row) {
      
      // コンテントを設定。ここで設定するのは単に行インデックス。
      // 後でラベルプロバイダが呼び出されるので、そこでIntegerの行インデックス
      // から実際の表示データを取得する。
      //tableViewer.replace(new Integer(index), index);
    }

    public void dispose() { }

    public void inputChanged(org.eclipse.jface.viewers.Viewer viewer, 
        Object old_object, Object new_object) {
    }
  }

  /** ラベルプロバイダ */
  /*
  protected class MyLabelProvider extends LabelProvider implements ITableColorProvider {
    public Color getForeground(Object element, int columnIndex) {
      return null;
    }
    public Color getBackground(Object element, int columnIndex) {
      return null;
    }
  }
  */

  /** TableViewerを取得する */
  public TableViewer getTableViewer() {
    return tableViewer;
  }

  /** 先頭行を取得する */
  public int getTopRow() {
    return control.getTopIndex();
  }

  /** 先頭行を設定する */
  public void setTopRow(int row) {
    setTopIndex(row);
  }
    
  public void setTopIndex(int index) {
    control.setTopIndex(index);
  }
  
  /** 選択消去 */
  public void deselectAll() {
    control.deselectAll();
  }

  /** 行範囲を選択 */
  public void setSelection (int startRow, int endRow) {
    control.setSelection(startRow, endRow);
  }

  /** 単一行を選択 */
  public void setSelection(int row) {
    control.setSelection(row);
  }

  /** 任意行を選択 */
  public void setSelection(int[]rows) {
    control.setSelection(rows);
  }

  /** 任意行を選択 */
  public void setSelection(Integer[]indices) {
    int[]temp = new int[indices.length];
    for (int i = 0; i < temp.length; i++) temp[i] = indices[i];
    setSelection(temp);
  }

  /** 全選択 */
  public void selectAll() {
    control.setSelection(0, control.getItemCount());
  }

  /** 全行選択解除 */
  public void clearSelection() {
    control.setSelection(new int[0]);
  }
  
  /** 選択を取得。インデックス昇順に並べられる */
  public int[] getSelection() {
    int[]indices =  control.getSelectionIndices();
    Arrays.sort(indices);
    return indices;
  }
  
  /** 選択数を取得 */
  public int getSelectionCount() {
    return control.getSelectionCount();
  }

  /** 選択数が１のときだけそのインデックスを返す。そうでなければ-1を返す */
  public int getUniqueSelection() {
    int[]indices = control.getSelectionIndices();
    if (indices.length == 1) return indices[0];
    return -1;
  }
  
  /** すべての列定義、すべての行をクリア、ヘッダの表示もクリア */
  public void clear() {
    control.clearAll();
    for (int i = control.getColumnCount() - 1; i >= 0; i--) {
      control.getColumn(i).dispose();
    }
    control.setItemCount(0);
    control.setHeaderVisible(false);
  }

  /** 行数を取得する */
  public int getRowCount() {
    return getItemCount();
  }
  
  public int getItemCount() {
    return control.getItemCount();
  }

  /** 行数を設定する */
  public void setRowCount(int count) {
    setItemCount(count);
  }
  
  public void setItemCount(int count) {
    control.setItemCount(count);
  }

  /** ヘッダの可視 */
  @SuppressWarnings("unchecked")
  public <T extends SwTable>T setHeaderVisible(boolean value) {
    control.setHeaderVisible(value);
    return (T)this;
  }

  /** 
   * 列を追加する。{@link Table}内部に{@link TableColumn}が追加される
   * @param name 列タイトル
   * @param width 幅
   * @param shift アラインメント
   * @return 
   */
  public <T extends SwTable>T addColumn(String name, int width, int shift) {
    return addColumn(name, width, shift, null, null);
  }

  /** 列を追加する。 */
  @SuppressWarnings("unchecked")
  public <T extends SwTable>T addColumn(String name, int width, int shift,
      ControlListener l, SelectionListener s) {
    switch (shift) {
    case 0: shift = SWT.LEFT; break;
    case 1: shift = SWT.CENTER; break;
    case 2: shift = SWT.RIGHT; break;
    }
    TableColumn column;
    column = new TableColumn(control, shift);
    column.setText(name);
    column.setWidth(width);
    if (l != null) column.addControlListener(l);
    if (s != null) column.addSelectionListener(s);
    return (T)this;    
  }
    
  /** 列数を取得する */
  public int getColumnCount() {
    return control.getColumnCount();
  }

  /** 指定列の{@link TableColumn}を取得 */
  public TableColumn getColumn(int col) {
    return control.getColumn(col);
  }
  
  /** 税列定義の{@link TableColumn}を取得する */
  public TableColumn[]getColumns() {
    TableColumn[]columns = new TableColumn[control.getColumnCount()];
    for (int i = 0; i < columns.length; i++) {
      columns[i] = control.getColumn(i);
    }
    return columns;
  }

  /** 指定列を削除する */
  public void removeColumn(int col) {
    control.getColumn(col).dispose();
  }

  /** 前列を削除する */
  public void removeAllColumns() {
    for (int i = control.getColumnCount() - 1; i >= 0; i--) {
      control.getColumn(i).dispose();
    }
  }
}
