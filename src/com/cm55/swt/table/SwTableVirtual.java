package com.cm55.swt.table;

import org.eclipse.swt.*;
import org.eclipse.swt.widgets.*;

import com.cm55.swt.color.*;

/**
 * あらかじめ全データを設定するのではなく、行表示がなされるその時に行データが要求される。
 * @author ysugimura
 */
public class SwTableVirtual extends SwTable {

  public SwTableVirtual() {
    // 仮想テーブルであることを宣言する必要がある。
    style |= SWT.VIRTUAL;
  }

  @Override
  public Table doCreate(Composite parent) {

    // テーブルを作成する
    Table table = super.doCreate(parent);
    
    final RowColoring rowColoring = new StdRowColoring();    
    
    // データ設定要求イベントが発生したら、そのTableItem（行）にデータを設定すべく
    // TableFillItemEventを発生する。
    table.addListener(SWT.SetData, event-> {
      TableItem item = (TableItem)event.item;        
      int row = table.indexOf(item);
      item.setBackground(rowColoring.get(row));
      dispatchEvent(new TableFillItemEvent(row, item));    
    });
    return table;
  }

  /** 全行を再表示 */
  public void redraw() {
    tableViewer.refresh();
    control.clearAll();
  }
}
