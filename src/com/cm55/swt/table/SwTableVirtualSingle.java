package com.cm55.swt.table;

public  class SwTableVirtualSingle extends SwTableVirtual {
  public SwTableVirtualSingle() {
    multipleSelection = false;
  }
  /** 唯一の選択インデックスを取得 */
  public int getSelectionIndex() {
    return control.getSelectionIndex();
  }
  public void setSelectionIndex(int index) {
    setSelection(new int[] { index });
  }
}
