package com.cm55.swt.table;

public class TableDoubleClickedEvent {
  public final int first;
  public final int count;
  public final int[]indices;
  TableDoubleClickedEvent(int first, int count, int[]indices) {
    this.first = first;
    this.count = count;
    this.indices = indices;
  }
  
  @Override
  public String toString() {
    StringBuilder buf = new StringBuilder();
    for (int index: indices) {
      buf.append(index + " ");
    }
    return "SelectionChangedEvent first:" + first + ", count:" + count + ", indices:" + buf;
  }
}