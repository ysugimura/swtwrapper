package com.cm55.swt.table;

import org.eclipse.swt.widgets.*;

/** 仮想テーブルの行充填用イベント */
public class TableFillItemEvent {
  
  /** テーブルの行インデックス */
  public final int index; 
  
  /** {@link Table}内の該当行の{@link TableItem} */
  public final TableItem item;

  /** 行、{@link TableItem}を指定する */
  protected TableFillItemEvent(int row, TableItem item) {
    this.index = row;
    this.item = item;
  }

  /** 対象行を取得する */
  public int getRow() {
    return index;
  }

  @Override
  public String toString() {
    return "FillItemEvent " + index + ", " + item;
  }
}