package com.cm55.swt.table;

/** 選択イベント */
public class TableSelectionChangedEvent {
  public final int first;
  public final int count;
  public final int[]indices;
  TableSelectionChangedEvent(int first, int count, int[]indices) {
    this.first = first;
    this.count = count;
    this.indices = indices;
  }
  
  @Override
  public String toString() {
    StringBuilder buf = new StringBuilder();
    for (int index: indices) {
      buf.append(index + " ");
    }
    return "SelectionChangedEvent first:" + first + ", count:" + count + ", indices:" + buf;
  }
}