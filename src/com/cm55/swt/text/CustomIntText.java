package com.cm55.swt.text;

import org.eclipse.swt.events.*;



/**
 * 整数値のみを受け付けるテキストコントロール
 */
public class CustomIntText extends SwText {

  //private static final Log log = LogFactory.getLog(CustomIntText.class);

  protected boolean suppressZero = true;
  protected boolean noVerifying = false;
  protected boolean allowMinus = false;

  /** 作成 */
  public CustomIntText() {
    imeMode = SwIme.IME_OFF;
  }

  public void allowMinus(boolean value) {
    allowMinus = value;
  }

  /** 入力文字のチェック */
  protected void verifyText(VerifyEvent e) {
    //if (log.isTraceEnabled())
      //log.trace("verifyText ... ");

    // 検証なしモードの場合は素通し
    if (noVerifying) {
      //if (log.isTraceEnabled()) log.trace("noVerifying mode");
      return;
    }

    // 特殊キーの場合は素通し
    if (e.character < 0x20 || e.character == 0x7F) {
      //if (log.isTraceEnabled()) log.trace("special keys");
      return;
    }

    e.doit = checkInsertion(getBeforeInsertion(e), e.start, e.character);
    if (!e.doit) beep();
  }

  /** 挿入チェック */
  protected boolean checkInsertion(String string, int position,  char character) {
    //if (log.isTraceEnabled())
      //log.trace("checkInsertion " + string + "," + position + "," + character);

    // 挿入後の文字列が整数に変換可能かをチェックすればよい。
    String s = string.substring(0, position) + character + string.substring(position);
    //if (log.isTraceEnabled())
      //log.trace("s:" + s);
    if (allowMinus && s.equals("-"))
      s = "-0";
    try {
      int value = Integer.parseInt(s);
      if (value < 0 && !allowMinus) return false;
      return true;
    } catch (Exception ex) {
      return false;
    }
  }


  /** 整数値を設定する */
  public void setInt(int value) {
    noVerifying = true;
    try {
      if (value == 0 && suppressZero) setText("");
      else                            setText("" + value);
    } finally {
      noVerifying = false;
    }
  }

  /** 整数値を取得する */
  public int getInt() {
    String s = getText().trim();
    if (suppressZero && s.length() == 0) return 0;
    if (s.equals("-")) return 0;
    return Integer.parseInt(s);
  }

  /** 値設定 */
  public void setValue(Object value) {
    setInt(((Integer)value).intValue());
  }

  /** 値取得 */
  public Object getValue() {
    return new Integer(getInt());
  }
}
