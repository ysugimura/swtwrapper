package com.cm55.swt.text;

import java.text.*;

/**
 * CustomIntTextと同じだが、指定された桁数を小数点以下として表示する。
 */
public class SwFracIntText extends CustomIntText {

  protected int fractions = 1;
        
  protected boolean checkInsertion(String string, int position, char character) {
  
    boolean hyphen = string.indexOf('-') >= 0;
    int period = string.indexOf('.');
        
    if (character == '-') { // ハイフン入力 
      // 先頭でかつハイフンがまだないこと
      if (position == 0 && !hyphen)
        return true;
    } else if (character == '.') { // ピリオド入力
      if (period < 0) {
        // ピリオドがあってはいけない
        if (!hyphen || position > 0) {
          // ハイフンがないか、あるいは先頭でない
          if (string.length() - position <= fractions) {
            // 挿入位置以降の文字数がfractionsを越えない。
            return true;
          }
        }
      }
    } else if ('0' <= character && character <= '9') { // 数字入力
      if (0 <= period && period < position) {
        // ピリオドがあり、かつピリオド以降への挿入の場合
        // サイズがfractionsを超えないように
        if (string.length() - period - 1 < fractions) 
          return true;
      } else {
        // ピリオドがないか、あるいはピリオド以前への挿入の場合
        // 先頭でないかあるはハイフンがないこと。
        if (position > 0 || !hyphen)
          return true;
      }
    }
    
    return false;
  }
  
  private static final DecimalFormat format = new DecimalFormat("#.#");
  
  /** 整数値を設定する */
  public void setInt(int value) {
    //ystem.out.println("setInt .... " + value);
    
    noVerifying = true;
    try {
      if (value == 0 && suppressZero) {
        setText("");
        return;
      }
      double dvalue = value;
      for (int i = 0; i < fractions; i++)
        dvalue /= 10;
      String s = format.format(dvalue);
      //ystem.out.println("formatted " + s);
      setText(s);
    } finally {
      noVerifying = false;
    }
  }
  
  /** 整数値を取得する */
  public int getInt() {
    String s = getText().trim();
    if (suppressZero && s.length() == 0) return 0;
    double dvalue = Double.parseDouble(s);
    for (int i = 0; i < fractions; i++)
      dvalue *= 10;
    return (int)Math.round(dvalue);
  }
}
