package com.cm55.swt.text;

import org.eclipse.swt.*;
import org.eclipse.swt.events.*;
import org.eclipse.swt.widgets.*;

public class SwIme {


  public static final int IME_DONTCARE = SWT.NONE; // ???

  public static final int IME_ZENKAKU_HIRAGANA = SWT.ROMAN| SWT.NATIVE|SWT.DBCS;
  public static final int IME_ZENKAKU_KATAKANA = SWT.ROMAN| SWT.NATIVE|SWT.DBCS|SWT.PHONETIC;
  public static final int IME_HANKAKU_KATAKANA = SWT.ROMAN| SWT.PHONETIC;

  public static final int IME_ZENKAKU_EISU = SWT.DBCS;
  public static final int IME_OFF = SWT.NONE;
  public static final int IME_ON = IME_ZENKAKU_HIRAGANA;
  
  /////////////////////////////////////////////////////////////////////////////
  // IME制御
  // Shell.setImeInputModeを参照のこと。
  // 以下はwindowsの場合
  /////////////////////////////////////////////////////////////////////////////



  /** 指定されたTextにフォーカスが移ったときに自動的にIMEモードを設定する。 */
  public static void setImeMode(Text text, final int mode) {
    text.addFocusListener(new FocusListener() {
      public void focusGained(FocusEvent e) {
        ((Text)e.widget).getShell().setImeInputMode(mode);
      }
      public void focusLost(FocusEvent e) {
        ((Text)e.widget).getShell().setImeInputMode(IME_OFF);
      }
    });
  }
}
