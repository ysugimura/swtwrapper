package com.cm55.swt.text;

/**
 * 名称、名称よみ編集用のカスタムテキストコントロール
 */
public class SwNameText extends SwText {

  public SwNameText() {
    imeMode = SwIme.IME_ZENKAKU_HIRAGANA;
  }
  
  public boolean isSane() {
    return getText().length() > 0;
  }
}
