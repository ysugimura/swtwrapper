package com.cm55.swt.text;

import org.eclipse.swt.*;
import org.eclipse.swt.events.*;
import org.eclipse.swt.graphics.*;
import org.eclipse.swt.widgets.*;

import com.cm55.swt.misc.*;

/**
 * カスタムテキストコントロール
 */
public class SwText extends TextModification<Text> {

  //private static final Log log = LogFactory.getLog(CustomText.class);

  /** スタイル */
  protected int style = SWT.BORDER;

  /** IMEモード */
  protected int imeMode = SwIme.IME_DONTCARE;

  /** フォーカスが来たときに全選択 */
  protected boolean selectAllOnFocus = true;

  /** returnで次のコントロールへ */
  protected boolean moveFocusOnReturn = true;


  private boolean noModifyEvent;

  public void setEchoChar(char c) {
    control.setEchoChar(c);
  }

  /** 前景色を設定する */
  public void setForeground(Color color) {
    control.setForeground(color);
  }

  /** 背景色を設定する */
  public void setBackground(Color color) {
    control.setBackground(color);
  }
  

  /** 領域設定 */
  public void setBounds(Rectangle rect) {
    control.setBounds(rect);
  }

  /** 正当か */
  public boolean isSane() { return true; }

  /** 作成 */
  @Override
  public Text doCreate(Composite parent) {
    Text textEdit = new Text(parent, style);

    // Modifyリスナを設定
    textEdit.addModifyListener(new ModifyListener() {
      public void modifyText(ModifyEvent e) {
        setModified();
        if (!noModifyEvent) {
          dispatchEvent(new ModifiedEvent(SwText.this));
          SwText.this.modifyText(e);
        }
      }
    });

    // Verifyリスナを設定
    textEdit.addVerifyListener(new VerifyListener() {
      public void verifyText(VerifyEvent e) {
        if (!noModifyEvent)
          SwText.this.verifyText(e);
      }
    });

    // Focusリスナを設定
    textEdit.addFocusListener(new FocusAdapter() {
      public void focusGained(FocusEvent e) {
        if (selectAllOnFocus) textEdit.selectAll();
        SwText.this.focusGained(e);
      }
      public void focusLost(FocusEvent e) {
        //ystem.out.println("...........");
        SwText.this.focusLost(e);
      }
    });

    if (moveFocusOnReturn) {
      // Returnキーでフォーカス移動
      textEdit.addTraverseListener(new TraverseListener() {
        public void keyTraversed(TraverseEvent e) {
          if (e.detail == SWT.TRAVERSE_RETURN && moveFocusOnReturn) {
            beforeMoveFocusOnReturn();
            e.detail = SWT.TRAVERSE_TAB_NEXT;
          }
        }
      });
    }

    textEdit.addKeyListener(new KeyListener() {
      public void keyPressed(KeyEvent e) {
        String text = getText();
        
        // このイベントを受信してウィジェットをdisposeしてしまう場合がある
        dispatchEvent(new KeyPressedEvent(e));
        
        if (e.keyCode == 0x1000050) {
          // このイベント送信時にウィジェットを参照してはいけない。
          dispatchEvent(new EnterPressedEvent(text));
        }
      }
      public void keyReleased(KeyEvent e) {
      }
    });

    // IMEモードを設定
    SwIme.setImeMode(textEdit, imeMode);

    return textEdit;
  }

  protected void beforeMoveFocusOnReturn() {

  }

  public void setMoveFocusOnReturn(boolean value) {
    moveFocusOnReturn = value;
  }
  
  /** TextEdit取得 */
  public Text getTextEdit() {
    return control;
  }

  /** テキスト設定 */
  public void setText(String s) {
    //ystem.out.println("!!!!! " + s);
    noModifyEvent = true;
    try {
      String _s = s == null? "":s;
      hook(()->control.setText(_s));
    } finally {
      noModifyEvent = false;
    }
    clearModified();
  }

  /** テキスト取得 */
  public String getText() {
    return control.getText();
  }

  /** */
  public void setEditable(boolean value) {
    control.setEditable(value);
  }

  public boolean getEditable() {
    return control.getEditable();
  }

  /** 値設定 */
  public void setValue(Object o) {
    setText((String)o);
  }

  /** 値取得 */
  public Object getValue() {
    return getText();
  }

  /** キャラクタ数取得 */
  public int getCharCount() {
    return control.getCharCount();
  }

  /** 全選択 */
  public void selectAll(){
    control.selectAll();
  }

  /** 選択数取得 */
  public int getSelectionCount() {
    return control.getSelectionCount();
  }

  /** Verify */
  protected void verifyText(VerifyEvent e) {
  }

  /** Modify */
  protected void modifyText(ModifyEvent e) {
  }

  /** focusGained */
  protected void focusGained(FocusEvent e) {
  }

  /** focusLost */
  protected void focusLost(FocusEvent e) {

  }

  protected void beep() {
    SwUtil.beep(control);
  }

  public void setSelection(int start) {
    control.setSelection(start);
  }

  public void setSelection(int start, int end) {
    control.setSelection(start, end);
  }

  public void setSelection(Point p) {
    control.setSelection(p);
  }

  public static class ModifiedEvent {
    public SwText customText;
    private ModifiedEvent(SwText t) {
      customText = t;
    }
  }


  /** 挿入前文字列を得る */
  protected String getBeforeInsertion(VerifyEvent e) {
    String s = getText();
    if (e.start >= e.end) return s;
    return s.substring(0, e.start) + s.substring(e.end);
  }
  
  /////////////////////////////////////////////////////////////////////////////
  // イベント
  /////////////////////////////////////////////////////////////////////////////

  public static class KeyPressedEvent {
    public KeyEvent e;

    private KeyPressedEvent(KeyEvent e) {
      this.e = e;
    }
  }
  
  public static class EnterPressedEvent {
    public final String text;
    private EnterPressedEvent(String text) {
      this.text = text;
    }
  }
}
