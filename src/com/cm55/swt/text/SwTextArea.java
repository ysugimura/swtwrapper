package com.cm55.swt.text;

import org.eclipse.swt.*;
import org.eclipse.swt.events.*;
import org.eclipse.swt.graphics.*;
import org.eclipse.swt.layout.*;
import org.eclipse.swt.widgets.*;


/**
 * 複数行編集用テキスト
 * <p>
 * 
 * </p>
 */
public class SwTextArea extends TextModification<Composite> {

  /** スタイル */
  protected int style = SWT.BORDER|SWT.MULTI|SWT.WRAP|SWT.V_SCROLL;
  

  /** テキストエディット */
  protected Text textEdit;
  
  /** 表示大きさ */
  protected int dispLines = 1;
  
  /** 表示大きさｘ＊ｙ */
  protected Point dispSize;
  
  /** IMEモード */
  protected int imeMode = SwIme.IME_DONTCARE;
  
  /** 作成 */
  public Composite doCreate(Composite parent) {
    Composite composite = new Composite(parent, SWT.NULL) {
      public Point computeSize(int wHint, int hHint, boolean changed) {
        if (dispSize != null) return dispSize;
        
        Point p = super.computeSize(wHint, hHint, changed);
        dispSize = new Point(p.x, p.y * dispLines);
        return dispSize;
      }
    };
    composite.setLayout(new FillLayout());
    textEdit = new Text(composite, style);
    
    // Modifyリスナを設定    
    textEdit.addModifyListener(new ModifyListener() {
      public void modifyText(ModifyEvent e) {
        //ystem.out.println("-----");
        setModified();
        SwTextArea.this.modifyText(e);
      }
    });
    
    // IMEモードを設定
    SwIme.setImeMode(textEdit, imeMode);
    
    return composite;
  }
  
  /** Modify */
  private final void modifyText(ModifyEvent e) {
    if (!textSetting) {
    dispatchEvent(new ModifiedEvent(this));
    }
  }
  
  private boolean textSetting;
  
  /** テキスト設定 */
  public void setText(String s) {
    textSetting = true;
    try {
    if (s == null) s = "";
    textEdit.setText(s);
    } finally {
      textSetting = false;
    }
  }
  
  /** テキスト取得 */
  public String getText() {
    return textEdit.getText();
  }
  
  /** テキスト追加 */
  public void append(String s) {
    textEdit.append(s);
  }
  
  public void setEditable(boolean value) {
    textEdit.setEditable(value);
  }
  
  public boolean getEditable() {
    return textEdit.getEditable();
  }
  
  public static class ModifiedEvent {
    public  SwTextArea textArea;
    private ModifiedEvent(SwTextArea t) {
      textArea = t;
    }
  }

}
