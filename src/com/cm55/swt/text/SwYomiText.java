package com.cm55.swt.text;

import org.eclipse.swt.events.*;

/**
 * 読み用のテキスト
 */
public class SwYomiText extends SwNameText {

  /** 入力文字のチェック */
  protected void verifyText(VerifyEvent e) {
    if (e.text.length() == 0) return;
    char c = e.text.charAt(0);
    if (Character.UnicodeBlock.of(c) !=
        Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS) {
      return;
    }
    /*
    if (CharKind.get(c) == (CharKind.ZENKAKU|CharKind.HIRAGANA) ||
        c == 'ー' || c == '゛' || c == '゜' || c == '　' ||
        Character.isLowerCase(c) || Character.isUpperCase(c) ||
        Character.isDigit(c)) {
      return;
    }
    */
    e.doit = false;
    beep();
  }
}
