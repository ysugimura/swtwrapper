package com.cm55.swt.text;

public class SwZenkakuArea extends SwTextArea {
  
  public SwZenkakuArea() {
    imeMode = SwIme.IME_ZENKAKU_HIRAGANA;
  }
}
