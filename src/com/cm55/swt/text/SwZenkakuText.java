package com.cm55.swt.text;

/**
 * <p>タイトル: </p>
 * <p>説明: </p>
 * <p>著作権: Copyright (c) 2003</p>
 * <p>会社名: cryptomedia</p>
 * @author yuichi sugimura
 * @version 1.0
 */
public class SwZenkakuText extends SwText {

  public SwZenkakuText() {
    imeMode = SwIme.IME_ZENKAKU_HIRAGANA;
  }

}
