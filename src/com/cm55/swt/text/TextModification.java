package com.cm55.swt.text;

import org.eclipse.swt.widgets.*;

import com.cm55.swt.*;
import com.cm55.swt.event.*;



/**
 * カスタムコントロール
 * <p>
 * EventSourceを新たに作成
 * </p>
 */
abstract class TextModification<T extends Control> extends SwControl<T> {

  
  private boolean modified;
  /** 変更済みか */
  public boolean isModified() { return modified; }

  protected void setModified() {
    modified = true;
    bus.dispatchEvent(new SwModifiedEvent(this));
  }

  public void clearModified() {
    modified = false;
  }
  

}
