package com.cm55.swt.tree;

import org.apache.commons.logging.*;
import org.eclipse.jface.viewers.*;
import org.eclipse.swt.widgets.*;


/**
 * チェックボックス付ツリー
 */
public class SwCheckTree extends SwTreeAbstract<CheckboxTreeViewer> {

  private static final Log log = LogFactory.getLog(SwCheckTree.class);
  
  private static final int NONE = 0;
  private static final int HALF = 1;
  private static final int ALL = 2;
  
  protected CheckboxTreeViewer createTreeViewer(Composite parent) {
    final CheckboxTreeViewer treeViewer = new CheckboxTreeViewer(parent, style);
    treeViewer.addCheckStateListener(new ICheckStateListener() {
      public void checkStateChanged(CheckStateChangedEvent event) {
        Object node = event.getElement();
        
        // グレー状態を解除
        treeViewer.setGrayed(node, false);        

        // チェックorアンチェック
        if (event.getChecked()) {
          treeViewer.setSubtreeChecked(node, true);
          dispatchEvent(new TreeCheckChangedEvent(node, true));
        } else {
          treeViewer.setSubtreeChecked(node, false);
          dispatchEvent(new TreeCheckChangedEvent(node, false));
        }
      
        influenceState(node);
      }
    });
    return treeViewer;
  }

  /** チェック状態による影響を与える */
  private void influenceState(Object node) {
    ITreeContentProvider cp = (ITreeContentProvider)treeViewer.getContentProvider();
    
    // 親の状態を連続的に変更
    Object parent = cp.getParent(node);
    while (parent != null) {
      boolean checked = false;
      boolean noChecked = false;
      for (Object child: cp.getChildren(parent)) {
        if (treeViewer.getGrayed(child)) {
          checked = true;
          noChecked = true;
        } else {
          if (treeViewer.getChecked(child)) checked = true;
          else                              noChecked = true;
        }
      }
      treeViewer.setChecked(parent, checked);
      treeViewer.setGrayed(parent, checked & noChecked);
      parent = cp.getParent(parent);
    }      
  }
  
  public void setChecked(Object element, boolean checked) {
    setChecked(new Object[] { element }, checked);
  }
  
  /** 指定された要素をチェック・非チェック */
  public void setChecked(Object[]elements, boolean checked) {
    for (Object e: elements) {
      treeViewer.setChecked(e, checked);
      treeViewer.setSubtreeChecked(e, checked);
      influenceState(e);
    }
    //resetState();
  }
  

  
  /** 全状態を再計算 */
//  private final void resetState() {
//    final ITreeContentProvider cp = (ITreeContentProvider)treeViewer.getContentProvider();    
//    new Object() {
//      int reset(Object parent) {
//        Object[]children = cp.getElements(parent);
//        if (children.length == 0) {
//          
//        }
//      }
//    }.reset(treeViewer.getInput());
//  }
  
  /** チェック状態変更イベント */
  public static class TreeCheckChangedEvent {
    public final Object node;
    public final boolean checked;
    public TreeCheckChangedEvent(Object node, boolean checked) {
      this.node = node;
      this.checked = checked;
    }
  }
}
