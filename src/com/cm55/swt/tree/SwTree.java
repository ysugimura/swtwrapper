// Created by Cryptomedia Co., Ltd. 2005/10/14
package com.cm55.swt.tree;

import org.eclipse.jface.viewers.*;
import org.eclipse.swt.*;
import org.eclipse.swt.widgets.*;

public class SwTree extends SwTreeAbstract<TreeViewer> {
  
  @Override protected TreeViewer createTreeViewer(Composite parent) {
    return new TreeViewer(parent, style);
  }
  
  public static class Single extends SwTree {
    public Single() {
      style &= ~SWT.MULTI;
      style |= SWT.SINGLE;
    }
  }
}
