package com.cm55.swt.tree;

import java.util.*;

import org.eclipse.jface.viewers.*;
import org.eclipse.swt.*;
import org.eclipse.swt.widgets.*;

import com.cm55.swt.*;

public abstract class SwTreeAbstract<T extends TreeViewer> extends SwControl<Tree> {

  protected T treeViewer;
  protected int style = SWT.BORDER ;
  protected boolean multipleSelection = true;
  protected boolean selectSameLevel = true;

  public SwTreeAbstract() {
    if (multipleSelection) style |= SWT.MULTI;
    else                   style |= SWT.SINGLE;

  }

  /** 作成 */
  public Tree doCreate(Composite parent) {
    treeViewer = createTreeViewer(parent); //new TreeViewer(parent, style);
    Tree tree = treeViewer.getTree();
    treeViewer.addPostSelectionChangedListener(new ISelectionChangedListener() {
      public void selectionChanged(org.eclipse.jface.viewers.SelectionChangedEvent event){
        SwTreeAbstract.this.selectionChanged();
      }
    });

    return tree;
  }
  
  protected  abstract T createTreeViewer(Composite parent);

  
  public void setSelection(Object node) {
    setSelection(new Object[] { node });
  }
  
  /** 選択 */
  public void setSelection(Object[]nodes) {
    treeViewer.setSelection(
        new StructuredSelection(nodes), true
    );  
  }
  
  /** 選択変更 */
  protected void selectionChanged() {

    // 「複数選択でかつ同レベル選択」でない場合は処理しない
    if (!selectSameLevel || !multipleSelection) {
      dispatchEvent(new SelectionChangedEvent());
      return;
    }

    // 現在の選択を取得。選択数が１以下の場合はそのまま
    TreeItem[]selections = control.getSelection();
    if (selections.length <= 1) {
      dispatchEvent(new SelectionChangedEvent());
      return;
    }


    // 最上位レベルのTreeItemをnewSelectionsに集める
    ArrayList<TreeItem>newSelections = new ArrayList<TreeItem>();
    newSelections.add(selections[0]);
    int currentLevel = levelOfItem(selections[0]);
    for (int i = 1; i < selections.length; i++) {
      // 一つのTreeItemを取り出し、レベルを調べる
      TreeItem item = selections[i];
      int itemLevel = levelOfItem(item);

      // 現在と同レベルの場合
      if (itemLevel == currentLevel) {
        newSelections.add(item);
        continue;
      }

      // より上位のレベルであった場合
      if (itemLevel < currentLevel) {
        newSelections = new ArrayList<TreeItem>();
        newSelections.add(item);
        currentLevel = itemLevel;
      }
    }

    // 別レベルのTreeItemが無い場合。そのままイベント発生
    if (newSelections.size() == selections.length) {
      dispatchEvent(new SelectionChangedEvent());
      return;
    }

    // レベル混在だった場合。最上位レベルのTreeItem群を選択しイベント発生
    control.setSelection(newSelections.toArray(new TreeItem[0]));
    dispatchEvent(new SelectionChangedEvent());
  }

  protected int levelOfItem(TreeItem item) {
    int level = 0;
    while (true) {
      TreeItem parent = item.getParentItem();
      if (parent == null) return level;
      item = parent;
      level++;
    }
  }

  /** TreeViewerを取得する */
  public TreeViewer getTreeViewer() {
    return treeViewer;
  }

  /** Treeを取得する */
  public Tree getTree() {
    return treeViewer.getTree();
  }

  public void setContentProvider(IContentProvider provider) {
    treeViewer.setContentProvider(provider);
  }

  public void setLabelProvider(IBaseLabelProvider provider) {
    treeViewer.setLabelProvider(provider);
  }

  public void setInput(Object input) {
    treeViewer.setInput(input);
  }


  public ISelection getSelection() {
    return treeViewer.getSelection();
  }

  public Object[]getSelections() {
    IStructuredSelection selection = (IStructuredSelection)getSelection();
    if (selection.isEmpty()) return new Object[0];
    return selection.toArray();    
  }
  
  public void expandAll() {
    treeViewer.expandAll();
  }
  
  public static class SelectionChangedEvent {
    public SelectionChangedEvent() {
    }
  }
}
