package com.cm55.swt.tv;

import org.eclipse.swt.*;

/**
 * 水平方向のアラインメント
 * @author ysugimura
 */
public enum SwHorAlign {
  LEFT(SWT.LEFT),
  CENTER(SWT.CENTER),
  RIGHT(SWT.RIGHT);
  public final int value;
  private SwHorAlign(int value) {
    this.value = value;
  }
}
