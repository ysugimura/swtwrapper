package com.cm55.swt.tv;

import java.util.*;
import java.util.function.*;

import org.apache.commons.logging.*;
import org.eclipse.jface.viewers.*;
import org.eclipse.swt.*;
import org.eclipse.swt.dnd.*;
import org.eclipse.swt.events.*;
import org.eclipse.swt.widgets.*;

import com.cm55.swt.*;
import com.cm55.swt.dnd.*;
import com.cm55.swt.table.*;

/**
 * テーブルビューア
 * @author ysugimura
 *
 * @param <ROW>
 */
public class SwTableViewer<ROW> extends SwControl<Table> {

  private static final Log log = LogFactory.getLog(SwTableViewer.class);
  
  protected Table table;
  protected TableViewer tableViewer;
  protected boolean multipleSelection = true;
  
  /** スタイル定義 */
  protected int style = SWT.H_SCROLL | SWT.V_SCROLL | SWT.BORDER | SWT.FULL_SELECTION;
  
  /** 列定義 */
  TVColumns<ROW> columns = new TVColumns<>();
  
  /** 指定行インデックスから行オブジェクトを取得する */
  private Function<Integer, ROW>rowDataGetter;

  /**
   * 作成する、指定行インデックスから行オブジェクトを取得する関数を指定
   * @param rowDataGetter
   */
  public SwTableViewer(Function<Integer, ROW>rowDataGetter) {
    this.rowDataGetter = rowDataGetter;
    style |= SWT.VIRTUAL;
  }
  
  /** 作成 */
  public Table doCreate(Composite parent) {
    
    if (multipleSelection) style |= SWT.MULTI;
    else                   style |= SWT.SINGLE;

    tableViewer = new TableViewer(parent, style);
    table = tableViewer.getTable();
    
    // テーブル選択変更の通知
    tableViewer.addPostSelectionChangedListener(
      e->dispatchEvent(new TVSelectionEvent(table.getSelectionIndices()))
    );
    
    // ダブルクリックの通知
    tableViewer.addDoubleClickListener(
      e->dispatchEvent(new TVDoubleClickedEvent(table.getSelectionIndex()))
    );

    // ラベルプロバイダの設定
    tableViewer.setLabelProvider(getLabelProvider());
    
    // コンテンツプロバイダの設定
    tableViewer.setContentProvider(getContentProvider());

    // ヘッダを表示
    table.setHeaderVisible(true);
    
    return table;
  }
    
  private static final Transfer[]TRANSFER_TYPES = new Transfer[] {TextTransfer.getInstance()};
  
  public void setupDragSource(SwDragSource dragSource) {
    DragSource source = new DragSource(table, DND.DROP_DEFAULT | DND.DROP_MOVE | DND.DROP_COPY);
    source.setTransfer(TRANSFER_TYPES);
    source.addDragListener(new SwDragSourceListener(dragSource));
  }

  public void setupDropTarget(SwDropTarget dropTarget) {
    // ドロップターゲットのセットアップ
    DropTarget target = new DropTarget(tableViewer.getTable(), DND.DROP_DEFAULT | DND.DROP_MOVE | DND.DROP_COPY);
    target.setTransfer(TRANSFER_TYPES);
    target.addDropListener(new SwDropTargetListener(tableViewer, dropTarget));    
  }
  
  /** {@link SwTable}のオーバライド */
  protected IContentProvider getContentProvider() {
    return new TVContentProvider(tableViewer);
  }  
    
  /** {@link SwTable}のオーバライド */
  protected IBaseLabelProvider getLabelProvider() {
    return new TVLabelProvider<ROW>(rowDataGetter, columns);
  }

  
  /** 
   * 列を追加する。追加した{@link TVColumn}を保持しておく。
   * @param c  追加する列定義
   */
  public void addColumn(TVColumn<ROW> c) {
    columns.add(c);
    TableColumn column;
    column = new TableColumn(table, c.getAlign().value);
    column.setText(c.getTitle());
    column.setWidth(c.getWidth());
    column.setMoveable(c.movable());    
    column.addControlListener(new ControlListener() {
      public void controlMoved(ControlEvent e) {}
      public void controlResized(ControlEvent e) {
        c.resized(column.getWidth());
      }      
    });
  }
    
  /**
   * 全列を削除する
   */
  public void removeAllColumns() {
    for (int i = control.getColumnCount() - 1; i >= 0; i--) {
      table.getColumn(i).dispose();
    }
    columns.clear();
  }

  /** 指定列を削除する */
  public void removeColumn(int col) {
    table.getColumn(col).dispose();
    columns.remove(col);
  }
  
  /** 
   * 行数を指定すると共に、全体を再描画する。
   * 全行が完全に変わった場合にはこれを呼び出さねばならない。
   */
  public void setRowCount(int count) {
    /*
     *  これには、setInputを使う方法とrefreshを使う方法があるが、setInputでモデルを設定する必要はないため
     *  refreshを行うことにする。
     */ 
    //tableViewer.setInput(null);
    tableViewer.setItemCount(count);
    tableViewer.refresh();
  }
  
  /** 
   * 行数のみを変更する。
   * 既に描画されているところはリフレッシュされない。例えば、追加された行だけを描画することになる。
   * @param count
   */
  public void setRowCountOnly(int count) {
    tableViewer.setItemCount(count);
  }
  
  /** トップ行を取得する */
  public int getTopRow() {
    return table.getTopIndex();
  }
  
  /** トップ行を設定する */
  public void setTopRow(int row) {
    table.setTopIndex(row);
  }
  
  /** すべてを再描画する */
  public void redrawAll() {
    tableViewer.refresh();
  }
  
  /** 指定行を再描画する */
  public void redraw(int[]rows) {
    for (int row: rows) tableViewer.refresh(row);
  }

  /** TableViewerを取得する */
  public TableViewer getTableViewer() {
    return tableViewer;
  }
  
  /** 選択消去 */
  public void deselectAll() {
    table.deselectAll();
  }

  /** 行範囲を選択 */
  public void setSelection (int startRow, int endRow) {
    table.setSelection(startRow, endRow);
  }

  /** 単一行を選択 */
  public void setSelection(int row) {
    table.setSelection(row);
  }

  /** 任意行を選択 */
  public void setSelection(int[]rows) {
    table.setSelection(rows);
  }

  /** 任意行を選択 */
  public void setSelection(Integer[]indices) {
    int[]temp = new int[indices.length];
    for (int i = 0; i < temp.length; i++) temp[i] = indices[i];
    setSelection(temp);
  }

  /** 全選択 */
  public void selectAll() {
    table.setSelection(0, table.getItemCount());
  }

  /** 全行選択解除 */
  public void clearSelection() {
    table.setSelection(new int[0]);
  }
  
  /** 
   * 選択を取得。インデックス昇順に並べられる
   * {@link TableViewer#getSelection()}は正しく動作しないので使わないこと。
   * @return
   */
  public int[] getSelection() {
    int[]indices = table.getSelectionIndices();
    Arrays.sort(indices);
    return indices;
  }
  
  /** 選択数を取得 */
  public int getSelectionCount() {
    return table.getSelectionCount();
  }

  /** 選択数が１のときだけそのインデックスを返す。そうでなければ-1を返す */
  public int getUniqueSelection() {
    int[]indices = table.getSelectionIndices();
    if (indices.length == 1) return indices[0];
    return -1;
  }
  
  /** すべての列定義、すべての行をクリア */
  public void clear() {
    table.clearAll();
    for (int i = table.getColumnCount() - 1; i >= 0; i--) {
      table.getColumn(i).dispose();
    }
    table.setItemCount(0);
  }

  /** 行数を取得する */
  public int getRowCount() {
    return table.getItemCount();
  }

  /** 列数を取得する */
  public int getColumnCount() {
    return table.getColumnCount();
  }

  /** 指定列の{@link TableColumn}を取得 */
  public TableColumn getColumn(int col) {
    return table.getColumn(col);
  }
  
  /** 税列定義の{@link TableColumn}を取得する */
  public TableColumn[]getColumns() {
    TableColumn[]columns = new TableColumn[table.getColumnCount()];
    for (int i = 0; i < columns.length; i++) {
      columns[i] = table.getColumn(i);
    }
    return columns;
  }

}
