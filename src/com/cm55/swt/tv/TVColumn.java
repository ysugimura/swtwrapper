package com.cm55.swt.tv;

import org.eclipse.swt.graphics.*;

/** 
 * {@link SwTableViewer}用の列定義
 * @author ysugimura
 *
 * @param <R>
 */
public abstract class TVColumn<R> {
  
  /** 列タイトルを取得する */
  protected abstract String getTitle();
  
  /** 列幅を取得する */
  protected abstract int getWidth();
  
  /** 列の水平アラインメントを取得する。デフォルトは左詰め */
  protected SwHorAlign getAlign() {
    return SwHorAlign.LEFT;
  }
  
  /** もしあれば指定行の、この列のテキストデータを取得する */
  protected String getText(R r) { return null; }
  
  /** もしあれば指定行の、この列の画像データを取得する */
  protected Image getImage(R r) { return null; }
  
  /** 列を移動可能とする */
  protected boolean movable() { return false; }
  
  /** 列サイズ変更時のコールバック */
  protected void resized(int size) {}
}
