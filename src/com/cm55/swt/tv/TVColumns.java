package com.cm55.swt.tv;

import java.util.*;

/**
 * 内部的に使用する行ホルダ
 * @author ysugimura
 *
 * @param <ROW> 行タイプ
 */
class TVColumns<ROW> {

  /** 列定義 */
  List<TVColumn<ROW>>columns = new ArrayList<>();

  void add(TVColumn<ROW>column) {
    columns.add(column);
  }
  
  TVColumn<ROW> get(int col) {
    return columns.get(col);
  }
  
  Optional<TVColumn<ROW>>getOpt(int col) {
    if (col < 0 || columns.size() <= col) return Optional.empty();
    return Optional.of(columns.get(col));
  }
  
  void remove(int col) {
    columns.remove(col);
  }
  
  void clear() {
    columns = new ArrayList<>();
  }
}
