package com.cm55.swt.tv;

import org.eclipse.jface.viewers.*;

/**
 * {@link TableViewer}では、任意のオブジェクトを{@link TableViewer#setInput(Object)}で設定することにより、
 * 表示すべきモデルが与えられるのだが、そのそれぞれの行を得るために、行インデックスから行オブジェクトを取得刷る必要がある。
 * ただし、ここでは、単純に「行オブジェクト」として「行インデックス」を設定するだけにする。
 * これにより、ユーザ側は、行インデックス・列インデックスのペアに対してデータを供給することになる。
 * @author ysugimura
 */
public class TVContentProvider implements ILazyContentProvider {
  final TableViewer tableViewer;
  TVContentProvider(TableViewer tableViewer) {
    this.tableViewer = tableViewer;
  }
  
  @Override
  public void updateElement(int row) {
    tableViewer.replace(new Integer(row), row);
  }
  
  @Override
  public void dispose() {
  }
  
  @Override
  public void inputChanged(org.eclipse.jface.viewers.Viewer viewer, Object old_object, Object new_object) {
  }
}