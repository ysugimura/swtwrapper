package com.cm55.swt.tv;

/**
 * ダブルクリックされた場合のイベント
 * @author ysugimura
 */
public class TVDoubleClickedEvent {

  /** ダブルクリックされた行 */
  public final int row;

  /** ダブルクリックされた行を指定する */
  TVDoubleClickedEvent(int row) {
    this.row = row;
  }

  @Override
  public String toString() {
    return "" + row;
  }
}
