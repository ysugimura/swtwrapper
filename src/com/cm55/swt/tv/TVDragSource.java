package com.cm55.swt.tv;

import com.cm55.swt.dnd.*;

/**
 * {@link SwTableViewer}の行をドラッグする場合のドラッグソース。
 * デフォルトで{@link SwInternalDragDrop}に、{@link SwTableViewer}オブジェクトと行インデックスを登録する。
 * 
 * @author ysugimura
 */
public class TVDragSource extends SwDragSource {

  /** ドラッグを開始した{@link SwTableViewer} */
  final SwTableViewer<?>tableViewer;

  /** ドラッグを開始した{@link SwTableViewer}を指定する */
  public TVDragSource(SwTableViewer<?> tableViewer) {
    this.tableViewer = tableViewer;
  }

  /** 
   * ドラッグ開始できるかを判断。行数が1以上であれば、開始ひ、
   * {@link SwInternalDragDrop}に内部ドラッグ用の情報を設定しておく
   */
  @Override
  public boolean start() {
    int[]rows = tableViewer.getSelection();
    if (rows.length == 0) return false;
    SwInternalDragDrop.start(tableViewer,  rows);
    return true;
  }

  /** ドラッグドロップ終了時、{@link SwInternalDragDrop}の登録を削除する */
  @Override
  public void finished() {
    SwInternalDragDrop.clear();    
  }
}
