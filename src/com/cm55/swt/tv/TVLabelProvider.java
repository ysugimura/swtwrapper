package com.cm55.swt.tv;

import java.util.function.*;

import org.eclipse.jface.viewers.*;
import org.eclipse.swt.graphics.*;

import com.cm55.swt.color.*;

/**
 * コンテンツプロバイダで「行オブジェクト」が取得されるのだが、その行の特定の列について表示データ、画像イメージを取得する。
 * @author ysugimura
 */
class TVLabelProvider<ROW> implements ITableLabelProvider, ITableColorProvider {

  Function<Integer, ROW>rowDataGetter;
  TVColumns<ROW>columns;
  
  TVLabelProvider(Function<Integer, ROW>rowDataGetter, TVColumns<ROW>columns) {
    this.rowDataGetter = rowDataGetter;
    this.columns = columns;
  }
  
  RowColoring rowColoring = new StdRowColoring();

  /** 
   * ここで渡されるelementは実際には行インデックスであり、それを列インデックスからテキストデータを取得する。
   */
  @Override
  public String getColumnText(Object element, int col) {
    int row = (Integer)element;
    ROW rowObject =  rowDataGetter.apply(row);
    if (rowObject == null) return null;
    return columns.getOpt(col).map(c->c.getText(rowObject)).orElse(null);
  }

  /** elementとして行インデックスが渡されるので、それをデレゲートに渡して、
   * 実際の表示イメージを得る。*/
  @Override
  public Image getColumnImage(Object element, int col) {
    int row = (Integer)element;
    ROW rowObject =  rowDataGetter.apply(row);
    if (rowObject == null) return null;
    return columns.getOpt(col).map(c->c.getImage(rowObject)).orElse(null);
  }

  @Override  
  public void addListener(ILabelProviderListener ilabelproviderlistener) {}
  
  @Override
  public void removeListener(ILabelProviderListener ilabelproviderlistener) {}
  
  @Override
  public void dispose() {}
  
  @Override
  public boolean isLabelProperty(Object obj, String s) { return false; }
  public Color getForeground(Object element, int columnIndex) {
    return null;
  }
  
  @Override
  public Color getBackground(Object element, int columnIndex) {
    return rowColoring.get((Integer)element); //BACK_COLORS[(Integer)element % BACK_COLORS.length];
  }
}