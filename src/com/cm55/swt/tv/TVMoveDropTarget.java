package com.cm55.swt.tv;

import com.cm55.swt.dnd.*;

/**
 * SwTableViewerの内部で行を移動するためのドロップターゲット
 * @author ysugimura
 */
public abstract class TVMoveDropTarget extends SwDropTarget {

  /** 
   * このドロップターゲットを起動した{@link SwTableViewer}オブジェクト。
   * ドラッグ起動が自身であるかを判断するため
   */
  private final SwTableViewer<?>tableViewer;
  
  /** ドロップ場所の行インデックス */
  private int dest;

  /** これが起動された{@link SwTableViewer}を覚えておく */
  public TVMoveDropTarget(SwTableViewer<?> tableViewer) {
    this.tableViewer = tableViewer;
  }

  /** ドロップタイプを決定する。自身からのドロップの場合はMOVE、そうでない場合はNONE */
  @Override
  public SwDropType getDropType() {
    if (SwInternalDragDrop.source() == tableViewer) return SwDropType.MOVE;
    return SwDropType.NONE;
  }

  /** ドロップ場所が変更される都度呼び出される。その際に行インデックスが指定される */
  @Override
  public boolean validate(Object o, SwDropType type) {
    if (o == null) return false;
    dest = (Integer)o;
    return true;
  }

  /** ドロップを実行する */
  @Override
  public boolean performDrop() {        
    
    // 移動する列がdestよりも前であれば、destの一をずらす
    int[]moveRows = SwInternalDragDrop.<int[]>data();
    for (int i = moveRows.length - 1; i >= 0; i--) {
      if (moveRows[i] < dest) dest--;
    }    
    
    return doMove(SwInternalDragDrop.<int[]>data(), dest);
  }

  /** 
   * 実際のドロップを実行する。下位クラスで実装する。
   * 注意、このメソッドのパラメータはあらかじめ調整済。
   * つまり、removeRowsで示される行を削除した後の状態のinsertion位置にそれを挿入するようになっている。
   * @param removeRows 削除する行インデックス。昇順であるため、実際には逆順に削除した方がよい。
   * @param insertion 上記の行を削除した後の状態で挿入すべき行インデックス
   * @return
   */
  protected abstract boolean doMove(int[]removeRows, int insertion);

}
