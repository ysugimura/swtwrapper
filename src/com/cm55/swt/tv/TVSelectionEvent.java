package com.cm55.swt.tv;

import java.util.*;
import java.util.stream.*;

/**
 * 行選択状態が変更された場合の通知イベント
 * 行は昇順にソートされている。
 * @author ysugimura
 */
public class TVSelectionEvent {
  
  /** 昇順ソートされた行インデックス */
  public final int[]rows;  
  
  TVSelectionEvent(int[]_rows) {
    int count = _rows.length;
    rows = new int[count];
    System.arraycopy(_rows, 0, rows, 0, count);
    Arrays.sort(this.rows);
  }
  
  @Override
  public String toString() {
    return IntStream.range(0, rows.length).mapToObj(i->rows[i] + "").collect(Collectors.joining(","));
  }
}
