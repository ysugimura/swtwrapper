package com.cm55.swt.window;

import org.eclipse.swt.widgets.*;

import com.cm55.swt.*;
import com.cm55.swt.button.*;
import com.cm55.swt.event.*;
import com.cm55.swt.label.*;
import com.cm55.swt.layout.*;
import com.cm55.swt.text.*;

/**
 * メッセージを出力し、入力を促す
 */
public class MessageTextBox extends SwDialog {

  private int imeMode;
  SwLabel msgLabel;
  SwText textEdit;

  public class TextEdit extends SwText {
    public TextEdit() {
      selectAllOnFocus = false;
      this.imeMode = MessageTextBox.this.imeMode;
    }
  }

  private MessageTextBox(Control parentControl, String title, String message,
    Object value, int imeMode) {
    super(parentControl, title, WindowModalMode.APPLICATION_MODAL, true);
    this.imeMode = imeMode;
    setCloseOnEscape(true);

    SwLayouter.layout(getShell(), new SwMarginLayout(
      new SwVerticalLayout(new SwItem[] {
        msgLabel = new SwLabel(""),
        textEdit = new TextEdit(),
        new SwAlign.CenterCenter(
          new SwHorizontalLayout(new SwItem[] {
            new SwButton("ＯＫ", this::ok),
            new SwButton("キャンセル", this::cancel)
          })
        )
      })
    ));
    
    // デフォルト値をエディタに設定
    if (value != null) {
      textEdit.setValue(value);
      if (value instanceof String) {
        String strValue = (String)value;
        textEdit.setSelection(strValue.length(), strValue.length());
      }
    }
    
    msgLabel.setText(message);
    getShell().pack();
  }

  /** 編集結果 */
  private Object result;

  /** 編集OK */
  void ok(SwSelectionEvent e) {   
    result = textEdit.getValue();    
    close();
  }

  /** 編集キャンセル */
  void cancel(SwSelectionEvent e) {
    result = null;
    close();
  }

  /////////////////////////////////////////////////////////////////////////////
  // ユーザ側
  /////////////////////////////////////////////////////////////////////////////

  public static String show(Control parent, String title, String message,
    String value, int imeMode) {
    MessageTextBox box = new MessageTextBox(parent, title, message, value, imeMode);
    box.showIt();
    return (String)box.result;
  }

  public static String showZenkakuHiragana(Control parent, String title, String message,
      String value) {
    return show(parent, title, message, value, SwIme.IME_ZENKAKU_HIRAGANA);
  }
  
  public static String show(Control parent, String title, String message,
    String value) {
    return show(parent, title, message, value, SwIme.IME_DONTCARE);
  }

  public static String password(Control parent, String title, String message) {
    MessageTextBox box = new MessageTextBox(parent, title, message, null, SwIme.IME_OFF);
    box.textEdit.setEchoChar('*');
    box.showIt();
    return (String)box.result;
  }
}
