package com.cm55.swt.window;

import org.eclipse.swt.*;
import org.eclipse.swt.events.*;
import org.eclipse.swt.graphics.*;
import org.eclipse.swt.widgets.*;

import com.cm55.swt.misc.*;

/**
 * ポップアップウインドウ
 */
public abstract class PopupWindow {

  //private static final Log log = LogFactory.getLog(PopupWindow.class);
  
  protected Shell parentShell;
  protected Shell shell;
    
  protected PopupWindow(Control parentControl, String title) {
    
    // シェルを作成
    parentShell = parentControl.getShell();
    Display display = parentShell.getDisplay();
    shell = ShellManager.newShell(display, 0);
    shell.setText(title);
    
    // escape押下時にクローズ
    shell.addTraverseListener(new TraverseListener() {
      public void keyTraversed(TraverseEvent e) {
        //if (log.isTraceEnabled()) log.trace("keyTraversed " + SWTUtil.traverseEventToString(e));
        
        if (e.detail == SWT.TRAVERSE_ESCAPE)
          close();
      }
    });
    
    // deactivated時にクローズ
    shell.addShellListener(new ShellAdapter() {
      public void shellActivated(ShellEvent e) {
        //if (log.isTraceEnabled()) log.trace("shellActivated");
      }
      public void shellDeactivated(ShellEvent e) {
        //if (log.isTraceEnabled()) log.trace("shellDeactivated");
        close();
      }
    });
    
    /*
    new UD2SWT(this).layout(shell,
      new UDVerticalLayout(UDMargin.MARGIN_5, UDSpacing.SPACING_5, new UDItem[] {
        new UDGroup( "「名前」の仕入価格", 
          new UDVerticalLayout(UDMargin.MARGIN_5, UDSpacing.SPACING_5, new UDItem[] {
            new UDLabel("2003/1/1：200円"),
            new UDLabel("2003/1/1：200円"),
            new UDLabel("2003/1/1：200円"),
            new UDLabel("2003/1/1：200円"),
            new UDLabel("2003/1/1：200円"),
            
          })
        ),
        new UDButton(null, "指定する", null)
      })
    );
    shell.pack();
    */
  }
  
  // クローズ
  protected void close() {
    if (shell != null) {
      //ModalDialog.topMostShell = parentShell;
      shell.dispose();
      //ModalDialog.topMostShell.forceActive();
    }
    shell = null;
  }
  
  /** 指定コントロールの場所にダイアログを表示する。 */
  protected void showFor(Control control, int controlSide) {
    ensureShell();
    Point position = control.getLocation();
    Point size = control.getSize();
    switch (controlSide) {
    default:
      position.y += size.y;
      break;
    case SWT.RIGHT:
      position.x += size.x;
      break;
    }
    position = SwUtil.toDisplay(control, position.x, position.y);
    shell.setLocation(position.x, position.y);
    //ModalDialog.topMostShell = shell;
    shell.open();
  }
  
  /** 指定コントロールの指定位置にダイアログを表示する */
  public void showAt(Control control, Point position) {
    ensureShell();
    position = SwUtil.toDisplay(control, position.x, position.y);
    shell.setLocation(position.x, position.y);
    //ModalDialog.topMostShell = shell;
    shell.open();
  }
  
  public void ensureShell() {
    if (shell == null) throw new InternalError();
  }
}
