package com.cm55.swt.window;

import java.util.*;

import org.eclipse.swt.widgets.*;

/**
 * {@link Shell}のリークをなくすため、作成された{@link Shell}をすべて
 * 格納しておく。{@link #shellHolders()}が呼び出されると、disposeされていない
 * {@link Shell}のリストを返す。
 */
public class ShellManager {

  /** {@link ShellHolder}のリスト */
  static java.util.List<ShellHolder>holders = new ArrayList<>();
  
  public static Shell newShell() {
    return shellCreated(new Shell());    
  }

  public Shell newShell(int style) {
    return shellCreated(new Shell(style));
  }
  
  public static Shell newShell(Display display) {
    return shellCreated(new Shell(display));
  }

  public static Shell newShell(Display display, int style) {
    return shellCreated(new Shell(display, style));
  }

  public static Shell newShell(Shell parentShell) {
    return shellCreated(new Shell(parentShell));
  }
  
  public static Shell newShell(Shell parentShell, int style) {
    return shellCreated(new Shell(parentShell, style));
  }

  /**
   * 作成したシェルをホルダーリストに登録して返す。
   * @param shell シェル
   * @return シェル
   */
  static Shell shellCreated(Shell shell) {
    dropDisposed();
    holders.add(new ShellHolder(shell));
    return shell;
  }

  /**
   * Dispose済のシェルを捨てる
   */
  static void dropDisposed() {
    for (int index = holders.size() - 1; index >= 0; index--) {
      if (holders.get(index).shell.isDisposed()) {
        holders.remove(index);      
      }
    }    
  }
  
  /**
   * 全{@link ShellHolder}のリストを取得する
   * @return 全{@link ShellHolder}のリスト
   */
  public static java.util.List<ShellHolder>shellHolders() {
    dropDisposed();
    return Collections.unmodifiableList(holders);
  }
  
  /**
   * シェルのホルダー
   */
  public static class ShellHolder {
    
    /** シェル */
    public final Shell shell;
    
    /** シェルが作成された時のスタックトレース */
    public final RuntimeException stack;
    
    ShellHolder(Shell shell) {
      this.shell = shell;
      stack = new RuntimeException();
    }
  }
}
