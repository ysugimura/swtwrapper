package com.cm55.swt.window;

import java.util.*;
import java.util.function.*;

import org.eclipse.swt.*;
import org.eclipse.swt.events.*;
import org.eclipse.swt.graphics.*;
import org.eclipse.swt.widgets.*;

import com.cm55.eventBus.*;
import com.cm55.swt.misc.*;



/**
 * ダイアログ
 *
 */
public class SwDialog  {

  protected EventBus es = new SwEventBus();
  //private static final Log log = LogFactory.getLog(SWTDialog.class);
  
  public <T> Unlistener<T> listen(Class<T> eventType, Consumer<T>listener) {
    return es.listen(eventType, listener);
  }
  
  public <T>Unlistener<T>listen(EventType<T>eventType, Consumer<T>listener) {
    return es.listen(eventType, listener);
  }
    
  public <T>Unlistener<T> listenWeak(Class<T> eventType, Object object, Consumer<T> consumer) {
    return es.listenWeak(eventType, object, consumer);
  }
  
  /** このダイアログを起動したコントロール */
  protected Control parentControl;
  
  /** 親シェル */
  protected Shell parentShell;
  
  /** ディスプレイ */
  private Display display;
  
  /** ダイアログ用に作成されたシェル */
  private Shell shell;

  /** escape押下でクローズする */
  private boolean closeOnEscape;
  
  private boolean disposeOnClose = true;
  
  protected void setCloseOnEscape(boolean value) {
    closeOnEscape = value;
  }
  
  protected void setDisposeOnClose(boolean value) {
    disposeOnClose = value;
  }
  
  /** モーダル */
  private boolean modal = true;
  
  private static int getModalFlag(WindowModalMode modalFlag) {
    if (modalFlag == null) return 0;
    switch (modalFlag) {
    case APPLICATION_MODAL: return SWT.APPLICATION_MODAL;
    case PRIMARY_MODAL:     return SWT.PRIMARY_MODAL;
    case SYSTEM_MODAL:      return SWT.SYSTEM_MODAL;
    default: assert false;
    }
    return 0;
  }

  private int getFrameAttr(EnumSet<WindowFrameAttr> frameAttr) {
    int style = 0;
    for (WindowFrameAttr attr: frameAttr) {
      switch (attr) {
      case HAS_BORDER:   style |= SWT.BORDER; break;     // ボーダーあり
      case CLOSE_BUTTON: style |= SWT.CLOSE; break;      // クローズボタンあり
      case HAS_TITLE:    style |= SWT.TITLE; break;      // タイトルあり    
      case RESIZABLE:    style |= SWT.Resize; break;     // リサイズ可能
      default: assert false;
      }
    }
    return style;
  }
  
  /** 作成する */
  public SwDialog(Control parentControl, String title, WindowModalMode modalFlag, boolean resizable) {
    this(parentControl, title, 
      0
      |SWT.BORDER            // ボーダーあり
      |SWT.CLOSE             // クローズボタンあり
      |SWT.TITLE             // タイトルあり
      |getModalFlag(modalFlag) //SWT.PRIMARY_MODAL 
      |(resizable?SWT.RESIZE:0)
    );
  }
  
  /** 作成する */
  public SwDialog(Display display, String title, WindowModalMode modalFlag, boolean resizable) {
    this(display, title, 
      0
      |SWT.BORDER            // ボーダーあり
      |SWT.CLOSE             // クローズボタンあり
      |SWT.TITLE             // タイトルあり
      |getModalFlag(modalFlag) //SWT.PRIMARY_MODAL
      |(resizable?SWT.RESIZE:0)
    );
  }

  
  private SwDialog(Control parentControl, String title, int style) {   
    this.parentControl = parentControl;
    parentShell = parentControl.getShell();
    create(
      parentShell, 
      ShellManager.newShell(parentShell, style),
      title, 
      style
    );
  }
  
  public SwDialog(Control parentControl, String title, EnumSet<WindowFrameAttr>frameAttr, WindowModalMode modalFlag) {   
    int style = getFrameAttr(frameAttr) | getModalFlag(modalFlag);
      
    this.parentControl = parentControl;
    parentShell = parentControl.getShell();
    create(
      parentShell, 
      ShellManager.newShell(parentShell, style),
      title, 
      style
    );
  }
  
  
  public SwDialog(Display display, String title, int style) {
    //create(display, title, style);
    create(null, 
      ShellManager.newShell(display, style),
      title, 
      style
    );
  }

  protected void packWithWidth(int width) {
    getShell().pack();
    Point size = getShell().getSize();
    getShell().setSize(Math.max(size.x, width), size.y);  
  }
  
  /** 親シェルのリスナー */
  //protected ShellListener parentShellListener;
  
  /** 作成する */
  private void create(Shell parentShell, Shell shell, String title, int style) {
    
    this.shell = shell;
    this.shell.setData(this);
    display = shell.getDisplay();
    
    modal = ((SWT.APPLICATION_MODAL|SWT.PRIMARY_MODAL|SWT.SYSTEM_MODAL) & style) != 0;
    if (title != null) shell.setText(title);

    /* うまくいかない
    
    // 親シェルがcloseしたときのリスナを設定する。
    // 
    // ※モードレスダイアログの場合、そのダイアログを閉じずに親シェルを閉じてしまうと
    // ダイアログのshellClosedイベントは発生しない。
    // このような場合に対応するため、親のシェルクローズを検出し、そのとき当該ダイアログ
    // のdoCloseを呼び出す。
    // ※シェル階層が深い場合は考慮していない。
    if (parentShell != null) {
      parentShellListener = new ShellAdapter() {
        @Override
        public void shellClosed(ShellEvent e) {
          if (log.isTraceEnabled())
            log.trace("parentShell of " + SWTDialog.this.getClass() + " closed");
          SWTDialog.this.doClose();
        }
      };
      parentShell.addShellListener(parentShellListener);
    }
    */
    
    // このShellがclose時のリスナを設定    
    shell.addShellListener(new ShellAdapter() {
      public void shellClosed(ShellEvent e) {

        //if (log.isTraceEnabled())
          //log.trace("" + SWTDialog.this.getClass() + " shellClosed event");
        
        if (!canClose()) {
          e.doit = false;
          return;
        }
        SwDialog.this.doClose();
      }
    });
    
    shell.addDisposeListener(new DisposeListener() {
      public void widgetDisposed(DisposeEvent e) {
        doShellDisposed();
      }
    });
    
    shell.addTraverseListener(new TraverseListener() {
      public void keyTraversed(TraverseEvent e) {
        SwDialog.this.keyTraversed(e);
      }
    });
  }

  /** SWT-Shellを取得 */
  public Shell getShell() {
    return shell;
  }

  /** タイトルを設定 */
  public void setTitle(String title) {
    getShell().setText(title);
  }
  
  
  /** シェル廃棄 */
  private final void doShellDisposed() {
    //if (log.isTraceEnabled()) {
      //log.trace("shellDisposed ... " + this.getClass());
    //}
    es.dispatchEvent(new DisposedEvent<SwDialog>(this));
    shellDisposed();
  }
  
  protected void shellDisposed() {

  }
  
  /** キートラバース */
  protected void keyTraversed(TraverseEvent e) {
    switch (e.detail) {
    case SWT.TRAVERSE_ESCAPE:
      if (closeOnEscape) close();
      e.doit = false;
      break;
    }
  }
  
  public Display getDisplay() {
    return display;
  }
  
  public void activate() {
    getShell().forceActive();
  }
  
  /** Shell以下すべてのコントロールにフォントを設定する */
  public void setFont(final Font font) {
    new Object() {
      public void set(Control control) {
        control.setFont(font);
        if (control instanceof Composite) {
          Composite composite = (Composite)control;
          Control[]children = composite.getChildren();
          for (int i = 0; i < children.length; i++)
            set(children[i]);
        }
      }
    }.set(getShell());
  }
  
  protected void showIt() {
    showIt(null);
  }
  
  /** シェルを適切な位置に表示し、disposeされるまでここでイベントループをまわす */
  protected void showIt(Point shellLoc) {
  
    Rectangle displayBounds = display.getBounds();
    Point shellSize = getShell().getSize();
    
    //if (log.isTraceEnabled()) log.trace("showIt " + shellLoc + ", " + shellSize);
    
    if (shellLoc == null) {
      
      Point centerLoc; 
      
      if (parentControl == null) {
        centerLoc = new Point(
          displayBounds.x + displayBounds.width / 2, 
          displayBounds.y + displayBounds.height / 2
        );
      } else {
        // ダイアログシェルの位置を決定する。既にシェルの大きさは決定済みであるので、
        // そのシェルの表示位置を呼び出しコントロールと同じ位置にする。
        // ただし、displayの境界からはみ出さないようにする。getBounds()はDisplay,
        // Deviceで異なる値を戻すようだが、これはマルチモニタに対応しているものと
        // 思われる。
    
        // 親コントロールのスクリーン上での座標とサイズを取得する。
        // 親コントロールがShellの場合はtoDisplayを行ってはいけない（バグ？）
        Point controlLoc = parentControl.getLocation();
        if (!(parentControl instanceof Shell))
          controlLoc = parentControl.toDisplay(controlLoc);
        Point controlSize = parentControl.getSize();
    
        // シェルを位置づけるべきスクリーン上の中央位置を算出。
        centerLoc = new Point(
          controlLoc.x + controlSize.x / 2, controlLoc.y + controlSize.y / 2
        );
      }
      
      // シェルの位置を仮に決定する。
      
      shellLoc = new Point(
        centerLoc.x - shellSize.x / 2, centerLoc.y - shellSize.y / 2
      );
      
     // if (log.isTraceEnabled())
        //log.trace("shellLoc:"+ shellLoc + ", shellSize:" + shellSize);      
    }

    {
      // ただしディスプレイの境界をはみださないように補正する。
      if (displayBounds.x + displayBounds.width < shellLoc.x + shellSize.x)
        shellLoc.x = displayBounds.x + displayBounds.width - shellSize.x;
      if (shellLoc.x < displayBounds.x)  shellLoc.x = displayBounds.x;
  
      if (displayBounds.y + displayBounds.height < shellLoc.y + shellSize.y)
        shellLoc.y = displayBounds.y + displayBounds.height - shellSize.y;
      if (shellLoc.y < displayBounds.y) shellLoc.y = displayBounds.y;
      
      //if (log.isTraceEnabled())
       // log.trace("adjusted:" + shellLoc);
    }
    
    // シェル位置を決定
    getShell().setLocation(shellLoc.x, shellLoc.y);
    
    
    if (modal && parentShell != null)
      parentShell.setEnabled(false);
    
   
    // ダイアログシェルを表示する。
    (topMostShell = getShell()).open();

    if (modal) {
      // シェルが廃棄されるまでここでイベントループをまわす。
      // こうしないとダイアログを表示した瞬間に制御が呼び出し側に戻ってしまう。
      while (!getShell().isDisposed ()) {
        if (!display.readAndDispatch ()) display.sleep();
      }
    }
  }

  /** 指定されたシェルをスクリーン中央に位置付ける */
  public static void centering(Shell shell) {
    Display display = shell.getDisplay();
    Rectangle displayBounds = display.getBounds();    
    Point centerLoc = new Point(
        displayBounds.x + displayBounds.width / 2, 
        displayBounds.y + displayBounds.height / 2
    );
    Point shellSize = shell.getSize();
    Point shellLoc = new Point(
        centerLoc.x - shellSize.x / 2, centerLoc.y - shellSize.y / 2
      );
    shell.setLocation(shellLoc.x, shellLoc.y);
  }
  
  /** クローズしてよいか聞く */
  protected boolean canClose() {
    return true;
  }
  
  /** プログラムからクローズ操作する */
  public void close() {
    if (!canClose()) return;
    doClose();
  }
  
  /** このシェルのクローズ時 */
  protected void doClose() {
    
   // if (log.isTraceEnabled()) 
    //  log.trace("doClose ... " + this.getClass());
    
    /*
    // 親シェルに設定したリスナーを解除
    if (parentShell != null && !parentShell.isDisposed() && parentShellListener != null) {
      parentShell.removeShellListener(parentShellListener);
      parentShellListener = null;
    }
    */

    // クローズ時ユーザ処理
    onClose();
    
    topMostShell = parentShell;
    if (modal && parentShell != null) parentShell.setEnabled(true);
    getShell().dispose();
    if (modal && parentShell != null) parentShell.forceActive();
    
    es.dispatchEvent(new ClosedEvent<SwDialog>(this));
  }
  
  /** クローズ時 */
  protected void onClose() {
  }
  
  static Shell topMostShell;  
  
  /** ダイアログがクローズした */
  public static class ClosedEvent<T extends SwDialog> {
    public T dialog;
    public ClosedEvent(T dialog) {
      this.dialog = dialog;
    }
  }
  
  /** ダイアログが廃棄された */
  public static class DisposedEvent<T extends SwDialog> {
    public T dialog;
    public DisposedEvent(T dialog) {
      this.dialog = dialog;
    }
  }
  
  
}
