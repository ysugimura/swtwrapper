package com.cm55.swt.window;

import java.io.*;

import org.eclipse.swt.*;
import org.eclipse.swt.widgets.*;

import com.cm55.swt.misc.*;


/**
 * ファイルのオープン指定、セーブ指定
 */
public class SwOpenSaveDialog {

  //private static final Log log = LogFactory.getLog(OpenSaveDialog.class);

  /** シェルと拡張子を指定してオープンファイルを取得する */
  public static File openFile(Shell shell, String ext) {
    
    /*
    FileDialog openDialog = new FileDialog(shell, SWT.OPEN);
    openDialog.setFilterExtensions(new String[] { "*." + ext });
    String openFile = openDialog.open();
    if (openFile == null) return null;
    return new File(openFile);
    */
    
    return openFile(shell, new String[] { ext });
  }

  /** シェルと拡張子を指定してオープンファイルを取得する */
  public static File openFile(Shell shell, String[]exts) {
    
    String[]extensions = new String[exts.length];
    for (int i = 0; i < extensions.length; i++) {
      extensions[i] = "*." + exts[i];
    }
    
    FileDialog openDialog = new FileDialog(shell, SWT.OPEN);
    openDialog.setFilterExtensions(extensions);
    String openFile = openDialog.open();
    if (openFile == null) return null;
    return new File(openFile);
  }
  
  public static File saveFile(Shell shell, String title, String ext, File defaultDir, boolean confirm) {
    return saveFile(shell, title, ext, defaultDir, null, confirm);
  }
  
  /** セーブ先ファイルを決定 
   * @param confirm 保存先ファイルが存在したら「書いていいか」の確認ダイアログを出す */
  public static File saveFile(Shell shell, String title, String ext, File defaultDir, String defaultFilename, boolean confirm) {
    // 拡張子を小文字に
    ext = ext.toLowerCase();

    // セーブダイアログを作成して表示
    FileDialog saveDialog = new FileDialog(shell, SWT.SAVE);
    saveDialog.setFileName(defaultFilename);
    saveDialog.setText(title);
    if (defaultDir != null) {
      saveDialog.setFilterPath(defaultDir.toString());
    }
    saveDialog.setFilterExtensions(new String[] { "*." + ext });
    String saveName = saveDialog.open();
    if (saveName == null) return null;

    // 名称のチェック
    int dotIndex = saveName.lastIndexOf('.');
    if (dotIndex >= 0) {
      String userExt = saveName.substring(dotIndex + 1);
      if (!userExt.toLowerCase().equals(ext))
        saveName = saveName + "." + ext;
    }

    // 存在チェック
    final File saveFile = new File(saveName);
    if (confirm) {
      if (saveFile.exists()) {
        if (!SwUtil.confirmYes(shell,
            "既にファイル「" +  saveFile + "」が存在します。上書きしますか？"))
          return null;
      }
    }

    return saveFile;
  }

}
