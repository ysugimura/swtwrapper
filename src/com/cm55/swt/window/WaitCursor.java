package com.cm55.swt.window;

import java.util.*;

import org.eclipse.swt.*;
import org.eclipse.swt.graphics.*;
import org.eclipse.swt.widgets.*;



/**
 * 待てカーソル
 */
public class WaitCursor {

  //private static final Log log = LogFactory.getLog(WaitCursor.class);

  private static final Cursor cursor_wait = new Cursor(null, SWT.CURSOR_WAIT);
  private static final Cursor cursor_arrow = new Cursor(null, SWT.CURSOR_ARROW);
  @SuppressWarnings("rawtypes")
  private static final Map shellCounter = new HashMap();

  protected Shell shell;

  @SuppressWarnings("unchecked")
  public WaitCursor(Control control) {

    //if (log.isTraceEnabled()) log.trace("WaitCursor " + control.getClass());

    // shellを取得する。
    shell = control.getShell();

    // 待ち数を取得
    Integer count = (Integer)shellCounter.get(shell);
    //if (log.isTraceEnabled()) log.trace("shell " + shell + " count " + count);

    if (count == null) {
      // まだwait中でない。waitカーソルにする。
      shell.setCursor(cursor_wait);
      shellCounter.put(shell,  new Integer(1));
    } else {
      // 既にwait中。インクリメント
      shellCounter.put(shell, new Integer(count.intValue() + 1));
    }
  }

  // 待ち状態廃棄
  @SuppressWarnings("unchecked")
  public synchronized void dispose() {

    //if (log.isTraceEnabled()) log.trace("dispose " + shell);

    if (shell == null) return;
    try {
      Integer count = (Integer)shellCounter.remove(shell);
      if (count == null) {
        return;
      }
      if (count.intValue() <= 1) {
        shell.setCursor(cursor_arrow);
      } else {
        shellCounter.put(shell, new Integer(count.intValue() - 1));
      }
    } finally {
      shell = null;
    }
  }

  protected void finalize() throws Throwable {
    dispose();
    super.finalize();
  }
}
