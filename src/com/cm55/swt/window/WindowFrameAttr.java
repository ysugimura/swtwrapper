package com.cm55.swt.window;

public enum WindowFrameAttr {
  HAS_BORDER,        // ボーダーあり
  CLOSE_BUTTON,      // クローズボタンあり
  HAS_TITLE,         // タイトルあり    
  RESIZABLE,         // リサイズ可能
  MIN_BUTTON,
  MAX_BUTTON;
}
