package com.cm55.swt.window;

/** モーダルモード */
public enum WindowModalMode {
  /** アプリケーション全体に渡ってモーダル */
  APPLICATION_MODAL,
  /** 親ダイアログのみについてモーダル */
  PRIMARY_MODAL, 
  /** OS全体にわたってモーダル */
  SYSTEM_MODAL;
  /** これらのフラグが無い場合はモードレス */
}
